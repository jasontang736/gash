//
// Created by Xiaoting Tang on 10/17/17.
//

#ifndef GASH_GARBLEDCIRCUIT_H
#define GASH_GARBLEDCIRCUIT_H

#include <unordered_map>
#include "garbledgate.h"
#include "circuit.h"
namespace GASH_GC {

    class GarbledCircuit{
    public:
        int n,m;
        ulong q;
        block R; // R includes the permutation bit, and that bit is certainly 1.
        int k; // The security parameter (i.e. the length of R)
        std::unordered_map<ulong, GarbledWireInstance*> garbledWireInstances;
        std::map<ulong, GarbledGate*> garbledGates;
        GarbledWireInstance* getGarbledWireInstance(ulong id);
        GarbledGate* getGarbledGate(ulong id);
        void addGarbledGate(Circuit &circ, ulong id, block row1, block row2, block row3);
        void addGarbledWireInstance(GarbledWireInstance* gw);
        void addGarbledGate(GarbledGate* gg);
        void setGarbledWireLabel(ulong id, block label);
        void initR();
        void printSummaryAsEvaluator();
        void printSummaryAsGarbler();
        GarbledCircuit() {}
        ~GarbledCircuit();
    };

}

#endif //GASH_GARBLEDCIRCUIT_H
