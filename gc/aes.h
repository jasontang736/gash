//
// Created by Xiaoting Tang on 10/8/17.
//

#ifndef GASH_AES_H
#define GASH_AES_H

#include <emmintrin.h>    // SSE2
#include <wmmintrin.h>    // AES
#include <stdio.h>
namespace GASH_GC {
    void expand_key128(__m128i key, __m128i* rndkeys);
    void expand_deckey128(__m128i key, __m128i* rndkeys);
    block aes_encrypt128(block msg, block key);
    block aes_decrypt128(block cipher, block key);
    block getRet(block a, block b, block T, block key);
    block encrypt(block a, block b, block T, block c, block key);
    block decrypt(block a, block b, block T, block cipher, block key);
}
#endif //GASH_AES_H
