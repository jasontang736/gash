//
// Created by Xiaoting Tang on 10/17/17.
//

#ifndef GASH_GATE_H
#define GASH_GATE_H

#include "common.h"
#include "wire.h"
namespace GASH_GC {

    class Gate {
    private:
        ulong id_;
    public:
        int func;
        WireInstance* in0, *in1, *out;
        ulong get_id() { return id_; }
        Gate() {}
        Gate(ulong id, int aFunc, WireInstance* w_in0, WireInstance* w_in1, WireInstance* w_out)
            : id_(id), func(aFunc), in0(w_in0), in1(w_in1), out(w_out) {}
    };
}

#endif //GASH_GATE_H
