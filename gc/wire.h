//
// Created by Xiaoting Tang on 10/17/17.
//

#ifndef GASH_WIRE_H
#define GASH_WIRE_H

#include "common.h"

namespace GASH_GC {

    typedef struct Wire {
        ulong id;
        int value;              // Only use for debugging
        // int interp(int semantic);
    } Wire;

    class WireInstance {
    private:
        Wire* wire_ptr_;
        bool inverted_;
        uint8 inverted_set_times_;
    protected:
        bool is_null() { return !((bool) wire_ptr_); }
        void require_not_null() {
            if (is_null()) {
                LOG(FATAL) << "Referencing a null wire_ptr";
            }
        }
    public:
        WireInstance() : wire_ptr_(nullptr), inverted_(false), inverted_set_times_(0) {}
        WireInstance(ulong id, bool inverted) {
            GASH_REQUIRE_GOOD_ALLOC(this->wire_ptr_ = new Wire);
            set_id(id);
            inverted_set_times_ = 0;
            set_inverted(inverted);
        }
        WireInstance(Wire & wire) : wire_ptr_(&wire), inverted_(false), inverted_set_times_(0) {}
        WireInstance(Wire* wire_ptr) : wire_ptr_(wire_ptr), inverted_(false), inverted_set_times_(0) {}
        WireInstance(Wire & wire, bool inverted) : wire_ptr_(&wire), inverted_(inverted), inverted_set_times_(0) {}
        WireInstance(Wire* wire_ptr, bool inverted) : wire_ptr_(wire_ptr), inverted_(inverted), inverted_set_times_(0) {}

        ulong get_id() {
            require_not_null();
            return wire_ptr_->id;
        }
        void set_id(ulong id) {
            require_not_null();
            wire_ptr_->id = id;
        }
        bool inverted() {
            return inverted_;
        }
        void set_inverted(bool inverted) {
            if(++inverted_set_times_ > 2) {
                LOG(FATAL) << "A Wireinstance's `inverted_` can only be set twice, firstly as an output wire (during initialzation), once as an input wire (during reference).";
            }
            inverted_ = inverted;
        }

        void set_value(int v)  {
            wire_ptr_->value = v;
        }

        int get_value() {
            return inverted() ? wire_ptr_->value ^ 1 : wire_ptr_->value;
        }
    };
}



#endif //GASH_WIRE_H
