//
// Created by Xiaoting Tang on 10/8/17.
//

#include "garble.h"
#include "circuit.h"
#include "evaluator.h"
#include "ot.h"
#include "tcp.h"
namespace GASH_GC {
    Evaluator::Evaluator(std::string ip, int port) {
        this->peer_ip = ip;
        this->peer_port = port;
        this->peer_ot_port = OT_PORT;
    }

    void Evaluator::buildCircuit(std::string circfile) {
        GASH_GC::buildCircuit(circfile, this->c);
        this->gc.n = this->c.n;
        this->gc.m = this->c.m;
        this->gc.q = this->c.q;
    }

    void Evaluator::garbleCircuit() {
        // Simply convert gc_pb to gc
        for (int i = 0; i < this->gc_pb.garbledgates_size(); ++i) {
            const GarbledCircuitProto::GarbledGateProto &gg = this->gc_pb.garbledgates(i);
            this->gc.addGarbledGate(this->c, gg.id(),
                                    concat64_to_block(gg.row_1_0(), gg.row_1_1()),
                                    concat64_to_block(gg.row_2_0(), gg.row_2_1()),
                                    concat64_to_block(gg.row_3_0(), gg.row_3_1()));
        }
        for (unsigned int i = 0; i < this->c.q; ++i) {
            if (this->gc.garbledGates.find(i) ==
                this->gc.garbledGates.end()) { // Not in GarbledGatePB, must be XOR gate
                GarbledGate* gg = new GarbledGate;
                gg->id = i;
                gg->isXOR = true;
                GarbledWireInstance *a = new GarbledWireInstance(this->c.getGate(i)->in0);
                GarbledWireInstance *b = new GarbledWireInstance(this->c.getGate(i)->in1);
                GarbledWireInstance *c = new GarbledWireInstance(this->c.getGate(i)->out);

                // If the garbled wire instance does not exists, create it, otherwise assign it to the garbled gate
                if (this->gc.garbledWireInstances.find(a->get_id()) == this->gc.garbledWireInstances.end()) {
                    gg->in0 = a;
                    this->gc.addGarbledWireInstance(a);
                } else {
                    gg->in0 = this->gc.garbledWireInstances.find(a->get_id())->second;
                    delete a;
                }

                if (this->gc.garbledWireInstances.find(b->get_id()) == this->gc.garbledWireInstances.end()) {
                    gg->in1 = b;
                    this->gc.addGarbledWireInstance(b);
                } else {
                    gg->in1 = this->gc.garbledWireInstances.find(b->get_id())->second;
                    delete b;
                }

                if (this->gc.garbledWireInstances.find(c->get_id()) == this->gc.garbledWireInstances.end()) {
                    gg->out = c;
                    this->gc.addGarbledWireInstance(c);
                } else {
                    gg->out = this->gc.garbledWireInstances.find(c->get_id())->second;
                    delete c;
                }

                this->gc.addGarbledGate(gg);
            }
        }
    }

    void Evaluator::simpleExecuteCircuit() {
        executeCircuit(this->c, this->input_values, this->c.output_ids, this->output_values);
    }

    void Evaluator::readInput(std::string fname) {
        /**
         * input <num_input>
         * 0 1
         * 1 1
         * 2 0
         * 3 1
         * 4 0
         * 5 1
         * ...
         */
        std::ifstream inFile(fname);
        if (!inFile.is_open()) {
            std::cerr << "Unable to open input file " << fname << std::endl;
            exit(1);
        }

        std::string line;
        int size;
        while (getline(inFile, line)) {
            std::vector<std::string> items = split(line, ' ');
            if (items[0].compare("input") == 0) {
                size = stoi(items[1]);
                this->self_in_size = size;
                this->peer_in_size = this->gc.n - this->self_in_size;
                continue;
            }
            if (items.size() != 2) {
                std::cerr << "Illegal input file " << fname << std::endl;
                exit(-1);
            }
            this->input_values.insert(
                    std::make_pair(stoi(items[0]) / 2, stoi(items[1])));
        }
    }

    void Evaluator::buildConnection() {
        buildConnectionAsClient(this->peer_ip, this->peer_port, this->self_sock);
    }

    void Evaluator::recvGarbledTables() {
        int size;
        char *data_c;
        recv(this->self_sock, &size, sizeof(size), 0);
        data_c = new char[size];

        int recv_amt = 0;
        while (recv_amt < size) {
            recv_amt += recv(this->self_sock, data_c + recv_amt, size - recv_amt, 0);
        }

        if (!this->gc_pb.ParseFromArray((const char *) data_c, size)) {
            std::cerr << "De-serialization failed." << std::endl;
            exit(-1);
        }
    }

    void Evaluator::recvPeerLabels() {
        int size;
        ulong id;
        unsigned char label[128 / sizeof(unsigned char)];
        block label_block;

        recv(this->self_sock, &size, sizeof(size), 0);

        for (int i = 0; i < size; i++) {
            recv(this->self_sock, &id, sizeof(ulong), 0);
            recv(this->self_sock, &label, sizeof(block), 0);
            label_block = _mm_load_si128((const __m128i *) label);
            this->inputLabel.insert(std::make_pair(id, label_block));

#ifdef GASH_DEBUG
            std::cout << "Receiving peer label for wire " << id << std::endl;
            std::cout << "label: " << block2hex(label_block) << std::endl;
#endif
        }
    }

    void Evaluator::print() {

        this->gc.printSummaryAsEvaluator();

        // TODO: use a vector to store the could-be-non-consistent ids
        std::cout << "Alice's input" << std::endl;
        for (int i = 0; i < this->peer_in_size; i++) {
            std::cout << INDENTx1 << "input_" << i << ":" << std::endl;
            std::cout << INDENTx2 << "label=" << block2dec(this->inputLabel[i]) << std::endl;
        }

        std::cout << "Bob's input" << std::endl;
        for (ulong i = 0; i < this->self_in_size; i++) {
            std::cout << INDENTx1 << "input_" << i + this->peer_in_size << ":" << std::endl;
            std::cout << INDENTx2 << "value=" << this->input_values[i + this->peer_in_size]
                      << std::endl;
            std::cout << INDENTx2 << "label=" << block2dec(this->inputLabel[i + this->peer_in_size])
                      << std::endl;
        }
    }

    void Evaluator::recvSelfLabels() {

#ifdef GASH_NO_OT
        // Simple recv
        for (auto it = this->input_values.begin(); it != this->input_values.end(); ++it) {
            int b_select = it->second;
            ulong id = it->first;
            block label = simpleRecv(this->self_sock, b_select);
            this->inputLabel.insert(std::make_pair(id, label));

#ifndef GASH_DEBUG
            std::cout << "Receiving self label for wire " << id << std::endl;
            std::cout << "label" << b_select << ": " << block2hex(label);
#endif
        }
#else
        // OT recv
        OTRecv(this->peer_ip, this->peer_ot_port, this->input_values, this->inputLabel);
#endif


    }

    void Evaluator::evaluateCircuit() {
        GASH_GC::evaluateGarbledCircuit(gc, this->c, this->gc_pb_debug, this->inputLabel, this->outputMap, this->outputValues,
        this->c.outputWireIds_);
    }

    void Evaluator::recvOutputMap() {

        int size = 0;
        char* data = recvBytes(this->self_sock, size);

        if (!this->outputMap.ParseFromArray(data, size)) {
            LOG(FATAL) << "Failed to parse outputMap.";
        }
    }

#ifdef GASH_DEBUG
    void Evaluator::sendLabelsForDebug() {
        int size = 0;
        size = this->gc_pb_debug.ByteSize();
        char* data = new char[size];
        if (!this->gc_pb_debug.SerializePartialToArray(data, size)) {
            std::cerr << "Serialization failed." << std::endl;
            abort();
        }
        send(this->self_sock, &size, sizeof(int), MSG_MORE);
        send(this->self_sock, data, size, 0);
    }
#endif

    void Evaluator::sendOutput() {
        int size = this->outputValues.ByteSize();
        char* data = new char[size];
        if (!this->outputValues.SerializeToArray(data, size)) {
            std::cerr << "Serialization failed." << std::endl;
            abort();
        }
        // send(this->self_sock, &size, sizeof(int), 0);
        // send(this->self_sock, data, size, 0);
        if (!sendBytes(this->self_sock, data, size)) {
            std::cerr << "Send Output failed." << std::endl;
            abort();
        }
    }

    void Evaluator::descOutput() {
        for (auto it = this->outputValues.bits().begin(); it != this->outputValues.bits().end(); it++) {
            std::cout << it->first << ": " << (int)it->second << std::endl;
        }
    }

    void Evaluator::getOutput(vector<bool>& bits) {
        for (auto it = this->c.output_ids.begin(); it != this->c.output_ids.end(); ++it) {
            int64 id = *it;
            bool b = false;
            auto bit = this->outputValues.bits().find(id);
            if (bit == this->outputValues.bits().end()) {
                auto vit = this->c.output_constants.find(id);
                if (vit == this->c.output_constants.end())
                    std::cerr << "Cannot find output value for wire " << id << std::endl;
                b = vit->second;
            } else {
                b = bit->second;
            }
            bits.push_back(b);
        }
    }

    Evaluator::~Evaluator() {
//        for (const auto &x: this->outputMap.outputmap) {
//            delete x.second; // The pointer to label pair in outputmap
//        }
    }
}
