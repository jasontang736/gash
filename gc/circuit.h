//
// Created by Xiaoting Tang on 10/17/17.
//

#ifndef GASH_CIRCUIT_H
#define GASH_CIRCUIT_H

#include <set>
#include "gate.h"
#include "common.h"
namespace GASH_GC {
    typedef struct {
        int n, m; // n: number of inputs, m: number of outputs, q: number of gates
        ulong q;
        std::map<ulong, WireInstance*> wireInstances_;
        std::map<ulong, Gate*> gates_;
        std::set<ulong> outputWireIds_;
        vector<ulong> input_ids;
        vector<ulong> output_ids;
        map<ulong, bool> output_constants;
        WireInstance* getWireInstance(ulong i);
        Gate* getGate(ulong i);
        void addWireInstance(ulong id, WireInstance* w);
        void addWireInstance(WireInstance* w);
        void addGate(Gate* g);
        bool hasGate(ulong i);
    } Circuit;
}

#endif //GASH_CIRCUIT_H
