#ifndef GASH_GARBLE_H
#define GASH_GARBLE_H

#include <map>
#include <unordered_map>
#include <fstream>
#include <utility>
#include <cstring>
#include <time.h>
#include <stdint.h>
#include <set>
#include "common.h"
#include "aes.h"
#include "util.h"
#include "circuit.h"
#include "garbledcircuit.h"
#include "labelpair.h"
#include "proto/output_map.pb.h"
#include "proto/bitmap.pb.h"
#include "proto/garbledcircuit.pb.h"

#define XOR_FUNC 6
//#define AND_func 8
//#define OR_func 14
namespace GASH_GC {
    void createWireInstance(ulong id, bool inverted, Circuit& circuit);
    void createInputWireInstance(ulong id, Circuit& circuit);
    void createOutputWireInstance(ulong id, bool inverted, Circuit& circuit);
    void createGate(ulong out, int func, ulong in0, bool in0_inverted, ulong in1, bool in1_inverted, Circuit &circuit);
    GarbledGate* newGarbledGate(ulong i, GarbledWire* wa, GarbledWire* wb, GarbledWire* wc);
    GarbledGate* newGarbledGate(ulong i, GarbledWire* wa, GarbledWire* wb, GarbledWire* wc, block* table);
    LabelPair newLabelPair(block label_0, block label_1);
    void buildCircuit(std::string fname, Circuit& circuit);
    void garbleCircuit(Circuit&, GarbledCircuit&, std::unordered_map<ulong, block>&, std::unordered_map<ulong, block>&, OutputMapProto&,
    std::set<ulong> outputWireIds, std::map<ulong, block>&, std::map<ulong, block>&);
    void evaluateGarbledCircuit(GarbledCircuit&, Circuit&, GarbledCircuitProto&, std::unordered_map<ulong, block>&, OutputMapProto&, BitMapProto&, std::set<ulong>&);
    void executeCircuit(Circuit& circ, std::map<ulong, int>& input_values, std::vector<ulong>& output_ids, std::map<ulong, int>& output_values);

}
#endif
