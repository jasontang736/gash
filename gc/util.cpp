#include <openssl/bn.h>
#include <openssl/bio.h>
#include "garble.h"
#include "util.h"
#include <sys/socket.h>
#include <math.h>
namespace GASH_GC {
    static __m128i cur_seed;

    Timer::Timer() {
        ticking = false;
        numrecords = 0;
    }

    void Timer::tic(const char* event_name) {
        if (ticking) {
            toc();
            tic(event_name);
        } else {
            ticking = true;
            clock_gettime(CLOCK_MONOTONIC, &last_time);
            current_event_name = event_name;
        }
    }

    void Timer::toc() {
        clock_gettime(CLOCK_MONOTONIC, &current_time);
        elapsed_time = (current_time.tv_sec - last_time.tv_sec) * 1000000000 + \
            current_time.tv_nsec - last_time.tv_nsec;
        Event e;
        e.name = current_event_name;
        e.elapsed_time = elapsed_time;
        records.insert(make_pair<int, Event> (numrecords++, (Event &&)e));
        ticking = false;
    }

    void Timer::summarize() {
        for (const auto& event : records) {
            cout << event.second.name << ", ";
        }
        cout << "\n";
        for (const auto& event : records) {
            cout << event.second.elapsed_time << ", ";
        }

    }

    template<typename Out>
    void split(std::string &s, char delim, Out result) {
        std::stringstream ss;
        ss.str(s);
        std::string item;
        while (getline(ss, item, delim)) {
            *(result++) = item;
        }
    }

    std::vector<std::string> split(std::string &s, char delim) {
        std::vector<std::string> elems;
        split(s, delim, back_inserter(elems));
        return elems;
    }

    void srand_sse(unsigned int seed) {
        cur_seed = _mm_set_epi32(seed, seed + 1, seed, seed + 1);
    }

    block random_block() {
        __m128i cur_seed_split;
        __m128i multiplier;
        __m128i adder;
        __m128i mod_mask;
        __m128i sra_mask;
        // __m128i sseresult;
        static const unsigned int mult[4] = {214013, 17405, 214013, 69069};
        static const unsigned int gadd[4] = {2531011, 10395331, 13737667, 1};
        static const unsigned int mask[4] = {0xFFFFFFFF, 0, 0xFFFFFFFF, 0};
        static const unsigned int masklo[4] = { 0x00007FFF, 0x00007FFF, 0x00007FFF, 0x00007FFF };

        adder = _mm_load_si128((__m128i *) gadd);
        multiplier = _mm_load_si128((__m128i *) mult);
        mod_mask = _mm_load_si128((__m128i *) mask);
        sra_mask = _mm_load_si128( (__m128i*) masklo);
        cur_seed_split = _mm_shuffle_epi32(cur_seed, _MM_SHUFFLE(2, 3, 0, 1));

        cur_seed = _mm_mul_epu32(cur_seed, multiplier);
        multiplier = _mm_shuffle_epi32(multiplier, _MM_SHUFFLE(2, 3, 0, 1));
        cur_seed_split = _mm_mul_epu32(cur_seed_split, multiplier);

        cur_seed = _mm_and_si128(cur_seed, mod_mask);
        cur_seed_split = _mm_and_si128(cur_seed_split, mod_mask);
        cur_seed_split = _mm_shuffle_epi32(cur_seed_split, _MM_SHUFFLE(2, 3, 0, 1));
        cur_seed = _mm_or_si128(cur_seed, cur_seed_split);
        cur_seed = _mm_add_epi32(cur_seed, adder);

        return cur_seed;
    }

    block random_R() {
        srand_sse(time(NULL));
        random_block();
        block R = random_block();
        uint8_t val[128 / 8];
        memcpy(val, (const void *) &R, 128 / 8);
        val[0] |= 1;
        return _mm_load_si128((const __m128i *) val);
    }

    bool blockEq(block a, block b) {
        // __m128i vcmp = (__m128i) _mm_cmpneq_ps(_mm_castsi128_ps(a), _mm_castsi128_ps(b)); // compare a, b for inequality
        // uint16_t test = _mm_movemask_epi8(vcmp); // extract results of comparison
        // if (test == 0xffff) // *all* elements not equal
        //     return false;
        // else if (test != 0) // *some* elements not equal
        //     return false;
        // else // no elements not equal, i.e. all elements equal
        //     return true;

        if (memcmp(&a, &b, 128/8) == 0) {
            return true;
        } else {
            return false;
        }
    }

    std::string block2hex(block a) {
        char buf[100];
        int cx;
        int64_t *v64val = (int64_t *) &a;
        cx = snprintf(buf, 100, "%.16llx %.16llx\n", (long long unsigned int) v64val[1],
                      (long long unsigned int) v64val[0]);
        if (cx > 0 && cx < 100) {
            return std::string(buf);
        }
        std::cerr << "Buffer overflow for block2hex()" << std::endl;
        exit(-1);
    }

    std::string block2dec(block a) {
        char buf[100];
        int cx;
        int64_t *v64val = (int64_t *) &a;
        cx = snprintf(buf, 100, "%lld %lld\n", (long long int) v64val[1], (long long int) v64val[0]);
        if (cx > 0 && cx < 100) {
            return std::string(buf);
        }
        std::cerr << "Buffer overflow for block2dec()" << std::endl;
        exit(-1);
    }

    void printBignum(std::string s, BIGNUM *bn) {
        char *str = BN_bn2hex(bn);
        std::stringstream stream;
        stream << s << std::endl << str << std::endl << std::endl;
        std::cout << stream.str();
    }

    LabelPair newLabelPair(block label_0, block label_1) {
        LabelPair l;
        l.label0 = label_0;
        l.label1 = label_1;
        return l;
    }

    block newTweak(ulong g) {
        block ret = _mm_set_epi64x((__int64_t) (g), (__int64_t) 0);
        return ret;
    }


    int find_semantic_of_label_from_pair(ulong id, OutputMapProto& outputMap, block label) {
        if (outputMap.wire_values().find(id) == outputMap.wire_values().end()) {
            LOG(FATAL) << "Wire Id: " << id << "is not in the outputMap.";
        }
        const OutputMapProto_LabelPairProto& labelPair = outputMap.wire_values().at(id);
        const OutputMapProto_LabelSemanticProto& labelSemantic0 = labelPair.label0();
        const OutputMapProto_LabelSemanticProto& labelSemantic1 = labelPair.label1();
        int s = -1;

        if ((labelSemantic0.label_first_half() == get_block_first_half(label)) &&
            (labelSemantic0.label_second_half() == get_block_second_half(label))) {
            s = 0;
        }
        else if ((labelSemantic1.label_first_half() == get_block_first_half(label)) &&
                 (labelSemantic1.label_second_half() == get_block_second_half(label))) {
            s = 1;
        }
        else {
            LOG(FATAL) << "Cannot find matched label for output wire " << id << "\n"\
            << "Expecting either(0): " << block2hex(concat64_to_block(labelSemantic0.label_first_half(), labelSemantic0.label_second_half()))\
            << "\n Or(1): " << block2hex(concat64_to_block(labelSemantic1.label_first_half(), labelSemantic1.label_second_half()))\
            << "But has: " << block2hex(label);
        }
#ifdef GASH_DEBUG
        std::cout << "Comparing labels for wire " << id << std::endl;
        std::cout << "labelSemantic0: " << block2hex(concat64_to_block(labelSemantic0.label_first_half(),
                                                                       labelSemantic0.label_second_half())) << std::endl;
        std::cout << "labelSemantic1: " << block2hex(concat64_to_block(labelSemantic1.label_first_half(),
                                                                       labelSemantic1.label_second_half())) << std::endl;
        std::cout << "expecting: " << block2hex(label) << std::endl;
        std::cout << "Semantic: " << s << std::endl;
#endif

        return s;
    }

    int bin2int(vector<bool> bin) {
        if (bin.size() > 63) {
            std::cerr << "Binary length exceeding 64, suggesting using longer data type" << std::endl;
        }
        int res = 0;
        for (int i = 0; i < bin.size(); ++i) {
            res += ((int)bin[i]) << i;
        }
        return res;
    }

    int partial_bin2int(vector<int>& bits, int start, int size) {
        // Converts binaries from bits[start] to bits[] to decimal.
        if (bits.size() < start + size - 1) {
            std::cerr << "Indexing overflow" << std::endl;
            abort();
        }

        vector<bool> bin;
        for (int i = start; i < start + size; ++i) {
            bin.push_back((bool) bits[i]);
        }

        int r = bin2int(bin);
        return r;
    }

    void printChar2Hex(char* s, int size, const char* fname) {
        for (int i = 0; i < size; ++i) {
            printf("%02x", *s++);
            if (i % 5 == 0)
                printf("---");
        }
    }

    void fprintChar2Hex(char* s, int size, const char* fname) {
        FILE * file;
        file = fopen(fname, "w");
        for (int i = 0; i < size; ++i) {
            fprintf(file, "%02x", *s++);
            if (i % 5 == 0)
                fprintf(file, "-");
        }
    }

    bool sendBytes(int socket, char* data, int size) {
        // package data to be 1024-aligned
        int aligned_size = ceil(size / 1024.f) * 1024;
        char* buf = new char[aligned_size];
        std::memcpy(buf, data, size);

        int r = send(socket, &aligned_size, sizeof(int), 0);
        if (r <= 0) {
            std::cerr << "sendBytes error: Unable to send aligned_size\n";
            return false;
        }
        r = send(socket, &size, sizeof(int), 0);
        if (r <= 0) {
            std::cerr << "sendBytes error: Unable to send size\n";
            return false;
        }


        int sent_amt = 0;
        while (sent_amt < aligned_size) { // We could allow sending less than `aligned_size`, but this is safer
            r = send(socket, buf + sent_amt, aligned_size - sent_amt, 0);
            if (r == -1) {
                std::cerr << "sendBytes error: Unable to sent (part of) buffer.\n"\
                          << "socket:" << socket << "\n"                \
                          << "sent_amt:" << sent_amt << "\n"\
                          << "aligned_size:" << aligned_size << "\n"\
                          << "size:" << size << "\n";

                return false;
            }
            if (r == 0) {
                std::cerr << "sendBytes warning: Sent nothing (0 byte) of buffer.\n"\
                          << "socket:" << socket << "\n"                \
                          << "sent_amt:" << sent_amt << "\n"\
                          << "aligned_size:" << aligned_size << "\n"\
                          << "size:" << size << "\n";
                return false;
            }
            sent_amt += r;
        }
        return true;
    }

    char* recvBytes(int socket, int& size) {
        // Receive bytes
        int r = 0;
        int aligned_size = 0;
        r = recv(socket, &aligned_size, sizeof(int), 0);
        if (r <= 0) {
            std::cerr << "recvBytes error: Unable to receive aligned_size\n";
        }
        if (aligned_size == 0) {
            std::cerr << "aligned_size is 0.\n";
        }
        if (aligned_size % 1024 != 0) {
            std::cerr << "aligned_size is not 1024-aligned.\n";
        }

        r = recv(socket, &size, sizeof(int), 0);
        if (r <= 0) {
            std::cerr << "recvBytes error: Unable to receive size\n";
        }
        if (size == 0) {
            std::cerr << "size is 0.\n";
        }

        char* data = new char[aligned_size];
        int recv_amt = 0;
        int n_retry = 10;
        int n_try = 0;
        while(recv_amt < size) { // We only require recv_amt to be larger than `size`, not `aligned_size`
            r = recv(socket, data + recv_amt, aligned_size - recv_amt, 0);
            if (r == -1) {
                std::cerr << "recvBytes error: Unable to recv (part of) buffer.\n"\
                          << "socket:" << socket << "\n"                \
                          << "recv_amt:" << recv_amt << "\n"\
                          << "aligned_size:" << aligned_size << "\n"\
                          << "size:" << size << "\n";
            }
            if (r == 0) {
                std::cerr << "recvBytes warning: Recv nothing (0 byte).\n"\
                          << "socket:" << socket << "\n"      \
                          << "recv_amt:" << recv_amt << "\n"\
                          << "aligned_size:" << aligned_size << "\n"\
                          << "size:" << size << "\n";
            }
            recv_amt += r;
            n_try++;
            if (n_try > n_retry)
                break;
        }

#ifdef GASH_DEBUG
        std::cout << "Want to receive " << size << " bytes.\n"\
                  <<"Received " << recv_amt <<" bytes.\n";
#endif

        return data;
    }

    Byte* label_vec_2_byte_array(vector<block>& labels, int& size) {
        size = labels.size() * 16; // 128 / 8 = 16, meaning 16 bytes per block
        Byte* bytes = new Byte[size];
        int i = 0;
        for (auto it = labels.begin(); it != labels.end(); ++it, ++i) {
            // notice operator * returns a reference so doing & on a reference will give you the address
            block* block_ptr = &(*it);
            Byte* block_byte_ptr = (Byte*) block_ptr;
            for (int j = 0; j < 16; ++j) {
                bytes[16 * i + j] = *(block_byte_ptr + j);
            }
        }

        return bytes;
    }

    Byte* int_vec_2_byte_array(vector<int>& selects, int& size) {
        // Note: selects must only contain 0s and 1s
        size = selects.size() / 8; // 1/8 byte per selection bit

        Byte* bytes = new Byte[size];
        int i = 0;
        for (auto it = selects.begin(); it != selects.end(); ++it, ++i) {
            bool s = (bool) *it;
            if (s)
                setBit(&bytes[i / 8], i % 8);
            else
                clearBit(&bytes[i / 8], i % 8);
        }
        return bytes;
    }

    block byte_2_label(Byte* bytes, int start) {
        block label;
        try {
            label = _mm_load_si128((__m128i*)(bytes + start));
         } catch (const std::exception& e) {
            std::cerr << e.what();
            std::cerr << "Bytes might be un-aligned." << "\n";
        }
        return label;
    }
}
