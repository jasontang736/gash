//
// Created by Xiaoting Tang on 10/17/17.
//

#include <cstring>
#include "garbledcircuit.h"
#include "util.h"
#include "garble.h"
namespace GASH_GC {
    GarbledGate* GarbledCircuit::getGarbledGate(ulong i) {
        return this->garbledGates.at(i);
    }

    void GarbledCircuit::addGarbledGate(GarbledGate* gate) {
        this->garbledGates.insert(std::make_pair(gate->id, gate));
    }

    void GarbledCircuit::addGarbledGate(Circuit &circ, ulong id, block row1, block row2, block row3) {
        Gate* g = circ.getGate(id);
        ulong in0 = g->in0->get_id();
        ulong in1 = g->in1->get_id();
        ulong out = g->out->get_id();

        GarbledWireInstance* w_in0;
        GarbledWireInstance* w_in1;
        GarbledWireInstance* w_out;

        if (this->garbledWireInstances.find(in0) == this->garbledWireInstances.end()) {
            w_in0 = new GarbledWireInstance(g->in0);
            this->addGarbledWireInstance(w_in0);
        } else {
            w_in0 = this->garbledWireInstances.find(in0)->second;
        }

        if (this->garbledWireInstances.find(in1) == this->garbledWireInstances.end()) {
            w_in1 = new GarbledWireInstance(g->in1);
            this->addGarbledWireInstance(w_in1);
        } else {
            w_in1 = this->garbledWireInstances.find(in1)->second;
        }

        if (this->garbledWireInstances.find(out) == this->garbledWireInstances.end()) {
            w_out = new GarbledWireInstance(g->out);
            this->addGarbledWireInstance(w_out);
        } else {
            w_out = this->garbledWireInstances.find(out)->second;
        }

        GarbledGate* gg;
        GASH_REQUIRE_GOOD_ALLOC(gg = new GarbledGate(id, false, w_in0, w_in1, w_out));

        EGTT* table = new EGTT(row1, row2, row3);
        gg->table = table;

        this->addGarbledGate(gg);
    }

    GarbledWireInstance* GarbledCircuit::getGarbledWireInstance(ulong i) {
        return this->garbledWireInstances.at(i);
    }

    void GarbledCircuit::addGarbledWireInstance(GarbledWireInstance* w) {
        this->garbledWireInstances.insert(std::make_pair(w->get_id(), w));
    }

    void GarbledCircuit::setGarbledWireLabel(ulong id, block label) {
        this->garbledWireInstances.find(id)->second->set_label(label);
    }

    void GarbledCircuit::initR() {
        this->R = random_R();
    }

    void GarbledCircuit::printSummaryAsGarbler() {
//        int q = this->q;
        GarbledWireInstance *w_in0, *w_in1, *w_out;
        GarbledGate* g;

        std::cout << "R: " << block2dec(this->R) << std::endl;

        for (auto it = this->garbledGates.begin(); it != this->garbledGates.end(); ++it) {
            g = it->second;
            if (!g) {
                LOG(FATAL) << "GarbledGate with output wire id" << g->out->get_id() << "is null";
            }
            w_in0 = g->in0;
            w_in1 = g->in1;
            w_out = g->out;
            std::cout << "Gate " << g->id << ":" << std::endl;
            std::cout << "Is XOR= " << g->isXOR << std::endl;
            std::cout << INDENTx1 << "in0=" << w_in0->get_id() << ":" << std::endl;
            std::cout << INDENTx2 << "label0=" << block2dec(w_in0->label0()) << std::endl;
            std::cout << INDENTx2 << "label1=" << block2dec(w_in0->label1()) << std::endl;
            std::cout << INDENTx1 << "in1=" << w_in1->get_id() << ":" << std::endl;
            std::cout << INDENTx2 << "label0=" << block2dec(w_in1->label0()) << std::endl;
            std::cout << INDENTx2 << "label1=" << block2dec(w_in1->label1()) << std::endl;
            std::cout << INDENTx1 << "out=" << w_out->get_id() << ":" << std::endl;
            std::cout << INDENTx2 << "label0=" << block2dec(w_out->label0()) << std::endl;
            std::cout << INDENTx2 << "label1=" << block2dec(w_out->label1()) << std::endl;
            std::cout << INDENTx1 << "table:" << std::endl;
            std::cout << INDENTx2 << "row1=" << block2dec(g->table->get_row(1));
            std::cout << INDENTx2 << "row2=" << block2dec(g->table->get_row(2));
            std::cout << INDENTx2 << "row3=" << block2dec(g->table->get_row(3));
        }

    }

    void GarbledCircuit::printSummaryAsEvaluator() {
        GarbledGate* g;
        for (auto it = this->garbledGates.begin(); it != this->garbledGates.end(); ++it) {
            g = it->second;
            std::cout << "Gate " << g->id << ":" << std::endl;
            std::cout << INDENTx1 << "isXOR= " << g->isXOR << std::endl;
            if (!g->isXOR) {
                std::cout << INDENTx2 << "table: " << std::endl;
                std::cout << INDENTx2 << "row1=" << block2dec(g->table->get_row(1)) << std::endl;
                std::cout << INDENTx2 << "row2=" << block2dec(g->table->get_row(2)) << std::endl;
                std::cout << INDENTx2 << "row3=" << block2dec(g->table->get_row(3)) << std::endl;
            }
        }
    }

    GarbledCircuit::~GarbledCircuit() {
        for (auto it = this->garbledWireInstances.begin(); it != this->garbledWireInstances.end(); ++it) {
            delete it->second;
        }
        for (auto it = this->garbledGates.begin(); it != this->garbledGates.end(); ++it) {
            delete it->second;
        }
    }
}
