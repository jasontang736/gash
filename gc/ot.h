//
// Created by Xiaoting Tang on 10/16/17.
//

#ifndef GASH_OT_H
#define GASH_OT_H

#include "../vendor/OTExtension/ENCRYPTO_utils/typedefs.h"
#include "../vendor/OTExtension/ENCRYPTO_utils/crypto/crypto.h"
#include "../vendor/OTExtension/ENCRYPTO_utils/socket.h"
#include "../vendor/OTExtension/ot/iknp-ot-ext-snd.h"
#include "../vendor/OTExtension/ot/iknp-ot-ext-rec.h"
#include "../vendor/OTExtension/ot/alsz-ot-ext-snd.h"
#include "../vendor/OTExtension/ot/alsz-ot-ext-rec.h"
#include "../vendor/OTExtension/ot/nnob-ot-ext-snd.h"
#include "../vendor/OTExtension/ot/nnob-ot-ext-rec.h"
#include "../vendor/OTExtension/ot/kk-ot-ext-snd.h"
#include "../vendor/OTExtension/ot/kk-ot-ext-rec.h"
#include "../vendor/OTExtension/ENCRYPTO_utils/cbitvector.h"
#include "../vendor/OTExtension/ot/xormasking.h"
#include "../vendor/OTExtension/ENCRYPTO_utils/rcvthread.h"
#include "../vendor/OTExtension/ENCRYPTO_utils/sndthread.h"
#include "../vendor/OTExtension/ENCRYPTO_utils/channel.h"
#include "../vendor/OTExtension/ENCRYPTO_utils/timer.h"
#include "../vendor/OTExtension/ENCRYPTO_utils/parse_options.h"

#include "common.h"
#include <sys/time.h>
#include <limits.h>
#include <iomanip>

#include <openssl/bn.h>
#include <openssl/rsa.h>
#include <sys/socket.h>

#define OT_PORT 23333
// #define GASH_OT_DEBUG

namespace GASH_GC {

    /* Version 1 OT -- Don't use it */
    void sendOT(int sock, block label0, block label1);
    block recvOT(int sock, int b_select);
    void sendOT(int sock, block label0, block label1, RSA*, BIGNUM*, BIGNUM*);
    block recvOT(int sock, int b_select, BIGNUM*);

    /* Simple send and recv */
    void simpleSend(int sock, block label0, block label1);
    block simpleRecv(int sock, int b);

    /* OT from OTExtension */
//TODO only for debugging purpose!!
    static const char* m_cConstSeed[2] = {"437398417012387813714564100", "15657566154164561"};

    // USHORT		m_nPort = 7766;
    // const char* m_nAddr ;// = "localhost";

    BOOL Init(crypto* crypt);
    BOOL Cleanup();
    BOOL Connect();
    BOOL Listen();

    void InitOTSender(const char* address, int port, crypto* crypt);
    void InitOTReceiver(const char* address, int port, crypto* crypt);

    BOOL ObliviouslyReceive(CBitVector* choices, CBitVector* ret, int numOTs, int bitlength, uint32_t nsndvals, snd_ot_flavor stype, rec_ot_flavor rtype, crypto* crypt);
    BOOL ObliviouslySend(CBitVector** X, int numOTs, int bitlength, uint32_t nsndvals, snd_ot_flavor stype, rec_ot_flavor rtype, crypto* crypt);

    void OTSend(string peer_addr, int peer_ot_port, vector<block>& label0s, vector<block>& label1s);

    void OTRecv(string peer_addr, int peer_ot_port, std::map<ulong, int>& selects, std::unordered_map<ulong, block>& inputLabel);

    // int32_t read_test_options(int32_t* argcp, char*** argvp, uint32_t* role, uint64_t* numots, uint32_t* bitlen,
    //                           uint32_t* secparam, string* address, uint16_t* port, ot_ext_prot* protocol, snd_ot_flavor* sndflav,
    //                           rec_ot_flavor* rcvflav, uint32_t* nthreads, uint32_t* nbaseots, uint32_t* nchecks, uint32_t* N, bool* usemecr, uint32_t* runs);
}
#endif //GASH_OT_H
