//
// Created by Xiaoting Tang on 10/16/17.
//

#ifndef GASH_TCP_H
#define GASH_TCP_H

#include <string>
namespace GASH_GC {
    void buildConnectionAsServer(int port, int& listen_sock, int& peer_sock);
    void buildConnectionAsClient(std::string ip, int port, int& sock);
}
#endif //GASH_TCP_H
