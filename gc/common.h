#ifndef GASH_COMMON_H
#define GASH_COMMON_H

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <unordered_map>
#include <emmintrin.h>
#include <cstring>
#include <sstream>
#include <sys/time.h>
#include <utility>
#include <new>
/* Using names */
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::string;
using std::pair;
using std::make_pair;
using std::ofstream;
using std::stringstream;
using std::strcpy;
using std::copy;
using std::map;

#define RSA_bits 2048
#define LABELSIZE 16  // 16 bytes = 128 bits
// #define GASH_DEBUG true // Comment this to toggle debug messages
#define GASH_NO_OT // Uncomment to use IKNP OT
#define GASH_TIMER true // Comment this to toggle timer
#define getZEROblock() _mm_setzero_si128()

namespace GASH_GC {
    typedef unsigned long ulong;
    typedef __m128i block;
    typedef uint64_t uint64;
    typedef uint32_t uint32;
    typedef uint16_t uint16;
    typedef uint8_t uint8;
    typedef int64_t int64;
    typedef int32_t int32;
    typedef int16_t int16;
    typedef int8_t int8;

    const int INFO = 0;
    const int WARNING = 1;
    const int ERROR = 2;
    const int FATAL = 3;


    class EnvTime {
    public:
        EnvTime() {}
        ~EnvTime() = default;

        static EnvTime& Default() {
            static EnvTime singleton;
            return singleton;
        }

        uint64 NowMicros() {
            struct timeval tv;
            gettimeofday(&tv, nullptr);
            return static_cast<uint64>(tv.tv_sec) * 1000000 + tv.tv_usec;
        }

        uint64 NowSeconds() {
            return NowMicros() / 1000000L;
        }
    };

    class LogMessage : public std::basic_ostringstream<char> {
    public:
        LogMessage(const char* filename, int line, int severity)
            : fname_(filename), line_(line), severity_(severity) {}
        ~LogMessage() {
            GenerateLogMessage();
        }
    protected:
        void GenerateLogMessage() {
            static EnvTime env_time = EnvTime::Default();
            uint64 now_micros = env_time.NowMicros();
            time_t now_seconds = static_cast<time_t>(now_micros / 1000000);
            int32 micros_remainder = static_cast<int32>(now_micros % 1000000);
            const size_t time_buffer_size = 30;
            char time_buffer[time_buffer_size];
            strftime(time_buffer, time_buffer_size, "%Y-%m-%d %H:%M:%S",
                     localtime(&now_seconds));
            fprintf(stderr, "%s.%06d: %c %s:%d] %s\n", time_buffer, micros_remainder,
                    "IWEF"[severity_], fname_, line_, str().c_str());

        }
    private:
        const char* fname_;
        int line_;
        int severity_;
    };

    class LogMessageFatal : public LogMessage {
    public:
        LogMessageFatal(const char* file, int line): LogMessage(file, line, FATAL) {}
        ~LogMessageFatal() {
            GenerateLogMessage();
            abort();
        }
    };


#define _GASH_GC_LOG_INFO                                     \
    ::GASH_GC::LogMessage(__FILE__, __LINE__, GASH_GC::INFO)
#define _GASH_GC_LOG_WARNING                                    \
    ::GASH_GC::LogMessage(__FILE__, __LINE__, GASH_GC::WARNING)
#define _GASH_GC_LOG_ERROR                                    \
    ::GASH_GC::LogMessage(__FILE__, __LINE__, GASH_GC::ERROR)
#define _GASH_GC_LOG_FATAL                          \
    ::GASH_GC::LogMessageFatal(__FILE__, __LINE__)

#define LOG(severity) _GASH_GC_LOG_##severity

    template <typename Arg, typename... Args>
    void doPrint(std::ostream& out, Arg&& arg, Args&&... args)
    {
        out << std::forward<Arg>(arg);
        using expander = int[];
        (void)expander{0, (void(out << ' ' << std::forward<Args>(args)),0)...};
    }

#define GASH_REQUIRE_NOT_NULL_w_MSG(expr, ...)  \
    if (!expr) {                                \
        doPrint(LOG(FATAL), __VA_ARGS__);       \
    }                                           \

#define GASH_REQUIRE_GOOD_ALLOC(expr)           \
    try                                         \
    {                                           \
        expr;                                   \
    }                                           \
    catch(std::bad_alloc& exc)                  \
    {                                           \
        LOG(FATAL) << "Memory exhausted.";      \
    }                                           \


}

#endif
