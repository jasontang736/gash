//
// Created by Xiaoting Tang on 10/17/17.
//

#include "util.h"
#include "outputmap.h"
#include "labelpair.h"
namespace GASH_GC {
    int OutputMap::find(int g, block label) {
        /**
         * Return the associated bit with `label` in gate `g`.
         */
        LabelPair *labelPair = this->outputmap.find(g)->second;
        block label0 = labelPair->label0;
        block label1 = labelPair->label1;
        if (blockEq(label0, label)) {
            return 0;
        } else if (blockEq(label1, label)) {
            return 1;
        } else {
            std::cerr << "Unable to find associated label with gate " << g << std::endl;
            std::cerr << "Requestd label: " << block2dec(label) << std::endl;
            std::cerr << "Existing label0: " << block2dec(label0) << std::endl;
            std::cerr << "Existing label1: " << block2dec(label1) << std::endl;
            exit(-1);
        }
    }

    void OutputMap::add(long g, block label, int semantic) {

        if (this->outputmap.find(g) == this->outputmap.end()) {

            LabelPair *pair = new LabelPair;
            if (semantic == 0)
                pair->label0 = label;
            else
                pair->label1 = label;
            this->outputmap.insert(std::make_pair<long, LabelPair *>((long &&) g, (LabelPair *&&) pair));

        } else {

            LabelPair *pair;
            pair = this->outputmap.find(g)->second;
            pair->setLabelWithSemantic(label, semantic);

        }
    }
}
