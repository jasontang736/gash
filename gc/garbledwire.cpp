//
// Created by Xiaoting Tang on 10/17/17.
//

#include "util.h"
#include "garble.h"
namespace GASH_GC {
    // --------- new -----------
    void GarbledWireInstance::garble(block R) {
        block label0 = random_block();
        block label1 = xor_block(label0, R);
        this->set_label0(label0);
        this->set_label1(label1);
#ifdef GASH_DEBUG
        std::cout << "Creating a new label0 = " << block2hex(label0) << std::endl;
        std::cout << "label1 = label0 ^ R = " << block2hex(label1) << std::endl;
#endif
    }

    block GarbledWireInstance::origLabelWithSemantic(int i) {
        // Return the original label, despite of its invertibility.
        if (!this->inverted()) {
            return i == 0 ? this->label0() : this->label1();
        } else {
            return i == 0 ? this->label1() : this->label0();
        }
    }

    block GarbledWireInstance::labelWithSemantic(int i) {
        if (this->inverted())
            return i == 0 ? this->label1() : this->label0();
        else
            return i == 0 ? this->label0() : this->label1();
    }

    block GarbledWireInstance::labelWithLSB(int lsb) {
        int lsb_label0 = getLSB(this->label0());
        int lsb_label1 = getLSB(this->label1());

        if (lsb_label0 == lsb_label1) {
            LOG(FATAL) << "GarbledWireInstance's two labels have the same LSB.";
        }
        if (lsb != 0 && lsb != 1) {
            LOG(FATAL) << "Invalid queried LSB (either 1 or 0): " << lsb;
        }
        if (lsb_label0 == lsb) {
            return this->label0();
        } else {
            return this->label1();
        }
    }

    int GarbledWireInstance::semanticWithLSB(int lsb) {
        int lsb_label0 = getLSB(this->label0());
        int lsb_label1 = getLSB(this->label1());

        if (lsb_label0 == lsb_label1) {
            LOG(FATAL) << "GarbledWireInstance's two labels have the same LSB.";
        }
        if (lsb != 0 && lsb != 1) {
            LOG(FATAL) << "Invalid queried LSB (either 1 or 0): " << lsb;
        }
        if (lsb_label0 == lsb) {
            return 0;
        } else {
            return 1;
        }
    }

    void GarbledWireInstance::setLabelWithSemantic(block label, int semantic) {
        if (semantic != 0 && semantic != 1) {
            LOG(FATAL) << "Invalid semantic (either 1 or 0): " << semantic;
        }
//        if (semantic == 0) {
//            if (this->inverted())
//                this->set_label1(label);
//            else
//                this->set_label0(label);
//        } else {
//            if (this->inverted())
//                this->set_label0(label);
//            else
//                this->set_label1(label);
//        }
        if (semantic == 0) {
            this->set_label0(label);
        } else {
            this->set_label1(label);
        }
    }
}
