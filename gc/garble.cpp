#include "garble.h"
#include "garbledwire.h"
#include "wire.h"
#include "circuit.h"
#include "garbledgate.h"
#include "common.h"
#include "proto/output_map.pb.h"
#include "proto/garbledcircuit.pb.h"
#include <string>

namespace GASH_GC {
    static ulong gateCount = 0;
    static const uint8_t AESkey_init[64] =
    {
        105, 187, 117, 137,
        65, 140, 104, 185,
        74, 49, 246, 13,
        246, 67, 88, 55,
        229, 178, 210, 121,
        195, 44, 80, 195,
        184, 111, 175, 138,
        56, 18, 248, 26,
        203, 123, 54, 238,
        95, 101, 16, 236,
        161, 238, 111, 155,
        74, 190, 115, 80,
        127, 40, 15, 186,
        77, 65, 117, 27,
        96, 74, 94, 115,
        18, 77, 79, 132
    };
    static const block AESkey = _mm_load_si128((const __m128i *) AESkey_init);

    ulong nextGateId() {
        return gateCount++;
    }

    GarbledGate* newGarbledGate(ulong id, GarbledWireInstance* w_a, GarbledWireInstance* w_b, GarbledWireInstance* w_c) {
        GarbledGate* gg = new GarbledGate;
        gg->id = id;
        gg->in0 = w_a;
        gg->in1 = w_b;
        gg->out = w_c;
        gg->isXOR = true;
        return gg;
    }

    GarbledGate* newGarbledGate(ulong id, GarbledWireInstance* w_a, GarbledWireInstance* w_b, GarbledWireInstance* w_c, EGTT* table) {
        GarbledGate* gg = new GarbledGate;
        gg->id = id;
        gg->in0 = w_a;
        gg->in1 = w_b;
        gg->out = w_c;
        gg->isXOR = false;
        gg->table = table;
        return gg;
    }

    void garbleCircuit(Circuit &circuit, GarbledCircuit &gc, std::unordered_map<ulong, block> &input_label0s,
                       std::unordered_map<ulong, block> &input_label1s, OutputMapProto &outputMap, std::set<ulong> outputWireIds,
                       std::map<ulong, block> &label0s, std::map<ulong, block> &label1s) {
        /** Assuming the previous n wires are input wires.
         *  Use optimizations of Point-and-Permute, Row Reduction and Free XOR.
         */

        block table[4];        // The first entry will always be NULL
        block label0;
        block label1;
        block label;

        WireInstance* w;
        Gate* g;
        GarbledWireInstance *gw;

        WireInstance* w_a;
        WireInstance* w_b;
        WireInstance* w_c;
        GarbledWireInstance *gw_a;
        GarbledWireInstance *gw_b;
        GarbledWireInstance *gw_c;
        int func;

        GarbledGate* gg;

        ulong i;
        int first_row_semantic;

        block ZERO = getZEROblock();

        block tweak;

        gc.initR();

#ifdef GASH_DEBUG
        std::cout << "R: " << block2hex(gc.R) << std::endl;
        std::cout << "R: " << block2hex(gc.R) << std::endl;
#endif

        for (auto it = circuit.input_ids.begin(); it != circuit.input_ids.end(); ++it) {
            w = circuit.getWireInstance(*it);
            gw = new GarbledWireInstance(w);
            gw->set_id(w->get_id());

            gw->garble(gc.R);
            gc.addGarbledWireInstance(gw);  // Use a map: id->vector<GarbledWireInstance>
            input_label0s.insert(std::make_pair(w->get_id(), gw->label0()));
            input_label1s.insert(std::make_pair(w->get_id(), gw->label1()));

#ifdef GASH_DEBUG
            label0s.insert(std::make_pair(w->get_id(), gw->label0()));
            label1s.insert(std::make_pair(w->get_id(), gw->label1()));

            std::cout << "Garbling for wire " << w->get_id() << std::endl;
            std::cout << "label0:" << block2hex(gw->label0());
            std::cout << "label1:" << block2hex(gw->label1()) << std::endl;
#endif
        }

        for (auto it = circuit.gates_.begin(); it != circuit.gates_.end(); ++it) {

            g = it->second;
            i = g->get_id();
            w_a = g->in0;
            w_b = g->in1;
            w_c = g->out;
            func = g->func;

            // Use the same id to get garbled wire
            gw_a = gc.getGarbledWireInstance(w_a->get_id());
            gw_b = gc.getGarbledWireInstance(w_b->get_id());
            gw_c = new GarbledWireInstance(w_c);
//            gw_c->set_id(i);
//            label_a_0 = gw_a->label0();
//            label_a_1 = gw_a->label1();
//            label_b_0 = gw_b->label0();
//            label_b_1 = gw_b->label1();

#ifdef GASH_DEBUG
            std::cout << "Garbling gate" << i << std::endl;
            std::cout << "R:" << block2hex(gc.R) << std::endl;
#endif

            if (func == XOR_FUNC) { // XOR gate
                // For XOR gate, we only xor the input labels
                label0 = xor_block(gw_a->label0(), gw_b->label0());
                gw_c->set_label0(label0);

                label1 = xor_block(label0, gc.R);
                gw_c->set_label1(label1);
                gc.addGarbledWireInstance(gw_c);

#ifdef GASH_DEBUG
                label0s.insert(std::make_pair(gw_c->get_id(), label0));
                label1s.insert(std::make_pair(gw_c->get_id(), label1));

                std::cout << "XORing wire_a.label0(0):" << block2hex(gw_a->label0()) << \
                    ", wire_a.ID: " << gw_a->get_id() <<\
                    " and \nwire_b.label0(0):" << block2hex(gw_b->label0()) << \
                    ", wire_b.ID: " << gw_b->get_id() << \
                    "\n = " << block2hex(gw_c->label0()) << \
                    ", wire_c.ID: " << gw_c->get_id() << std::endl;
                std::cout << "Label for semantic 0 of wire_c =" << block2hex(gw_c->label0()) << std::endl;
                std::cout << "Label for semantic 1 of wire_c =" << block2hex(gw_c->label1()) << std::endl;
#endif
                // Create the garbled gate without truth table (because we don't need)
                gg = newGarbledGate(i, gw_a, gw_b, gw_c);
            } else {

                // Construct the tweak
                tweak = newTweak(i);

                // Now we want to know the semantic value of the first row
                first_row_semantic = eval_bit(gw_a->semanticWithLSB(0),
                                              gw_b->semanticWithLSB(0), func);

                // Computethe Wire c' label of the first row by encryption
                label = encrypt(gw_a->labelWithLSB(0),
                                                   gw_b->labelWithLSB(0),
                                                   tweak,
                                                   ZERO,
                                                   AESkey);

                gw_c->setLabelWithSemantic(label, first_row_semantic);
                gw_c->setLabelWithSemantic(xor_block(label, gc.R), first_row_semantic ^ 1);

                label0 = gw_c->labelWithSemantic(0);
                label1 = gw_c->labelWithSemantic(1);

#ifdef GASH_DEBUG
                std::cout << "Computing wire_c's labels with ID:" << gw_c->get_id() << std::endl;
                std::cout << "Encrypting gate " << i << " with wire_a.ID:" << gw_a->get_id() <<\
                    ", with wire_a.label0():" << block2hex(gw_a->label0()) <<\
                    ", with wire_a.label1():" << block2hex(gw_a->label1()) <<\
                    ", with wire_b.ID:" << gw_b->get_id() <<\
                    ", with wire_b.label0():" << block2hex(gw_b->label0()) <<\
                    ", with wire_b.label1():" << block2hex(gw_b->label1()) << std::endl;

                std::cout << "Tweak:"  << block2hex(tweak) << std::endl;
                std::cout << "first_row_semantic:" << first_row_semantic << std::endl;
                std::cout << "gw_c->label" << first_row_semantic << ":" << "encrypt(gw_a->labelWithLSB(0):"\
                          << block2hex(gw_a->labelWithLSB(0)) << ", gw_b->labelWithLSB(0):" << block2hex(gw_b->labelWithLSB(0))\
                          << ", tweak:" << block2hex(tweak)\
                          << ", ZERO" << block2hex(ZERO)\
                          << "AESkey:" << block2hex(AESkey)\
                          << "gw_c->label" << (first_row_semantic) << ": " << block2hex(gw_c->labelWithSemantic(first_row_semantic))\
                          << "gw_c->label" << (1 - first_row_semantic) << ": " << block2hex(gw_c->labelWithSemantic(first_row_semantic ^ 1))\
                          << std::endl;

                std::cout << "gw_a->inverted: " << (gw_a->inverted() ? "true" : "false") << std::endl;
                std::cout << "gw_b->inverted: " << (gw_b->inverted() ? "true" : "false") << std::endl;
                std::cout << "gw_c->inverted: " << (gw_c->inverted() ? "true" : "false") << std::endl;
#endif

                gc.addGarbledWireInstance(gw_c);

                // -------- Handle Garbled Truth Table ----------- //
                table[1] =
                    encrypt(gw_a->labelWithLSB(1),
                            gw_b->labelWithLSB(0),
                            tweak,
                            gw_c->labelWithSemantic(eval_bit(gw_a->semanticWithLSB(1),
                                                             gw_b->semanticWithLSB(0),
                                                             func)),
                            AESkey);
                table[2] =
                    encrypt(gw_a->labelWithLSB(0),
                            gw_b->labelWithLSB(1),
                            tweak,
                            gw_c->labelWithSemantic(eval_bit(gw_a->semanticWithLSB(0),
                                                             gw_b->semanticWithLSB(1),
                                                             func)),
                            AESkey);
                table[3] =
                    encrypt(gw_a->labelWithLSB(1),
                            gw_b->labelWithLSB(1),
                            tweak,
                            gw_c->labelWithSemantic(eval_bit(gw_a->semanticWithLSB(1),
                                                             gw_b->semanticWithLSB(1),
                                                             func)),
                            AESkey);
#ifdef GASH_DEBUG
                std::cout << "table[1]=encrypt("\
                          << "gw_a-" << gw_a->get_id() << "(" << gw_a->semanticWithLSB(1)  << "): " << block2hex(gw_a->labelWithLSB(1)) << ", "\
                          << "gw_b-" << gw_b->get_id() << "(" << gw_b->semanticWithLSB(0) << "): " << block2hex(gw_b->labelWithLSB(0)) << ", "\
                          << block2hex(tweak) << ", "\
                          << block2hex(gw_c->labelWithSemantic(eval_bit(gw_a->semanticWithLSB(1),\
                                                                        gw_b->semanticWithLSB(0),\
                                                                        func))) << ")" << std::endl;
                std::cout << "i.e. gw_c->labelWithSemantic(eval_bit(" << gw_a->semanticWithLSB(1)\
                          << "," << gw_b->semanticWithLSB(0) << ", " << func << "))" << std::endl;
                std::cout << "=" << block2hex(table[1]) << std::endl;

                std::cout << "table[2]=encrypt("\
                          << "gw_a-" << gw_a->get_id() << "(" << gw_a->semanticWithLSB(0) << "): " << block2hex(gw_a->labelWithLSB(0)) << ", "\
                          << "gw_b-" << gw_b->get_id() << "(" << gw_b->semanticWithLSB(1) << "): " << block2hex(gw_b->labelWithLSB(1)) << ", "\
                          << block2hex(tweak) << ", "\
                          << block2hex(gw_c->labelWithSemantic(eval_bit(gw_a->semanticWithLSB(0),\
                                                                        gw_b->semanticWithLSB(1),\
                                                                        func))) << std::endl;
                std::cout << "i.e. gw_c->labelWithSemantic(eval_bit(" << gw_a->semanticWithLSB(0)\
                          << "," << gw_b->semanticWithLSB(1) << ", " << func << "))" << std::endl;
                std::cout << "=" << block2hex(table[2]) << std::endl;

                std::cout << "table[3]=encrypt("\
                          << "gw_a-" << gw_a->get_id() << "(" << gw_a->semanticWithLSB(1) << "): " << block2hex(gw_a->labelWithLSB(1)) << ", "\
                          << "gw_b-" << gw_b->get_id() << "(" << gw_b->semanticWithLSB(1) << "): " << block2hex(gw_b->labelWithLSB(1)) << ", "\
                          << block2hex(tweak) << ", "\
                          << block2hex(gw_c->labelWithSemantic(eval_bit(gw_a->semanticWithLSB(1),\
                                                                        gw_b->semanticWithLSB(1),\
                                                                        func))) << std::endl;
                std::cout << "i.e. gw_c->labelWithSemantic(eval_bit(" << gw_a->semanticWithLSB(1)\
                          << "," << gw_b->semanticWithLSB(1) << ", " << func << "))" << std::endl;
                std::cout << "=" << block2hex(table[3]) << std::endl;
#endif


                // Create the garbled gate with garbled truth table
                EGTT* egtt = new EGTT(table[1], table[2], table[3]);
                gg = newGarbledGate(i, gw_a, gw_b, gw_c, egtt);
            }

#ifdef GASH_DEBUG
            label0s.insert(std::make_pair(gw_c->get_id(), label0));
            label1s.insert(std::make_pair(gw_c->get_id(), label1));
#endif
            gc.addGarbledGate(gg);

            if (outputWireIds.find(w_c->get_id()) != outputWireIds.end()) {
                // Handles output wires
                OutputMapProto_LabelSemanticProto* label0 = new OutputMapProto_LabelSemanticProto;
                label0->set_label_first_half(get_block_first_half(gw_c->label0()));
                label0->set_label_second_half(get_block_second_half(gw_c->label0()));

                OutputMapProto_LabelSemanticProto* label1 = new OutputMapProto_LabelSemanticProto;
                label1->set_label_first_half(get_block_first_half(gw_c->label1()));
                label1->set_label_second_half(get_block_second_half(gw_c->label1()));

                OutputMapProto_LabelPairProto labelPair;
                labelPair.set_allocated_label0(label0);
                labelPair.set_allocated_label1(label1);
                (*outputMap.mutable_wire_values())[w_c->get_id()] = labelPair;

#ifdef GASH_DEBUG
                std::cout << "Outputmap adding Wire " << w_c->get_id() << "-label0: " << block2hex(gw_c->label0()) << std::endl;
                std::cout << "Outputmap adding Wire " << w_c->get_id() << "-label1: " << block2hex(gw_c->label1()) << std::endl;
#endif
            }
        }
    }

    void evaluateGarbledCircuit(GarbledCircuit &gc, Circuit &c, GarbledCircuitProto& gc_pb, std::unordered_map<ulong, block> &input_label,
                                OutputMapProto &outputMap, BitMapProto &output, std::set<ulong> &outputWires) {

        block tweak;
        // int n = c.n;
        // int q = c.q;
        block result;

        GarbledWireInstance *gw_a, *gw_b, *gw_c;
        GarbledGate *g;

        ulong i;
        int select;

        for (auto it = c.input_ids.begin(); it != c.input_ids.end(); ++it) {

            i = *it;
            gc.setGarbledWireLabel(i, input_label[i]);

#ifdef GASH_DEBUG
            result = input_label[i];
            GarbledCircuitProto_Block * ggb = gc_pb.mutable_wirelabels()->Add();
            ggb->set_id(i);
            ggb->set_least_half(get_block_first_half(result));
            ggb->set_great_half(get_block_second_half(result));

            std::cout << "Setting label for garbledWire with ID" << i << ":" << block2hex(input_label[i]);
#endif

        }
        for (auto it = gc.garbledGates.begin(); it != gc.garbledGates.end(); ++it) {

            i = it->second->id;
            g = gc.getGarbledGate(i);
            gw_a = g->in0;
            gw_b = g->in1;
            gw_c = g->out;

#ifdef GASH_DEBUG
            std::cout << "gw_a " << gw_a->get_id() << ", address:" << gw_a << std::endl;
            std::cout << "gw_b " << gw_b->get_id() << ", address:" << gw_b << std::endl;
            std::cout << "gw_c " << gw_c->get_id() << ", address:" << gw_c << std::endl;

            std::cout << "Processing gate" << i << std::endl;
#endif
            if (g->isXOR) {

                result = xor_block(gw_a->label(), gw_b->label());
                gw_c->set_label(result);

#ifdef GASH_DEBUG
                std::cout << "XORing gw_a:" << block2hex(gw_a->label()) << " and gw_b:" << block2hex(gw_b->label()) << " = "
                          << block2hex(result) << std::endl;
#endif
            } else {

                select = getLSB(gw_a->label()) + (getLSB(gw_b->label()) << 1);
                tweak = newTweak(g->id);
                if (select == 0) {
                    result = encrypt(gw_a->label(), gw_b->label(), tweak, getZEROblock(), AESkey);
                    gw_c->set_label(result);
#ifdef GASH_DEBUG
                    std::cout << "Encrypting gw_a:" << block2hex(gw_a->label()) \
                              << " and gw_b:" << block2hex(gw_b->label())\
                              << "with tweak" << block2hex(tweak)\
                              << "for ZERO" << block2hex(getZEROblock())\
                              << "AESkey:" << block2hex(AESkey)\
                              << " = " << block2hex(result)\
                              << std::endl;
                    std::cout << "gw_c->inverted:" << gw_c->inverted() << std::endl;
#endif

                } else {
                    result = encrypt(gw_a->label(), gw_b->label(), tweak, g->table->get_row(select), AESkey);
                    gw_c->set_label(result);
#ifdef GASH_DEBUG
                    std::cout << "Encrypting gw_a:" << block2hex(gw_a->label())\
                              << " and gw_b:" << block2hex(gw_b->label())\
                              << "with tweak" << block2hex(tweak) \
                              << "for g.table[" << select << "]:" << block2hex(g->table->get_row(select))\
                              << " = " << block2hex(result) << std::endl;
#endif
                }

            }

#ifdef GASH_DEBUG
            GarbledCircuitProto_Block * ggb = gc_pb.mutable_wirelabels()->Add();
            ggb->set_id(gw_c->get_id());
            ggb->set_least_half(get_block_first_half(result));
            ggb->set_great_half(get_block_second_half(result));
#endif

            if (outputWires.find(gw_c->get_id()) != outputWires.end()) {
                int s = find_semantic_of_label_from_pair(gw_c->get_id(), outputMap, gw_c->label());
                (*output.mutable_bits())[gw_c->get_id()] = s;
            }
        }
    }

    void buildCircuit(std::string fname, Circuit &circuit) {
        /* Build a circuit from a pre-topologically-sorted circ file
         */
        std::ifstream inFile(fname);
        if (!inFile.is_open()) {
            std::cerr << "Unable to open file " << fname << std::endl;
            exit(1);
        }

        std::string line;
        int numVar = 0, numIn = 0, numOut = 0, numAND = 0, numOR = 0, numXOR = 0, numDFF = 0;
        int op;
        int wcount = 0;
        std::map<ulong, bool> wires_invertibility;
        // Note: id = floor(idx/2). The parity in idx represents whether the wire is inverted or not.
        ulong out_id, in0_id, in1_id, out_idx, in0_idx, in1_idx;

        while (getline(inFile, line)) {

            std::vector<std::string> items = split(line, ' ');
            if (items[0].find('#') != std::string::npos)  // Skip directives
                continue;
            switch (items.size()) {
            case 8: {
                numVar = stoi(items[1]);
                numIn = stoi(items[2]);
                numOut = stoi(items[3]);
                numAND = stoi(items[4]);
                numOR = stoi(items[5]);
                numXOR = stoi(items[6]);
                numDFF = stoi(items[7]);

                circuit.n = numIn;
                circuit.m = numOut;
                circuit.q = numAND + numOR + numXOR;
                break;
            }
            case 1: {
                std::vector<std::string> id_items = split(items[0], ':');
                if (id_items.size() == 1) {
                    ulong wire_idx = stol(items[0]);
                    ulong wire_id = wire_idx / 2;
                    if (wcount < numIn) {
                        if (is_odd(wire_idx)) {
                            LOG(FATAL) << "Declaring circuit input wires as inverted is not allowed.";
                        }
                        createInputWireInstance(wire_id, circuit);
                        circuit.input_ids.push_back(wire_id);
                    }
                    else {
                        if (is_odd(wire_idx))
                            createOutputWireInstance(wire_id, true, circuit);
                        else
                            createOutputWireInstance(wire_id, false, circuit);
                        circuit.output_ids.push_back(wire_id);
                    }
                    wcount++;
                } else {
                    // Output wire is constant
                    ulong wire_idx = stol(id_items[0]);
                    ulong wire_id = wire_idx / 2;
                    bool v = (bool) stoi(id_items[1]);
                    if (wcount < numIn) {
                        LOG(FATAL)  << "Declaring circuit input wires as constants is not allowed.";
                    }
                    circuit.output_ids.push_back(wire_id);
                    circuit.output_constants.insert(std::make_pair(wire_id, v));
                }
                break;
            }
            case 4: {
                op = stoi(items[1]);
                out_idx = stol(items[0]);
                out_id = out_idx/2;   // Divided by 2 to obtain the id
                in0_idx = stol(items[2]);
                in0_id = in0_idx/2;
                in1_idx = stol(items[3]);
                in1_id = in1_idx/2;

                if (is_odd(out_idx)) {
                    LOG(FATAL) << "Fatal error in reading wire: " << out_idx <<\
                        "Output wire should always have even id. Use odd id in input wires.";
                }

                auto in0_it = wires_invertibility.find(in0_id);
                bool in0_inverted = is_odd(in0_idx);
                if (in0_it == wires_invertibility.end()) {
                    wires_invertibility.insert(std::make_pair(in0_id, in0_inverted));
                } else if ((*in0_it).second != in0_inverted) {
                    LOG(FATAL) << "Fatal error in reading wire: " << in0_idx << \
                        "\nCannot use a wire in both inverted and uninverted state. Duplicate the gate instead.\n";
                }

                auto in1_it = wires_invertibility.find(in1_id);
                bool in1_inverted = is_odd(in1_idx);
                if (in1_it == wires_invertibility.end()) {
                    wires_invertibility.insert(std::make_pair(in1_id, in1_inverted));
                } else if ((*in1_it).second != in1_inverted) {
                    LOG(FATAL) << "Fatal error in reading wire: " << in1_idx << \
                        "\nCannot use a wire in both inverted and uninverted state. Duplicate the gate instead.\n";
                }

                createGate(out_id, op, in0_id, in0_inverted, in1_id, in1_inverted, circuit);
                break;
            }
            default: {
                break;
            }
            }
        }
#ifdef GASH_DEBUG
        std::cout << numVar << " variables" << std::endl
                  << numIn << " inputs" << std::endl
                  << numOut << " outputs" << std::endl
                  << numAND << " AND gates" << std::endl
                  << numOR << " OR gates" << std::endl
                  << numXOR << " XOR gates" << std::endl
                  << numDFF << " DFF gates" << std::endl;
#endif
    }

    void executeCircuit(Circuit& circ, std::map<ulong, int>& input_values, std::vector<ulong>& output_ids, std::map<ulong, int>& output_values) {

        std::set<ulong> output_ids_set;
        for (auto it = output_ids.begin(); it != output_ids.end(); ++it) {
            output_ids_set.insert(*it);
        }

        WireInstance* w;
        WireInstance* w_in0;
        WireInstance* w_in1;
        WireInstance* w_out;
        Gate* g;
        for (auto it = circ.input_ids.begin(); it != circ.input_ids.end(); ++it) {
            w = circ.getWireInstance(*it);

            auto bit = input_values.find(*it);
            if (bit == input_values.end()) {
                std::cerr << "Cannot find values for input wire " << *it << std::endl;
                abort();
            }
            int b = bit->second;
            w->set_value(b);
        }

        for (auto it = circ.gates_.begin(); it != circ.gates_.end(); ++it) {
            g = it->second;
            w_in0 = g->in0;
            w_in1 = g->in1;
            w_out = g->out;

            if (w_in0->get_value() < 0) {
                std::cerr << "Input Wire " << w_in0->get_id() << " of Gate " << g->get_id() << " has value of  -1." << std::endl;
                abort();
            }
            if (w_in1->get_value() < 0) {
                std::cerr << "Input Wire " << w_in1->get_id() << " of Gate " << g->get_id() << " has value of  -1." << std::endl;
                abort();
            }

            int v_in0 = w_in0->get_value();
            int v_in1 = w_in1->get_value();
            int func = g->func;
            int v_out = eval_bit(v_in0, v_in1, func);

            w_out->set_value(v_out);

            ulong out = w_out->get_id();

            if (output_ids_set.find(out) != output_ids_set.end()) {
                output_values.insert(std::make_pair(out, v_out));
            }
        }
    }

    void createWireInstance(ulong id, bool inverted, Circuit & circuit) {
        WireInstance* w = new WireInstance(id, inverted);
        circuit.addWireInstance(w);
    }

    void createInputWireInstance(ulong id, Circuit &circuit) {
        createWireInstance(id, false, circuit);
    }

    void createOutputWireInstance(ulong id, bool inverted, Circuit &circuit) {
        createWireInstance(id, inverted, circuit);
        circuit.outputWireIds_.insert(id);
    }

    void createGate(ulong out, int func, ulong in0, bool in0_inverted, ulong in1, bool in1_inverted, Circuit &circuit) {
        if (!circuit.hasGate(out)) {    // The output wire is already created, i.e. it is also the circuit output wire
            createWireInstance(out, false, circuit);
        }

        WireInstance* w_out = circuit.getWireInstance(out);
        WireInstance* w_in0 = circuit.getWireInstance(in0);
        if (in0_inverted != w_in0->inverted()) {
            w_in0->set_inverted(in0_inverted);
        }
        WireInstance* w_in1 = circuit.getWireInstance(in1);
        if (in1_inverted != w_in1->inverted()) {
            w_in1->set_inverted(in1_inverted);
        }

        Gate* g = new Gate(nextGateId(), func, w_in0, w_in1, w_out);
        circuit.addGate(g);
    }

    void createGate(const ulong & out, const int & func, const ulong & in0, const bool & in0_inverted, const ulong & in1, const bool & in1_inverted, Circuit & circuit) {
        if (!circuit.hasGate(out)) {
            // Note: all output wires are created with inverted=false, and they have 1 chance to change it.
            createWireInstance(out, false, circuit);
        }
        else {
            LOG(FATAL) << "Error in creating Gate for output wire: " << out << \
                ".\n Output wire already exists.\n";
        }
        WireInstance* w_out = circuit.getWireInstance(out);
        WireInstance* w_in0 = circuit.getWireInstance(in0);
        WireInstance* w_in1 = circuit.getWireInstance(in1);
        if (!w_out)
            LOG(FATAL) << "WireInstance of wire: " << out << ", is null.";
        if (!w_in0)
            LOG(FATAL) << "WireInstance of wire: " << in0 << ", is null.";
        if (!w_in1)
            LOG(FATAL) << "WireInstance of wire: " << in1 << ", is null.";

        Gate* g;
        GASH_REQUIRE_GOOD_ALLOC(g = new Gate(nextGateId(), func, w_in0, w_in1, w_out));
        circuit.addGate(g);
    }
}
