//
// Created by Xiaoting Tang on 10/8/17.
//
// Usage:
// ./gash_gc --circuit/-c <input circuit file> (--alice/-a --port/-p <port>| --bob/-b --peer-ip/-P <ip> --port/-p <port>)
//           --input/-i <input file> --output/-o <output file>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <iostream>
#include "garbler.h"
#include "evaluator.h"
#include "util.h"

int main(int argc, const char* argv[]) {
    bool alice, bob;
    po::options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "produce help message")
            ("circuit,c", po::value<std::string>(), "input circuit file")
            ("alice,a", po::bool_switch(&alice), "act as garbler(Alice)")
            ("bob,b", po::bool_switch(&bob), "act as evaluator(Bob)")
            ("port,p", po::value<int>(), "Alice: listening port | Bob: peer port")
            ("peer-ip,P", po::value<std::string>(), "Bob: peer ip")
            ("input,i", po::value<std::string>(), "Input file")
            ("output,o", po::value<std::string>(), "Output file");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << "Usage: options description [options]\n";;
        std::cout << desc;
        return 0;
    }

    if (alice == bob) {
        std::cerr << "Cannot be both garbler and evaluator." << std::endl;
        return -1;
    }

    GASH_GC::srand_sse(time(NULL));

    if (alice) {
        GASH_GC::Garbler garbler(vm["port"].as<int>());
        garbler.buildCircuit(vm["circuit"].as<std::string>());
        garbler.readInput(vm["input"].as<std::string>());
        garbler.garbleCircuit();
        garbler.print();
        garbler.buildConnection();
        garbler.sendGarbledTables();
        garbler.sendSelfLabels();
        garbler.sendPeerLabels();
        garbler.sendOutputMap();
        garbler.recvOutput();
        garbler.descOutput();
    } else if (bob) {
        GASH_GC::Evaluator evaluator(vm["peer-ip"].as<std::string>(), vm["port"].as<int>());
        evaluator.buildCircuit(vm["circuit"].as<std::string>());
        evaluator.readInput(vm["input"].as<std::string>());
        evaluator.buildConnection();
        evaluator.recvGarbledTables();
        evaluator.recvPeerLabels();
        evaluator.recvSelfLabels();
        evaluator.recvOutputMap();
        evaluator.garbleCircuit();
        evaluator.print();
        evaluator.evaluateCircuit();
        evaluator.sendOutput();
        evaluator.descOutput();
    }
}
