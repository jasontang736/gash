//
// Created by Xiaoting Tang on 10/16/17.
//

#include <inttypes.h>
#include "ot.h"
#include "util.h"

#ifndef MSG_MORE
#define MSG_MORE 0
#endif

//#define GASH_OT_DEBUG
namespace GASH_GC {

    /* Version 1 OT - Don't use it */
    void sendOT(int sock, block label0, block label1) {

        int ret = 0;
        RSA *r = NULL;
        BIGNUM *bn_x0 = BN_new();
        BIGNUM *bn_x1 = BN_new();
        BN_rand(bn_x0, RSA_bits, 0, 0);
        int len = BN_num_bytes((const BIGNUM *) bn_x0);
        unsigned char x0_bin[len / sizeof(unsigned char)];
        unsigned char x1_bin[len / sizeof(unsigned char)];
        BIGNUM *bn_e = NULL;
        BIGNUM *bn_n = NULL;
        BIGNUM *bn_d = NULL;
#ifdef GASH_OT_DEBUG
        BIGNUM *bn_p = NULL;
        BIGNUM *bn_q = NULL;
#endif
        unsigned long e = 3;

        bn_e = BN_new();
        ret = BN_set_word(bn_e, e);
        if (ret != 1) {
            std::cerr << "BN_set_word() failed." << std::endl;
            exit(-1);
        }

        r = RSA_new();
        ret = RSA_generate_key_ex(r, RSA_bits, bn_e, NULL);
        if (!ret) {
            std::cerr << "RSA_generate_key_ex() failed." << std::endl;
            exit(-1);
        }

        bn_n = r->n;
        bn_d = r->d;
#ifdef GASH_OT_DEBUG
        bn_p = r->p;
        bn_q = r->q;
#endif

        int e_size = BN_num_bytes((const BIGNUM *) bn_e);
        int n_size = BN_num_bytes((const BIGNUM *) bn_n);
        unsigned char e_bin[e_size / sizeof(unsigned char)];
        unsigned char n_bin[n_size / sizeof(unsigned char)];

        BN_bn2bin((const BIGNUM *) bn_e, e_bin);
        BN_bn2bin((const BIGNUM *) bn_n, n_bin);

        // Send Public Key
        send(sock, &e_size, sizeof(int), MSG_MORE);
        send(sock, &n_size, sizeof(int), MSG_MORE);
        send(sock, &e_bin, e_size, MSG_MORE);
        send(sock, &n_bin, n_size, MSG_MORE);

        // Send random message size
        int msg_size = len;
        send(sock, &msg_size, sizeof(int), MSG_MORE);

        // Define additional variables
        int v_size;
        BIGNUM *bn_v = BN_new();

        BIGNUM *bn_k0 = BN_new();

        BIGNUM *bn_k1 = BN_new();

        int _m0_size;
        unsigned char *_m0_bin;
        BIGNUM *bn__m0 = BN_new();

        int _m1_size;
        unsigned char *_m1_bin;
        BIGNUM *bn__m1 = BN_new();

        BIGNUM *bn_v_sub_x0 = BN_new();
        BIGNUM *bn_v_sub_x1 = BN_new();
        BIGNUM *bn_label0 = BN_new();
        BIGNUM *bn_label1 = BN_new();

        BN_CTX *ctx = BN_CTX_new();

        // Convert InputLabelPairs to array of BIGNUMs

        BN_bin2bn((const unsigned char *) &label0, LABELSIZE, bn_label0);
        BN_bin2bn((const unsigned char *) &label1, LABELSIZE, bn_label1);

        // Perform 1-2-OT
        BN_rand(bn_x0, RSA_bits, 0, 0);
        BN_rand(bn_x1, RSA_bits, 0, 0);
        BN_bn2bin((const BIGNUM *) bn_x0, x0_bin);
        BN_bn2bin((const BIGNUM *) bn_x1, x1_bin);

        // Send random messages
        send(sock, x0_bin, len, MSG_MORE);
        send(sock, x1_bin, len, MSG_MORE);

        // Receive v size
        recv(sock, &v_size, sizeof(int), 0);
        unsigned char v_bin[v_size];
        recv(sock, &v_bin, v_size, 0);

        BN_bin2bn((const unsigned char *) v_bin, v_size, bn_v);

        BN_mod_sub(bn_v_sub_x0, bn_v, bn_x0, bn_n, ctx);
        BN_mod_exp(bn_k0, bn_v_sub_x0, bn_d, bn_n, ctx);

        BN_mod_sub(bn_v_sub_x1, bn_v, bn_x1, bn_n, ctx);
        BN_mod_exp(bn_k1, bn_v_sub_x1, bn_d, bn_n, ctx);

#ifdef GASH_OT_DEBUG
        printBignum("Sender d:", bn_d);
        printBignum("Sender p:", bn_p);
        printBignum("Sender q:", bn_q);
        printBignum("Sender n:", bn_n);
        printBignum("Sender e:", bn_e);
        printBignum("Sender x0:", bn_x0);
        printBignum("Sender x1:", bn_x1);
        printBignum("Sender d:", bn_d);
        printBignum("Sender V:", bn_v);
        printBignum("Sender calculated v_sub_x0: ", bn_v_sub_x0);
        printBignum("Sender calculated v_sub_x1: ", bn_v_sub_x1);
        printBignum("K0: ", bn_k0);
        printBignum("K1: ", bn_k1);
#endif

        BN_add(bn__m0, (const BIGNUM *) bn_k0, (const BIGNUM *) bn_label0);
        BN_add(bn__m1, (const BIGNUM *) bn_k1, (const BIGNUM *) bn_label1);

        _m0_size = BN_num_bytes((const BIGNUM *) bn__m0);
        _m1_size = BN_num_bytes((const BIGNUM *) bn__m1);
        _m0_bin = new unsigned char[_m0_size / sizeof(unsigned char)];
        _m1_bin = new unsigned char[_m1_size / sizeof(unsigned char)];

        BN_bn2bin((const BIGNUM *) bn__m0, _m0_bin);
        BN_bn2bin((const BIGNUM *) bn__m1, _m1_bin);

        // send encrypted messages
        send(sock, &_m0_size, sizeof(int), MSG_MORE);
        send(sock, &_m1_size, sizeof(int), MSG_MORE);
        send(sock, _m0_bin, _m0_size, MSG_MORE);
        send(sock, _m1_bin, _m1_size, MSG_MORE);

        delete[] _m0_bin;
        delete[] _m1_bin;

        BN_CTX_free(ctx);
        BN_free(bn_x0);
        BN_free(bn_x1);
        BN_free(bn_e);
        BN_free(bn_n);
        BN_free(bn_d);
        BN_free(bn_v);
        BN_free(bn_k0);
        BN_free(bn_k1);
        BN_free(bn__m0);
        BN_free(bn__m1);
        BN_free(bn_v_sub_x0);
        BN_free(bn_v_sub_x1);
        BN_free(bn_label0);
        BN_free(bn_label1);

    }

    block recvOT(int sock, int b_select) {
        unsigned char *x0_bin;
        unsigned char *x1_bin;
        unsigned char *_m0_bin;
        unsigned char *_m1_bin;
        unsigned char *mb_bin;
        BIGNUM *bn_e = BN_new();
        BIGNUM *bn_n = BN_new();
        BIGNUM *bn_xb_mod_n = BN_new();
        BIGNUM *bn_k_pow_e_mod_n = BN_new();
        BIGNUM *bn_v = BN_new();
        BIGNUM *bn_x0 = BN_new();  // TODO: should we delete BIGNUM* after use?
        BIGNUM *bn_x1 = BN_new();
        BIGNUM *bn_xb = NULL;
        BIGNUM *bn_k = BN_new();
        BIGNUM *bn__m0 = BN_new();
        BIGNUM *bn__m1 = BN_new();
        BIGNUM *bn__mb;
        BIGNUM *bn_mb = BN_new();
        BN_CTX *ctx = BN_CTX_new();
        int len, e_size, n_size, v_size;
        int _m0_size, _m1_size;
        block label;

        // Receive public key
        recv(sock, &e_size, sizeof(int), 0);
        recv(sock, &n_size, sizeof(int), 0);
        unsigned char e_bin[e_size];
        unsigned char n_bin[n_size];
        recv(sock, &e_bin, e_size, 0);
        recv(sock, &n_bin, n_size, 0);

        // Convert binary data to BIGNUM
        BN_bin2bn((const unsigned char *) e_bin, e_size, bn_e);
        BN_bin2bn((const unsigned char *) n_bin, n_size, bn_n);

        // Receive message size
        recv(sock, &len, sizeof(int), 0);

        // Receive x0_bin and x1_bin
        x0_bin = new unsigned char[len / sizeof(unsigned char)];
        x1_bin = new unsigned char[len / sizeof(unsigned char)];
        recv(sock, x0_bin, len, 0);
        recv(sock, x1_bin, len, 0);

        // Convert binary data to BIGNUM
        BN_bin2bn((const unsigned char *) x0_bin, len, bn_x0);
        BN_bin2bn((const unsigned char *) x1_bin, len, bn_x1);
        delete[] x0_bin;
        delete[] x1_bin;


        // Generate random K
        BN_rand(bn_k, RSA_bits - 10, 0, 0); // k has to be less than N!

        // Compute v=(x_b + k^e) mod N
        bn_xb = b_select == 0 ? bn_x0 : bn_x1;
        BN_mod(bn_xb_mod_n, (const BIGNUM *) bn_xb, (const BIGNUM *) bn_n, ctx);
        BN_mod_exp(bn_k_pow_e_mod_n, (const BIGNUM *) bn_k, (const BIGNUM *) bn_e, (const BIGNUM *) bn_n, ctx);
        BN_mod_add(bn_v, (const BIGNUM *) bn_xb_mod_n, (const BIGNUM *) bn_k_pow_e_mod_n, (const BIGNUM *) bn_n, ctx);


#ifdef GASH_OT_DEBUG
        printBignum("Receiver n:", bn_n);
        printBignum("Receiver e:", bn_e);
        printBignum("Receiver x0:", bn_x0);
        printBignum("Receiver x1:", bn_x1);
        printBignum("K:" , bn_k);
        printBignum("Receiver V:", bn_v);
#endif

        // Convert v to binary data and send v
        v_size = BN_num_bytes((const BIGNUM *) bn_v);
        unsigned char v_bin[v_size];
        BN_bn2bin((const BIGNUM *) bn_v, v_bin);
        send(sock, &v_size, sizeof(int), MSG_MORE);
        send(sock, &v_bin, v_size, MSG_MORE);

        // Receive _m0 and _m1
        recv(sock, &_m0_size, sizeof(int), 0);
        recv(sock, &_m1_size, sizeof(int), 0);
        _m0_bin = new unsigned char[_m0_size];
        _m1_bin = new unsigned char[_m1_size];
        recv(sock, _m0_bin, _m0_size, 0);
        recv(sock, _m1_bin, _m1_size, 0);

        // Convert binary data to BIGNUM
        BN_bin2bn((const unsigned char *) _m0_bin, _m0_size, bn__m0);
        BN_bin2bn((const unsigned char *) _m1_bin, _m1_size, bn__m1);
        delete[] _m0_bin;
        delete[] _m1_bin;

        // Compute mb = _mb-k
        bn__mb = b_select == 0 ? bn__m0 : bn__m1;
        BN_sub(bn_mb, (const BIGNUM *) bn__mb, (const BIGNUM *) bn_k);

        // Convert BIGNUM to label and stores it. Note: bn_mb is guaranteed to be 128 bit in size
        mb_bin = new unsigned char[BN_num_bytes(bn_mb) / sizeof(unsigned char)];
        BN_bn2bin((const BIGNUM *) bn_mb, mb_bin);
        label = _mm_load_si128((__m128i *) mb_bin);

        delete[] mb_bin;
        BN_CTX_free(ctx);
        BN_free(bn_e);
        BN_free(bn_n);
        BN_free(bn_xb_mod_n);
        BN_free(bn_k_pow_e_mod_n);
        BN_free(bn_v);
        BN_free(bn_x0);
        BN_free(bn_x1);
        BN_free(bn_k);
        BN_free(bn__m0);
        BN_free(bn__m1);
        BN_free(bn_mb);

        return label;
    }

    /* Simple Send and Recv */
    void simpleSend(int sock, block label0, block label1) {
        send(sock, (unsigned char *)&label0, 16, 0);
        send(sock, (unsigned char *)&label1, 16, 0);
    }

    block simpleRecv(int sock, int b) {
        unsigned char *label0_ptr[16];
        unsigned char *label1_ptr[16];

        recv(sock, label0_ptr, 16, 0);
        recv(sock, label1_ptr, 16, 0);

        if (b == 0) {
            return _mm_load_si128((__m128i *)label0_ptr);
        } else {
            return _mm_load_si128((__m128i *)label1_ptr);
        }
    }

    /* OT using OTExtension */
// Network Communication
    USHORT m_nPort = 1;
    const char* m_nAddr ;// = "localhost";
    CSocket* m_vSocket;
    uint32_t m_nPID; // thread id
    uint32_t runs = 1;
    field_type m_eFType;
    uint32_t m_nBitLength;
    MaskingFunction* m_fMaskFct;

// Naor-Pinkas OT
//BaseOT* bot;
    OTExtSnd *sender;
    OTExtRec *receiver;

    SndThread* sndthread;
    RcvThread* rcvthread;

    uint32_t m_nNumOTThreads;
    uint32_t m_nBaseOTs;
    uint32_t m_nChecks;

    bool m_bUseMinEntCorAssumption;
    ot_ext_prot m_eProt;

    double rndgentime;
    BOOL Init(crypto* crypt)
    {
        m_vSocket = new CSocket();//*) malloc(sizeof(CSocket) * m_nNumOTThreads);

        return TRUE;
    }

    BOOL Cleanup()
    {
        delete sndthread;

        //rcvthread->Wait();

        delete rcvthread;

        //cout << "Cleaning" << endl;
        delete m_vSocket;
        //cout << "done" << endl;
        return true;
    }

    BOOL Connect()
    {
        bool bFail = FALSE;
        uint64_t lTO = CONNECT_TIMEO_MILISEC;

#ifndef BATCH
        cout << "Connecting to party "<< !m_nPID << ": " << m_nAddr << ", " << m_nPort << endl;
#endif
        for(int k = 0; k >= 0 ; k--)
        {
            for( int i=0; i<RETRY_CONNECT; i++ )
            {
                if( !m_vSocket->Socket() )
                {
                    printf("Socket failure: ");
                    goto connect_failure;
                }

                if( m_vSocket->Connect( m_nAddr, m_nPort, lTO))
                {
                    // send pid when connected
                    m_vSocket->Send( &k, sizeof(int) );
#ifndef BATCH
                    cout << " (" << !m_nPID << ") (" << k << ") connected" << endl;
#endif
                    if(k == 0)
                    {
                        //cout << "connected" << endl;
                        return TRUE;
                    }
                    else
                    {
                        break;
                    }
                    SleepMiliSec(10);
                    m_vSocket->Close();
                }
                SleepMiliSec(20);
                if(i+1 == RETRY_CONNECT)
                    goto server_not_available;
            }
        }
    server_not_available:
        printf("Server not available: ");
    connect_failure:
        cout << " (" << !m_nPID << ") connection failed" << endl;
        return FALSE;
    }

    BOOL Listen()
    {
#ifndef BATCH
        cout << "Listening: " << m_nAddr << ":" << m_nPort << ", with size: " << m_nNumOTThreads << endl;
#endif
        if( !m_vSocket->Socket() )
        {
            goto listen_failure;
        }
        if( !m_vSocket->Bind(m_nPort, m_nAddr) )
            goto listen_failure;
        if( !m_vSocket->Listen() )
            goto listen_failure;

        for( int i = 0; i<1; i++ ) //twice the actual number, due to double sockets for OT
        {
            CSocket sock;
            //cout << "New round! " << endl;
            if( !m_vSocket->Accept(sock) )
            {
                cerr << "Error in accept" << endl;
                goto listen_failure;
            }

            UINT threadID;
            sock.Receive(&threadID, sizeof(int));

            if( threadID >= 1)
            {
                sock.Close();
                i--;
                continue;
            }

#ifndef BATCH
            cout <<  " (" << m_nPID <<") (" << threadID << ") connection accepted" << endl;
#endif
            // locate the socket appropriately
            m_vSocket->AttachFrom(sock);
            sock.Detach();
        }

#ifndef BATCH
        cout << "Listening finished"  << endl;
#endif
        return TRUE;

    listen_failure:
        cout << "Listen failed" << endl;
        return FALSE;
    }

    void InitOTSender(const char* address, int port, crypto* crypt, CLock *glock)
    {
#ifdef OTTiming
        timespec np_begin, np_end;
#endif
        m_nPort = (USHORT) port;
        m_nAddr = address;

        //Initialize values
        Init(crypt);

        //Server listen
        Listen();

        sndthread = new SndThread(m_vSocket, glock);
        rcvthread = new RcvThread(m_vSocket, glock);

        rcvthread->Start();
        sndthread->Start();

        switch(m_eProt) {
        case ALSZ: sender = new ALSZOTExtSnd(crypt, rcvthread, sndthread, m_nBaseOTs, m_nChecks); break;
        case IKNP: sender = new IKNPOTExtSnd(crypt, rcvthread, sndthread); break;
        case NNOB: sender = new NNOBOTExtSnd(crypt, rcvthread, sndthread); break;
        case KK: sender = new KKOTExtSnd(crypt, rcvthread, sndthread); break;
        default: sender = new ALSZOTExtSnd(crypt, rcvthread, sndthread, m_nBaseOTs, m_nChecks); break;
        }

        if(m_bUseMinEntCorAssumption)
            sender->EnableMinEntCorrRobustness();
        sender->ComputeBaseOTs(m_eFType);
    }

    void InitOTReceiver(const char* address, int port, crypto* crypt, CLock *glock)
    {
        m_nPort = (USHORT) port;
        m_nAddr = address;

        //Initialize values
        Init(crypt);

        //Client connect
        Connect();

        sndthread = new SndThread(m_vSocket, glock);
        rcvthread = new RcvThread(m_vSocket, glock);

        rcvthread->Start();
        sndthread->Start();

        switch(m_eProt) {
        case ALSZ: receiver = new ALSZOTExtRec(crypt, rcvthread, sndthread, m_nBaseOTs, m_nChecks); break;
        case IKNP: receiver = new IKNPOTExtRec(crypt, rcvthread, sndthread); break;
        case NNOB: receiver = new NNOBOTExtRec(crypt, rcvthread, sndthread); break;
        case KK: receiver = new KKOTExtRec(crypt, rcvthread, sndthread); break;
        default: receiver = new ALSZOTExtRec(crypt, rcvthread, sndthread, m_nBaseOTs, m_nChecks); break;
        }


        if(m_bUseMinEntCorAssumption)
            receiver->EnableMinEntCorrRobustness();
        receiver->ComputeBaseOTs(m_eFType);
    }
    // Code From OTExtension
    BOOL ObliviouslySend(CBitVector** X, int numOTs, int bitlength, uint32_t nsndvals,
                         snd_ot_flavor stype, rec_ot_flavor rtype, crypto* crypt)
    {
        bool success = FALSE;

        m_vSocket->ResetSndCnt();
        m_vSocket->ResetRcvCnt();
        timespec ot_begin, ot_end;

        clock_gettime(CLOCK_MONOTONIC, &ot_begin);
        // Execute OT sender routine
        success = sender->send(numOTs, bitlength, nsndvals, X, stype, rtype, m_nNumOTThreads, m_fMaskFct);
        clock_gettime(CLOCK_MONOTONIC, &ot_end);

#ifndef BATCH
        printf("Time spent:\t%f\n", getMillies(ot_begin, ot_end) + rndgentime);
        cout << "Sent:\t\t" << m_vSocket->getSndCnt() << " bytes" << endl;
        cout << "Received:\t" << m_vSocket->getRcvCnt() <<" bytes" << endl;
#else
        cout << getMillies(ot_begin, ot_end) + rndgentime << "\t" << m_vSocket->getSndCnt() << "\t" << m_vSocket->getRcvCnt() << endl;
#endif


        return success;
    }

    BOOL ObliviouslyReceive(CBitVector* choices, CBitVector* ret, int numOTs, int bitlength, uint32_t nsndvals,
                            snd_ot_flavor stype, rec_ot_flavor rtype, crypto* crypt)
    {
        bool success = FALSE;

        m_vSocket->ResetSndCnt();
        m_vSocket->ResetRcvCnt();


        timespec ot_begin, ot_end;
        clock_gettime(CLOCK_MONOTONIC, &ot_begin);
        // Execute OT receiver routine
        success = receiver->receive(numOTs, bitlength, nsndvals, choices, ret, stype, rtype, m_nNumOTThreads, m_fMaskFct);
        clock_gettime(CLOCK_MONOTONIC, &ot_end);

#ifndef BATCH
        printf("Time spent:\t%f\n", getMillies(ot_begin, ot_end) + rndgentime);

        cout << "Sent:\t\t" << m_vSocket->getSndCnt() << " bytes" << endl;
        cout << "Received:\t" << m_vSocket->getRcvCnt() <<" bytes" << endl;
#else
        cout << getMillies(ot_begin, ot_end) + rndgentime << "\t" << m_vSocket->getSndCnt() << "\t" << m_vSocket->getRcvCnt() << endl;
#endif


        return success;
    }

    void OTSend(string peer_addr, int peer_ot_port, vector<block>& label0s, vector<block>& label1s) {

        assert(label0s.size() == label1s.size());

#ifdef GASH_OT_DEBUG
        std::cout << "GASH_OT_DEBUG is set." << std::endl;
        peer_addr = string("127.0.0.1");
        peer_ot_port = 7766;
#endif
        uint32_t m_nLabel = label0s.size();

        uint64_t numOTs = m_nLabel;

#ifdef GASH_OT_DEBUG
        uint32_t bitlength = 8;
        uint32_t nsndvals = 2;
#else
        uint32_t bitlength = 128;
        uint32_t nsndvals = 2;
#endif

        m_eFType = ECC_FIELD;

        uint32_t m_nSecParam = 128;

        m_nNumOTThreads = 1;

        snd_ot_flavor stype = Snd_OT;
        rec_ot_flavor rtype = Rec_OT;

        // The following two integers are only useful for ALSZ
        m_nBaseOTs = 190;
        m_nChecks = 380;

        m_bUseMinEntCorAssumption = false;

        m_eProt = IKNP;

        m_nAddr = peer_addr.c_str();
        m_nPort = peer_ot_port;
        m_nPID = 0;

        std::cout << "Parameters for sender:\n"\
                  << "numOTs:" << numOTs << "\n"\
                  << "bitlength:" << bitlength << "\n"\
                  << "nsndvals:" << nsndvals << "\n"\
                  << "m_eFType:" << m_eFType << "\n"\
                  << "m_nSecParam:" << m_nSecParam << "\n"\
                  << "m_nNumOTThreads:" << m_nNumOTThreads << "\n"\
                  << "m_nBaseOTs:" << m_nBaseOTs << "\n"\
                  << "m_nChecks:" << m_nChecks << "\n"\
                  << "m_nPID:" << m_nPID << "\n"\
                  << "m_nAddr:" << m_nAddr << "\n"\
                  << "m_nPort:" << m_nPort << "\n";

        crypto *crypt = new crypto(m_nSecParam, (uint8_t*) m_cConstSeed[m_nPID]);

        CLock *glock = new CLock();

        InitOTSender(peer_addr.c_str(), peer_ot_port, crypt, glock);

        CBitVector delta; // The R
        CBitVector** X = (CBitVector**) malloc(sizeof(CBitVector*) * nsndvals);
        m_fMaskFct = new XORMasking(bitlength, delta);

        //creates delta as an array with "numOTs" entries of "bitlength" bit-values and fills delta with random values
        delta.Create(numOTs, bitlength);

        BYTE** bytes = (BYTE**) malloc(sizeof(BYTE*) * nsndvals);
        for (int i = 0; i < nsndvals; ++i) {
            X[i] = new CBitVector();
            X[i]->Create(numOTs, bitlength);
            int size = 0;
            if (i == 0)
                bytes[i] = label_vec_2_byte_array(label0s, size);
            else
                bytes[i] = label_vec_2_byte_array(label1s, size);
            X[i]->SetBytes(bytes[i], 0, size);
        }

#ifdef GASH_DEBUG
        std::cout << getProt(m_eProt) << " Sender performing " << numOTs << " " << getSndFlavor(stype) << " / " <<
            getRecFlavor(rtype) << " extensions on " << bitlength << " bit elements with " <<	m_nNumOTThreads << " threads, " <<
            getFieldType(m_eFType) << " and" << (m_bUseMinEntCorAssumption ? "": " no" ) << " min-ent-corr-robustness " <<
            runs << " times" << std::endl;
#endif
        ObliviouslySend(X, numOTs, bitlength, nsndvals, stype, rtype, crypt);

        Cleanup();
        delete crypt;
        delete glock;
        for (int i = 0; i < nsndvals; ++i) {
            delete [] bytes[i];
        }
        free(bytes);

    }

    void OTRecv(string peer_addr, int peer_ot_port, std::map<ulong, int>& selects, std::unordered_map<ulong, block>& inputLabel) {

#ifdef GASH_OT_DEBUG
        std::cout << "GASH_OT_DEBUG is set." << std::endl;
        peer_addr = string("127.0.0.1");
        peer_ot_port = 7766;
#endif

        uint64_t numOTs = selects.size();


#ifdef GASH_OT_DEBUG
        uint32_t bitlength = 8;
        uint32_t nsndvals = 2;
#else
        uint32_t bitlength = 128;
        uint32_t nsndvals = 2;
#endif

        m_eFType = ECC_FIELD;

        uint32_t m_nSecParam = 128;

        m_nNumOTThreads = 1;

        snd_ot_flavor stype = Snd_OT;
        rec_ot_flavor rtype = Rec_OT;

        // The following two integers are only useful for ALSZ
        m_nBaseOTs = 190;
        m_nChecks = 380;

        m_bUseMinEntCorAssumption = false;

        m_eProt = IKNP;

        m_nAddr = peer_addr.c_str();
        m_nPort = peer_ot_port;
        m_nPID = 1;

        std::cout << "Parameters for recver:\n"\
                  << "numOTs:" << numOTs << "\n"\
                  << "bitlength:" << bitlength << "\n"\
                  << "nsndvals:" << nsndvals << "\n"\
                  << "m_eFType:" << m_eFType << "\n"\
                  << "m_nSecParam:" << m_nSecParam << "\n"\
                  << "m_nNumOTThreads:" << m_nNumOTThreads << "\n"\
                  << "m_nBaseOTs:" << m_nBaseOTs << "\n"\
                  << "m_nChecks:" << m_nChecks << "\n"\
                  << "m_nPID:" << m_nPID << "\n"\
                  << "m_nAddr:" << m_nAddr << "\n"\
                  << "m_nPort:" << m_nPort << "\n";

        crypto *crypt = new crypto(m_nSecParam, (uint8_t*) m_cConstSeed[m_nPID]);

        CLock *glock = new CLock();

        InitOTReceiver(peer_addr.c_str(), peer_ot_port, crypt, glock);

        CBitVector choices, response;

        m_fMaskFct = new XORMasking(bitlength);

        //Fill choices with values in selects
        choices.Create(numOTs * ceil_log2(nsndvals), crypt);
        // X[i]->SetBytes(bytes[i], 0, size);
        int size = 0;
        vector<int> selects_vec;
        for (auto it = selects.begin(); it != selects.end(); ++it) {
#ifdef GASH_DEBUG
            std::cout << "Pushing back the bit for wire: " << it->first\
                      << "bit: " << it->second;
#endif
            selects_vec.push_back(it->second);
        }
        BYTE* choice_bytes = int_vec_2_byte_array(selects_vec, size);
        choices.SetBytes(choice_bytes, 0, size);

        //Pre-generate the respose vector for the results
        response.Create(numOTs, bitlength);
        response.Reset();

        ObliviouslyReceive(&choices, &response, numOTs, bitlength, nsndvals, stype, rtype, crypt);

        // TODO: convert `response` back to label vectors

        BYTE* response_bytes = new BYTE[size * 128];
        response.GetBytes(response_bytes, 0, size * 128);
        int i = 0;
        for (auto it = selects.begin(); it != selects.end(); ++it, ++i) {
            ulong id = it->first;
            block label = byte_2_label(response_bytes, 16 * i);
            inputLabel.insert(std::make_pair(id, label));
#ifdef GASH_DEBUG
            std::cout << "OT Receiving label for wire: " << id << "\n"\
                      << "Label: " << block2hex(label) << "\n";
#endif
        }

        Cleanup();
        delete crypt;
        delete glock;
    }
}
