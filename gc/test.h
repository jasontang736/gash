//
// Created by Xiaoting Tang on 10/16/17.
//

#ifndef GASH_TEST_H
#define GASH_TEST_H
namespace GASH_GC {

void testOT();
void test_mod_exp();
void test_special_case();

}
#endif //GASH_TEST_H
