//
// Created by Xiaoting Tang on 10/8/17.
//

#ifndef GASH_GARBLER_H
#define GASH_GARBLER_H
#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <unistd.h>
#include <cstdlib>
#include <smmintrin.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "common.h"
#include "proto/garbledcircuit.pb.h"
#include "proto/bitmap.pb.h"
#include "proto/output_map.pb.h"
#include "garble.h"
#ifndef MSG_MORE
# define MSG_MORE 0
#endif
namespace GASH_GC {
    class Garbler{
    public:
        GarbledCircuit gc;
        Circuit c;
        GarbledCircuitProto gc_pb;
        GarbledCircuitProto gc_pb_debug;
        std::map<ulong, block> label0s;
        std::map<ulong, block> label1s;
        std::unordered_map<ulong, block> input_label0s;
        std::unordered_map<ulong, block> input_label1s;
        int self_in_size;
        int peer_in_size;
        OutputMapProto outputMap;
        std::map<ulong, int> inputValues;
        std::map<ulong, int> outputValues;
        vector<ulong> peer_in_ids;
        BitMapProto outputBits;
        int peer_sock;
        int listen_sock;
        string ip;  // localhost
        int port;
        int peer_ot_port;
        std::string outputfile;
        void buildCircuit(std::string);
        void readOutputSpec(std::string);
        void readInput(std::string);
        void garbleCircuit();
        void simpleExecuteCircuit();
        void buildConnection();
        void sendGarbledTables();
        void sendSelfLabels();
        void sendPeerLabels();     // ot
        void sendOutputMap();
        void recvOutput();
        void getOutput(vector<bool>& output);
        void simpleDescOutput();
        void recvLabelsForDebug();
        void descGarbledWires();
        void descWires();
        void descOutput();
        void print();
        Garbler(int port);
        ~Garbler();
    };
}
#endif //GASH_GARBLER_H
