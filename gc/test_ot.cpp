//
// Created by Xiaoting Tang on 10/16/17.
//

#include <unistd.h>
#include <thread>
#include "common.h"
#include "util.h"
#include "ot.h"
#include "tcp.h"
namespace GASH_GC {
    void testReceiver(int b_select, block label0, block label1) {
        int peer_sock = 0;
        for (int i = 0; i < 10; i++) {
            buildConnectionAsClient("127.0.0.1", 24567, peer_sock);
            block recv_label = recvOT(peer_sock, b_select);
            block target = b_select == 0 ? label0 : label1;
            block theother = b_select == 0 ? label1 : label0;
            if (!blockEq(recv_label, target)) {
                std::cerr << "OT failed." << std::endl;
                std::cerr << "Received:" << block2hex(recv_label) << std::endl;
                std::cerr << "Expecting:" << block2hex(target) << std::endl;
                std::cerr << "The other is:" << block2hex(theother) << std::endl;
            } else {
                std::cout << "OT succedded." << std::endl;
                std::cout << "Received:" << block2hex(recv_label) << std::endl;
                std::cout << "Expecting:" << block2hex(target) << std::endl;
                std::cout << "The other is:" << block2hex(theother) << std::endl;
            }
            close(peer_sock);
        }
    }

    void testSender(block label0, block label1) {
        int listen_sock = 0;
        int peer_sock = 0;
        for (int i = 0; i < 10; i++) {
            buildConnectionAsServer(24567, listen_sock, peer_sock);
            sendOT(peer_sock, label0, label1);
            close(listen_sock);
            close(peer_sock);
        }
    }

    void testOT() {
        srand_sse(time(NULL));
        block label1 = random_block();
        block label0 = random_block();
        std::thread tRecv(testReceiver, 0, label0, label1);
        testSender(label0, label1);
        tRecv.join();
    }
}
