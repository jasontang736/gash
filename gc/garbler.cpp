#include "garble.h"
#include "garbler.h"
#include "ot.h"
#include "tcp.h"
#include "circuit.h"

namespace GASH_GC {

    Garbler::Garbler(int port) {
        this->port = port;
        this->peer_ot_port = OT_PORT;
        this->ip = "";
    }

    void Garbler::buildCircuit(std::string circfile) {
        GASH_GC::buildCircuit(circfile, this->c);
        this->gc.n = this->c.n;
        this->gc.m = this->c.m;
        this->gc.q = this->c.q;
        // this->readOutputSpec(circfile);
    }

    // void Garbler::readOutputSpec(std::string circfile) {
    //      std::ifstream inFile(circfile);
    //      if (!inFile.is_open()) {
    //           std::cerr << "Unable to open file " << fname << std::endl;
    //           exit(1);
    //      }
    //      std::string line;
    //      while (getline(inFile, line)) {
    //           BitsField bits_field;
    //           IdBundle id_bundle;
    //           OutputSpec output_spec;
    //           ulong id;

    //           std::vector<std::string> items = split(line, ' ');
    //           if (items[0].find('#') == std::string::npos)  // Skip gate and wire stmt
    //                continue;

    //           if (std::strcmp(items[0].c_str(), Garbler::DIR_OUTPUTSPEC)) {
    //                std::vector<std::string> name_spec = split(items[1], ':');
    //                std::vector<std::string> port_list = split(items[2], ',');

    //                output_spec->add_names(name_spec[0]);

    //                bits_field.add_int_part(stoi(name_spec[1]));
    //                bits_field.add_frac_part(stoi(name_spec[2]));
    //                output_spec->add_bits_fields(bits_field);

    //                for (auto & it = port_list.begin(); it != port_list.end(); ++it) {
    //                     id = stoi(*it)/2;    // idx -> id
    //                     id_bundle.add_ids(id);
    //                     this->output_spec_map->insert(\
    //                          std::make_pair<::google::protobuf::int64, ::std::string>\
    //                          (id, name_spec[0]));
    //                }
    //                output_spec->set_id_bundle(id_bundle);
    //                this->output_spec_map->insert(\
    //                     std::make_pair<::std::string, OutputSpec>\
    //                     (name_spec[0], output_spec));
    //           } else {
    //                LOG(FATAL) << "Unsupported directive " << items[0] << "\n";
    //           }
    //      }
    // }

    void Garbler::garbleCircuit() {
        GASH_GC::garbleCircuit(this->c, this->gc, this->input_label0s, this->input_label1s, this->outputMap, this->c.outputWireIds_,
                               this->label0s, this->label1s);
    }

    void Garbler::print() {
        int id;

        this->gc.printSummaryAsGarbler();

        std::cout << "Alice's input" << std::endl;

        for (int i = 0; i < this->self_in_size; i++) {
            id = i;
            std::cout << INDENTx1 << "input_" << id << std::endl;
            std::cout << INDENTx2 << "wire_id=" << id << std::endl;
            std::cout << INDENTx2 << "value=" << this->inputValues[id] << std::endl;
            std::cout << INDENTx2 << "label="
                      << block2dec(this->inputValues[id] == 0 ? this->input_label0s[id] : this->input_label1s[id])
                      << std::endl;
        }

        std::cout << "Bob's input" << std::endl;

        for (int i = 0; i < this->peer_in_size; i++) {
            id = i + this->self_in_size;
            std::cout << INDENTx1 << "input_" << id << std::endl;
            std::cout << INDENTx2 << "wire_id=" << id << std::endl;
            std::cout << INDENTx2 << "label0=" << block2dec(this->input_label0s[id]) << std::endl;
            std::cout << INDENTx2 << "label1=" << block2dec(this->input_label1s[id]) << std::endl;
        }
    }

    void Garbler::readInput(std::string fname) {
        /**
         * input <num_input>
         * 0 1
         * 1 1
         * 2 0
         * 3 1
         * 4 0
         * 5 1
         * ...
         */
        std::ifstream inFile(fname);
        if (!inFile.is_open()) {
            std::cerr << "Unable to open input file " << fname << std::endl;
            exit(1);
        }

        std::string line;
        int size;
        while (getline(inFile, line)) {
            std::vector<std::string> items = split(line, ' ');
            if (items[0].compare("input") == 0) {      // Prologue
                size = stoi(items[1]);
                this->self_in_size = size;
                this->peer_in_size = this->c.n - size;
                continue;
            }
            if (items.size() != 2) {
                std::cerr << "Invalid input file " << fname << std::endl;
                exit(-1);
            }
            // Bits
            this->inputValues.insert(
                std::make_pair(stoi(items[0]) / 2, stoi(items[1])));
        }

        for (auto it = this->c.input_ids.begin(); it != this->c.input_ids.end(); ++it) {
            int id = *it;
            if (this->inputValues.find(id) == this->inputValues.end()) {
                this->peer_in_ids.push_back(id);
            }
        }

//        int out_count = 0;
//        while (out_count++ < this->c.m) {
//            this->peer_in_ids.pop_back();
//        }
    }

    void Garbler::simpleExecuteCircuit() {
        executeCircuit(this->c, this->inputValues, this->c.output_ids, this->outputValues);
    }

    void Garbler::buildConnection() {
        buildConnectionAsServer(this->port, this->listen_sock, this->peer_sock);
    }

    void Garbler::sendGarbledTables() {
        GarbledCircuitProto gc_pb;
        int size = 0;
        for (auto pair: this->gc.garbledGates) {

            GarbledGate* gg = pair.second;
#ifdef GASH_DEBUG
            std::cout << "Sending gate " << pair.first << " of type " << (gg->isXOR ? "XOR" : "Non-XOR") << std::endl;
#endif
            if (gg->isXOR) {
                continue;
            }

            ulong id = pair.first;

            GarbledCircuitProto::GarbledGateProto *gg_pb = gc_pb.add_garbledgates();

            gg_pb->set_id(id);

            // TODO: verify table[1], table[2], table[3] is materialized
            ulong row_1_0 = (ulong) _mm_extract_epi64(gg->table->get_row(1), 0);
            ulong row_1_1 = (ulong) _mm_extract_epi64(gg->table->get_row(1), 1);
            ulong row_2_0 = (ulong) _mm_extract_epi64(gg->table->get_row(2), 0);
            ulong row_2_1 = (ulong) _mm_extract_epi64(gg->table->get_row(2), 1);
            ulong row_3_0 = (ulong) _mm_extract_epi64(gg->table->get_row(3), 0);
            ulong row_3_1 = (ulong) _mm_extract_epi64(gg->table->get_row(3), 1);
            gg_pb->set_row_1_0(row_1_0);
            gg_pb->set_row_1_1(row_1_1);
            gg_pb->set_row_2_0(row_2_0);
            gg_pb->set_row_2_1(row_2_1);
            gg_pb->set_row_3_0(row_3_0);
            gg_pb->set_row_3_1(row_3_1);

            // TODO: now GarbledCircuit can be safely deleted, unless you still need it.
        }

        this->gc_pb = gc_pb;
        size = gc_pb.ByteSize();
        char *data = new char[size];
        gc_pb.SerializeToArray(data, size);

        // Ready to send
        send(this->peer_sock, &size, sizeof(size), MSG_MORE);
        send(this->peer_sock, data, size, MSG_MORE);   // MSG_MORE: Don't send partial frames
    }

    void Garbler::sendSelfLabels() {
        ulong id;
        int value;
        block label;

        send(this->peer_sock, &this->self_in_size, sizeof(int), MSG_MORE);

        // Garble self input and send
        for (auto it = this->inputValues.begin(); it != this->inputValues.end(); ++it) {
            id = it->first;
            value = it->second;
            send(this->peer_sock, &id, sizeof(ulong), MSG_MORE);

            if (value == 0) {
                label = this->input_label0s[id];
            } else {
                label = this->input_label1s[id];
            }

            send(this->peer_sock, &label, sizeof(block), MSG_MORE);

#ifdef GASH_DEBUG
            std::cout << "Sending self label for wire " << id\
                      << ". semantic:" << this->inputValues[id]\
                      << ". label:" << block2hex(label) << std::endl;
#endif
        }
    }

    void Garbler::sendPeerLabels() {

#ifdef GASH_NO_OT
        // Using simple send and recv
        for (auto it = this->peer_in_ids.begin(); it != this->peer_in_ids.end(); ++it) {
            int id = *it;
            block label0 = this->input_label0s[id];
            block label1 = this->input_label1s[id];
            simpleSend(this->peer_sock, label0, label1);

#ifdef GASH_DEBUG
            std::cout << "Sending peer label for wire " << id\
                      << "label0:" << block2hex(label0) << std::endl\
                      << "label1:" << block2hex(label1) << std::endl;
#endif
        }
#else
        // Using IKNP OTExt with Naor & Pinkas base OT
        vector<block> peer_label0s;
        vector<block> peer_label1s;
        for (auto it = this->peer_in_ids.begin(); it != this->peer_in_ids.end(); ++it) {
            int id = *it;
            block label0 = this->input_label0s[id];
            block label1 = this->input_label1s[id];
            peer_label0s.push_back(label0);
            peer_label1s.push_back(label1);
        }
        OTSend(this->ip, this->peer_ot_port, peer_label0s, peer_label1s);
#endif
    }

    void Garbler::sendOutputMap() {
        int size = this->outputMap.ByteSize();
        char *data = new char[size];
        this->outputMap.SerializeToArray(data, size);

        // send(this->peer_sock, &size, sizeof(int), MSG_DONTWAIT);

        // int sent_amt = 0;
        // while(sent_amt < size) {
        //     sent_amt += send(this->peer_sock, data + sent_amt, size - sent_amt, MSG_DONTWAIT);
        // }
        if (!sendBytes(this->peer_sock, data, size)) {
            std::cerr << "Failed to send outputMap.\n";
        }

        delete [] data;
    }

    void Garbler::recvOutput() {
        // Receive values of named variables

        int size = 0; // size of ValueBundle

        char* data = recvBytes(this->peer_sock, size);

        // Parse
        if (!this->outputBits.ParseFromArray(data, size))
            LOG(FATAL) << "De-serialization failed.\n";

        if (this->outputBits.bits_size() != this->gc.m) {
            LOG(FATAL) << "Output size does not match with circuit.\n"\
                       << "Receive size: " << this->outputBits.bits_size()<< "\n" \
                       << "Expecting size: " << this->gc.m << "\n";
        }

    }

    void Garbler::descOutput() {
        for (auto it = this->outputValues.begin(); it != this->outputValues.end(); ++it) {
            std::cout << it->first << ": " << (int)it->second << std::endl;
        }
    }

    void Garbler::descWires() {
        for (auto it = this->c.wireInstances_.begin(); it != this->c.wireInstances_.end(); ++it) {
            std::cout << it->first << ": " << (int)it->second->get_value() << std::endl;
        }
    }

    void Garbler::simpleDescOutput() {
        // Describe the output by "variable_name, bit_size, bit_value, float_value"
        LOG(FATAL) << "Function unimplemented.\n";
    }

    void Garbler::getOutput(vector<bool>& vec) {
        for (auto it = this->outputBits.bits().begin(); it != this->outputBits.bits().end(); ++it)  {
            vec.push_back((bool)it->second);
        }
    }

    void Garbler::recvLabelsForDebug() {

        int size = 0;
        recv(this->peer_sock, &size, sizeof(int), 0);

        char* data = new char[size];
        recv(this->peer_sock, data, size, 0);

        if (!this->gc_pb_debug.ParseFromArray(data, size)) {
            std::cerr << "Parse labels for debugging failed." << std::endl;
            abort();
        }
    }

    void Garbler::descGarbledWires() {

        for(auto it = this->gc_pb_debug.mutable_wirelabels()->begin(); it != this->gc_pb_debug.mutable_wirelabels()->end(); ++it) {
            ulong id = it->id();
            ulong label_least_half = it->least_half();
            ulong label_great_half = it->great_half();
            block label = concat64_to_block(label_least_half, label_great_half);
            auto label0_it = this->label0s.find(id);
            block label0 = label0_it->second;
            auto label1_it = this->label1s.find(id);
            block label1 = label1_it->second;

            if (blockEq(label0, label1)) {
                std::cerr << "Label0 and label1 cannot be the same." << std::endl;
            }

            if (blockEq(label, label0)) {
                std::cout << id << ":" << 0 << std::endl;
            } else if (blockEq(label, label1)) {
                std::cout << id << ":" << 1 << std::endl;
            } else {
                std::cerr << "Invalid label " << block2hex(label) << std::endl;
                std::cerr << "Expecting either (0):" << block2hex(label0) << std::endl;
                std::cerr << "or (1):" << block2hex(label1) << std::endl;
                std::cerr << "Wire " << id << ", inverted: " << this->c.getWireInstance(id)->inverted() << std::endl;
            }
        }
    }

    Garbler::~Garbler() {
        for (auto it = this->gc.garbledWireInstances.begin(); it != this->gc.garbledWireInstances.end(); ++it) {
            delete it->second;
        }
    }
}
