//
// Created by Xiaoting Tang on 10/8/17.
//

#ifndef GASH_UTIL_H
#define GASH_UTIL_H

#include "common.h"
#include "emmintrin.h"
#include "proto/output_map.pb.h"
#include <sstream>
#include <time.h>
#include <openssl/bn.h>

#define INDENTx1 "    "
#define INDENTx2 "        "

#define idx2id_garbler(a) (a + 1) * 2
#define idx2id_evaluator(a, b) (a + 1 + b) * 2

namespace GASH_GC {

    typedef struct Event {
        const char* name;
        unsigned long elapsed_time;
    } Event;

    typedef struct Timer {
        map<int, Event> records;
        int numrecords;
        bool ticking;
        timespec last_time, current_time;
        unsigned long elapsed_time;
        const char* current_event_name;
        void tic(const char*);
        void toc();
        void summarize();
        Timer();
    } Timer;

    template<typename Out>
    void split(std::string &s, char delim, Out result);

    std::vector<std::string> split(std::string &s, char delim);

    void srand_sse(unsigned int seed);

    block random_block();

    block random_R();

    bool blockEq(block a, block b);

    std::string block2hex(block);

    std::string block2dec(block);

    void printBignum(std::string s, BIGNUM *bn);

    inline int getBit(int a, int b) {
        return ((a >> b) & 1) == 1 ? 1 : 0;
    }

    inline int eval_bit(int a, int b, int func) {
        return getBit(func, a + (b << 1));
    }

    inline int eval_bit(int a, int b, int func, bool inverted) {
        if (inverted)
            return getBit(func, a + (b << 1));
        else
            return 1 ^ getBit(func, a + (b << 1));
    }

    inline int is_odd(int n) { return !(n%2==0); }

    inline int getLSB(block label) {
        uint8_t *var = (uint8_t *) &label;
        return (*(var) & 1) == 1 ? 1 : 0;
    }

    inline block xor_block(block block_a, block block_b) {
        return _mm_xor_si128(block_a, block_b);
    }

    block newTweak(ulong g);

    inline block concat64_to_block(ulong least_half, ulong great_half) {
        return _mm_set_epi64x(great_half, least_half);
    }

    inline ulong get_block_first_half(block b) {
        return *((ulong*)&b);
    }


    inline ulong get_block_second_half(block b) {
        return *(((ulong*)&b)+1);
    }

    int find_semantic_of_label_from_pair(ulong id, OutputMapProto& outputMap, block label);

    int bin2int(vector<bool> bin);

    int partial_bin2int(vector<int>& bits, int start, int size);

    void printChar2Hex(char* s, int size);

    void fprintChar2Hex(char* s, int size, const char* fname);

    bool sendBytes(int socket, char* s, int size);

    char* recvBytes(int socket, int& size);

    typedef unsigned char Byte;

    inline void setBit(Byte* byte, int pos) {
        (*byte) |= 1 << pos;
    }

    inline void clearBit(Byte* byte, int pos) {
        (*byte) &= ~(1 << pos);
    }

    Byte* label_vec_2_byte_array(vector<block>& labels, int& size);

    Byte* int_vec_2_byte_array(vector<int>& selects, int& size);

    block byte_2_label(Byte* bytes, int start);
}
#endif //GASH_UTIL_H
