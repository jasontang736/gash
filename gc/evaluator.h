//
// Created by Xiaoting Tang on 10/8/17.
//

#ifndef GASH_EVALUATOR_H
#define GASH_EVALUATOR_H

#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <openssl/bn.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include "common.h"
#include "garble.h"
#include "proto/garbledcircuit.pb.h"
#include "proto/bitmap.pb.h"
#ifndef MSG_MORE
# define MSG_MORE 0
#endif
namespace GASH_GC {
    class Evaluator {
    public:
        GarbledCircuit gc;
        GarbledCircuitProto gc_pb;
        GarbledCircuitProto gc_pb_debug;
        Circuit c;
        std::unordered_map<ulong, block> inputLabel;
        int self_in_size;
        int peer_in_size;
        OutputMapProto outputMap;
        std::map<ulong, int> input_values;
        BitMapProto outputValues;
        std::map<ulong, int> output_values;
        std::string peer_ip;
        int peer_ot_port;
        int peer_port;
        int self_sock;
        void buildCircuit(std::string);
        void readInput(std::string);
        void buildConnection();
        void simpleExecuteCircuit();
        void recvGarbledTables();
        void garbleCircuit();
        void recvPeerLabels();
        void recvSelfLabels();    // ot
        void recvOutputMap();
        void evaluateCircuit();
#ifdef GASH_DEBUG
        void sendLabelsForDebug();
#endif
        void sendOutput();
        void descOutput();
        void getOutput(vector<bool>&);
        void print();
        Evaluator(std::string ip, int port);
        ~Evaluator();
    };
}
#endif //GASH_EVALUATOR_H
