//
// Created by Xiaoting Tang on 10/17/17.
//

#include "circuit.h"
#include <utility>
namespace GASH_GC{
     // ------- new ---------
     Gate* Circuit::getGate(ulong i) {
          return this->gates_[i];
     }

     WireInstance* Circuit::getWireInstance(ulong i) {
          return this->wireInstances_[i];
     }

     void Circuit::addWireInstance(WireInstance* w) {
          this->wireInstances_.insert(std::make_pair(w->get_id(), w));
     }

     void Circuit::addGate(Gate* g) {
          // Note: it is better to map the gate's output wire to the gate, than map the gate's own id to gate.
          this->gates_.insert(std::make_pair(g->get_id(), g));
     }

     bool Circuit::hasGate(ulong id) {
          // Check whether circuit contain a gate whose output wire id is `id`
          return !(this->gates_.find(id) == this->gates_.end());
     }

}
