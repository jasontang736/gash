//
// Created by Xiaoting Tang on 10/17/17.
//

#include "labelpair.h"
#include "util.h"
namespace GASH_GC {
    block LabelPair::withSemantic(int s) {
        return s == 0 ? this->label0 : this->label1;
    }

    int LabelPair::semanticWithLSB(int lsb) {
        return getLSB(this->label0) == lsb ? 0 : 1;
    }

    void LabelPair::setLabelWithSemantic(block label, int semantic) {
        if (semantic == 0) {
            this->label0 = label;
        } else {
            this->label1 = label;
        }
    }

    block LabelPair::withLSB(int i) {
        return (getLSB(this->label0) == i) ? this->label0 : this->label1;
    }
}