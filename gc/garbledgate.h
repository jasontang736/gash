//
// Created by Xiaoting Tang on 10/17/17.
//

#ifndef GASH_GARBLEDGATE_H
#define GASH_GARBLEDGATE_H

#include "garbledwire.h"
#include "util.h"
namespace GASH_GC {

    class EGTT {
    private:
        // block row0;  // Uncomment to disable row reduction
        block row1_;
        block row2_;
        block row3_;
    public:
        EGTT() {}
        EGTT(block row1, block row2, block row3) : row1_(row1), row2_(row2), row3_(row3) {}
        // block get_row0() { return row0_; }   // Uncomment to disable row reduction
        block get_row1() { return row1_; }
        block get_row2() { return row2_; }
        block get_row3() { return row3_; }
        block get_row(int i) {
            switch (i) {
            case 0:
                return getZEROblock();
            case 1:
                return row1_;
            case 2:
                return row2_;
            case 3:
                return row3_;
            default:
                LOG(FATAL) << "Invalid row selection: " << i << "\n";
            }
            return getZEROblock();
        }
        void set_row1(block r1) { row1_ = r1; }
        void set_row2(block r2) { row2_ = r2; }
        void set_row3(block r3) { row3_ = r3; }
    };

    class GarbledGate {
    public:
        ulong id;
        bool isXOR;
        GarbledWireInstance *in0, *in1, *out;
        EGTT* table;
        GarbledGate(){}
        GarbledGate(ulong aId, bool IsXOR, GarbledWireInstance* w_in0, GarbledWireInstance* w_in1, GarbledWireInstance* w_out)
        : id(aId), isXOR(IsXOR), in0(w_in0), in1(w_in1), out(w_out) {}
    };
}
#endif //GASH_GARBLEDGATE_H
