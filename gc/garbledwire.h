//
// Created by Xiaoting Tang on 10/17/17.
//

#ifndef GASH_GARBLEDWIRE_H
#define GASH_GARBLEDWIRE_H

#include "common.h"
#include "wire.h"
namespace GASH_GC {

    class GarbledWire {
    public:
        ulong id;
        block label0_;               // Used for garbling
        block label1_;
        block label_;               // Used for evaluating
        GarbledWire () {}
        GarbledWire (WireInstance* w) : id(w->get_id()) {}
        GarbledWire (ulong id) : id(id) {}
    };

    class GarbledWireInstance {
    private:
        GarbledWire* garbled_wire_ptr_;
        bool inverted_;
    protected:
        bool is_null() { return !((bool) garbled_wire_ptr_); }
        inline void require_not_null() {
            if (is_null()) {
                LOG(FATAL) << "Referencing a null garbled_wire_ptr";
            }
        }
    public:
        GarbledWireInstance() : garbled_wire_ptr_(nullptr) {}
        GarbledWireInstance(ulong id) : garbled_wire_ptr_(new GarbledWire(id)), inverted_(false) {}
        GarbledWireInstance(WireInstance* w) : garbled_wire_ptr_(new GarbledWire(w)), inverted_(w->inverted()) {}
        GarbledWireInstance(GarbledWire & garbled_wire) : garbled_wire_ptr_(&garbled_wire), inverted_(false) {}
        GarbledWireInstance(GarbledWire* garbled_wire_ptr) : garbled_wire_ptr_(garbled_wire_ptr), inverted_(false) {}
        GarbledWireInstance(GarbledWire & garbled_wire, bool inverted) : garbled_wire_ptr_(&garbled_wire), inverted_(inverted) {}
        GarbledWireInstance(GarbledWire* garbled_wire_ptr, bool inverted) : garbled_wire_ptr_(garbled_wire_ptr), inverted_(inverted) {}

        block label0() {
            require_not_null();
            return (inverted_) ? garbled_wire_ptr_->label1_ : garbled_wire_ptr_->label0_;
        }

        block label1() {
            require_not_null();
            return (inverted_) ? garbled_wire_ptr_->label0_ : garbled_wire_ptr_->label1_;
        }

        const block label() {
            require_not_null();
            return garbled_wire_ptr_->label_;
        }

        void set_label0(block label0) {
            require_not_null();
            garbled_wire_ptr_->label0_ = label0;
        }

        void set_label1(block label1) {
            require_not_null();
            garbled_wire_ptr_->label1_ = label1;
        }

        void set_label(block label) {
            require_not_null();
            garbled_wire_ptr_->label_ = label;
        }

        void set_id(ulong id) {
            require_not_null();
            garbled_wire_ptr_->id = id;
        }
        ulong get_id() {
            require_not_null();
            return garbled_wire_ptr_->id;
        }
        void garble(block R);

        bool inverted() {
            require_not_null();
            return inverted_;
        }
        int semanticWithLSB(int lsb);
        block labelWithSemantic(int semantic);
        block origLabelWithSemantic(int semantic);
        block labelWithLSB(int lsb);
        void setLabelWithSemantic(block label, int semantic);

    };
}
#endif //GASH_GARBLEDWIRE_H
