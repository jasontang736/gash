//
// Created by Xiaoting Tang on 10/17/17.
//

#ifndef GASH_LABELPAIR_H
#define GASH_LABELPAIR_H

#include "common.h"
namespace GASH_GC {
    typedef struct {
        block label0;
        block label1;
        int semanticWithLSB(int);
        block withLSB(int);
        block withSemantic(int);
        void setLabelWithSemantic(block label, int semantic);
    } LabelPair;
}
#endif //GASH_LABELPAIR_H
