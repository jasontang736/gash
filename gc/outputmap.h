//
// Created by Xiaoting Tang on 10/17/17.
//

#ifndef GASH_OUTPUTMAP_H
#define GASH_OUTPUTMAP_H

#include "common.h"
#include "labelpair.h"
namespace GASH_GC {
    typedef struct {
        std::map<long, LabelPair*> outputmap;
        int find(int g, block label);
        void add(long, block, int);
    } OutputMap;
}

#endif //GASH_OUTPUTMAP_H
