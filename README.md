# GArbled circuit SyntHesizer (GASH)

## Functionality

GASH is designed for compiling circuit description files (written in `gcdf`
language, which has somewhat similar syntax with C++) to binary circuits (in
`circ` format, which has similar syntax with the [And Inverter
Graph](http://fmv.jku.at/aiger/format-20070427.pdf)-AIG circuit syntax).

Then, GASH garbles the circuit described by the `circ` file (if the machine is
garbler), connects to peer, and perform Yao's protocol.

## Example - Millionaire problem

Alice holds her secret value `x0` and Bob holds the secret value `x1`, they want
to determine whether `x0 > x1` without telling each other her/his secret value.
They decide to use GASH to solve the problem

Suppose `x0=1000`, Alice writes the following circuit description file
`alice.gcdf`:

```
#definput x0 1000
#defrole GARBLER
#defport 12345

func millionaire(int32 x0, int32 x1) {
    int32 result;
    result = x0 > x1;
    return result;
}
```

Suppose `x1=500` and Alice has ip address `178.56.1.9`, Bob writes the following
circuit description file `bob.gcdf`:

```
#definput x1 500
#defrole EVALUATOR
#defport 12345
#defip 178.56.1.9

func millionaire(int32 x0, int32 x1) {
    int32 result;
    result = x0 > x1;
    return result;
}
```

Alice opens a terminal and calls
```
gash_lang -i alice.gcdf -o alice.circ -d alice.data -m normal
```

Bob opens a terminal and calls
```
gash_lang -i bob.gcdf -o bob.circ -d bob.data -m normal
```

## GCDF Syntax

## Install

GASH is tested only on Ubuntu 16.4LTS, compiling on other platforms may not
work.

GASH requires *gcc* (v5), *CMake* (v3.5 or newer), *protocol buffer* (v3), *flex*,
*bison*. Please install those packages before trying to compile.

It also requires CPUs that support SSE4 (since we will be using the flag
`-msse4`). Most Intel and AMD CPUs support it (click
[here](https://en.wikipedia.org/wiki/SSE4#Supporting_CPUs) for an extended
reference)

To install GASH on your machine, call

```
cd gash
cmake .
make
make install
```

By default it tries to install to `/usr/local/bin`, which needs root permission,
therefore call `sudo make install` to make it happen. If you want to install to
some other directories, say your home directory, use
```
cd gash
cmake -DCMAKE_INSTALL_PREFIX:PATH=${HOME} .
make
make install
```

## Test
