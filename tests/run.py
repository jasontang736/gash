import os.path
import sys
import subprocess
import time

def test_tanh_all(t = 1):

    def test_tanh(n, t = 1):

        # Check if file exists
        g_prefix = "./tanh/tanh" + str(n) + "/tanh" + str(n) + "_garbler"
        e_prefix = "./tanh/tanh" + str(n) + "/tanh" + str(n) + "_evaluator"
        g_fname =  g_prefix + ".gcdf"
        e_fname = e_prefix + ".gcdf"
        if not os.path.isfile(g_fname):
            print("File " + g_fname + " does not exists.")
            sys.exit(-1)
        if not os.path.isfile(e_fname):
            print("File " + e_fname + " does not exists.")
            sys.exit(-1)

        # Run
        for i in range(t):

            print("Running garbler...")
            g_log = open(g_prefix + ".log" + "." + str(i), "wb")
            g_err = open(e_prefix + ".err" + "." + str(i), "wb")
            g_cmd = "../bin/gash_lang" + " -i " + g_prefix + ".gcdf" +\
                    " -o " + g_prefix + ".circ" +\
                    " -d " + g_prefix + ".data" +\
                    " -m " + " normal"
            # g_p = subprocess.Popen(g_cmd, shell=True)
            g_p = subprocess.Popen(g_cmd, shell=True, stdout=g_log, stderr=g_err)

            time.sleep(2)

            print("Runnning evaluator...")
            e_log = open(e_prefix + ".log" + "." + str(i), "wb")
            e_err = open(e_prefix + ".err" + "." + str(i), "wb")
            e_cmd = "../bin/gash_lang" + " -i " + e_prefix + ".gcdf" +\
                    " -o " + e_prefix + ".circ" +\
                    " -d " + e_prefix + ".data" +\
                    " -m " + " normal"
            # e_p = subprocess.Popen(e_cmd, shell=True)
            e_p = subprocess.Popen(e_cmd, shell=True, stdout=e_log, stderr=e_err)

            g_p.wait()
            e_p.wait()

    test_tanh(8, t)
    # test_tanh(16, t)
    # test_tanh(32, t)
    # test_tanh(64, t)

def test_multi_threading_tanh(t = 1, thread = 4):

    def test_tanh(n, t = 1):
        # Check if file exists
        g_prefix = "./tanh/tanh" + str(n) + "/tanh" + str(n) + "_garbler"
        e_prefix = "./tanh/tanh" + str(n) + "/tanh" + str(n) + "_evaluator"
        g_fname =  g_prefix + ".gcdf"
        e_fname = e_prefix + ".gcdf"
        if not os.path.isfile(g_fname):
            print("File " + g_fname + " does not exists.")
            sys.exit(-1)
        if not os.path.isfile(e_fname):
            print("File " + e_fname + " does not exists.")
            sys.exit(-1)

        # Run
        for i in range(t):

            g_p_arr = []
            e_p_arr = []
            for th in range(thread):

                print("Running garbler...")
                g_log = open(g_prefix + ".log" + "." + str(i), "wb")
                g_err = open(e_prefix + ".err" + "." + str(i), "wb")
                g_cmd = "../bin/gash_lang" + " -i " + g_prefix + ".gcdf" +\
                        " -o " + g_prefix + ".circ" +\
                        " -d " + g_prefix + ".data" +\
                        " -m " + " normal"
                # g_p = subprocess.Popen(g_cmd, shell=True)
                g_p = subprocess.Popen(g_cmd, shell=True, stdout=g_log, stderr=g_err)
                g_p_arr += [g_p]

                time.sleep(2)

                print("Runnning evaluator...")
                e_log = open(e_prefix + ".log" + "." + str(i), "wb")
                e_err = open(e_prefix + ".err" + "." + str(i), "wb")
                e_cmd = "../bin/gash_lang" + " -i " + e_prefix + ".gcdf" +\
                        " -o " + e_prefix + ".circ" +\
                        " -d " + e_prefix + ".data" +\
                        " -m " + " normal"
                # e_p = subprocess.Popen(e_cmd, shell=True)
                e_p = subprocess.Popen(e_cmd, shell=True, stdout=e_log, stderr=e_err)
                e_p_arr += [e_p]

            for g_p, e_p in zip(g_p_arr, e_p_arr):
                g_p.wait()
                e_p.wait()

    test_tanh(8, t)
    test_tanh(16, t)
    test_tanh(32, t)
    test_tanh(50, t)
    test_tanh(64, t)

def main():
    test_tanh_all(10)
    # test_multi_threading_tanh(t = 10)

if __name__ == '__main__':
    main()
