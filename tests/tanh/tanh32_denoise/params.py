import random
def compute_params(x, scaling_factor=16, ring_factor=30):
    n1 = -0.54324
    n2 = -0.16957
    c1 = 1
    c2 = 0.42654
    a = 1.52
    b = 2.57
    d1 = 0.016
    d2 = 0.4519

    scaled_x = x*2**scaling_factor
    x0 = random.uniform(0, x*2) # Enough though?
    scaled_x0 = x0 * 2**scaling_factor
    x1 = x-x0
    scaled_x1 = x1 * 2**scaling_factor

    t1 = n1 * x**2 + c1 * x + d1
    t2 = n2 * x**2 + c2 * x + d2
    t3 = -n1 * x**2 + c1 * x - d1
    t4 = -n2 * x**2 + c2 * x - d2

    t10 = n1 * x0**2 + c1 * x0 + 0
    t11 = n1 * x1**2 + c1 * x1 + d1
    t20 = n2 * x0**2 + c2 * x0 + 0
    t21 = n2 * x1**2 + c2 * x1 + d2
    t30 = -n1 * x0**2 + c1 * x0 - 0
    t31 = -n1 * x1**2 + c1 * x1 - d1
    t40 = -n2 * x0**2 + c2 * x0 - 0
    t41 = -n2 * x1**2 + c2 * x1 - d2

    scaled_t10 = t10 * 2**scaling_factor
    scaled_t11 = t11 * 2**scaling_factor
    scaled_t20 = t20 * 2**scaling_factor
    scaled_t21 = t21 * 2**scaling_factor
    scaled_t30 = t30 * 2**scaling_factor
    scaled_t31 = t31 * 2**scaling_factor
    scaled_t40 = t40 * 2**scaling_factor
    scaled_t41 = t41 * 2**scaling_factor

    r = random.uniform(0, x*2)
    scaled_r = r * 2**scaling_factor

    bigNegativeA = -a * 2**scaling_factor
    bigNegativeB = -b * 2**scaling_factor
    bigPositiveA = a * 2**scaling_factor
    bigPositiveB = b * 2**scaling_factor
    bigNegativeOne = -1 * 2**scaling_factor
    bigPositiveOne = 1 * 2**scaling_factor
    half_ring_size = 2**(ring_factor-1)
    ring_size = 2**ring_factor
    ring_size_minus_1 = ring_size-1

    print("scaled_x0:", scaled_x0)
    print("scaled_x1:", scaled_x1)
    print("scaled_t10:", scaled_t10)
    print("scaled_t11:", scaled_t11)
    print("scaled_t20:", scaled_t20)
    print("scaled_t21:", scaled_t21)
    print("scaled_t30:", scaled_t30)
    print("scaled_t31:", scaled_t31)
    print("scaled_t40:", scaled_t40)
    print("scaled_t41:", scaled_t41)
    print("scaled_r:", scaled_r)
    print("bigNegativeA:", bigNegativeA)
    print("bigNegativeB:", bigNegativeB)
    print("bigPositiveA:", bigPositiveA)
    print("bigPositiveB:", bigPositiveB)
    print("bigNegativeOne:", bigNegativeOne)
    print("bigPositiveOne:", bigPositiveOne)
    print("half_ring_size:", half_ring_size)
    print("ring_size:", ring_size)
    print("ring_size_minus_1:", ring_size_minus_1)

if __name__ == '__main__':
    compute_params(0.67, 32, 60)
