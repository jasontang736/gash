%option noyywrap nodefault yylineno
%{
#include "gash_lang.h"
#include "gash_lang.tab.h"
extern "C" {int yylex(void);}
int yycolumn = 1;
#define YY_LOC {yylloc.first_line = yylineno; \
yylloc.first_column = yycolumn;                 \
yycolumn=yycolumn+yyleng;                         \
yylloc.last_column = yycolumn;                    \
yylloc.last_line = yylineno;}

%}

%%


[-]?[0-9]{1,9} { YY_LOC yylval.d = atoi(yytext); return NUMBER;        }
[-]?[0-9]{9,} { YY_LOC yylval.ld = atol(yytext); return LONG_NUMBER;   }

 /* singe character ops */

"+" |
"-" |
"*" |
"/" |
"=" |
"|" |
"^" |
"&" |
"~" |
"," |
"." |
";" |
":" |
"#" |
"@" |
"[" |
"]" |
"(" |
")" |
 /* "{" | */
 /* "}" | */
"_" { YY_LOC return yytext[0]; }

 /* scoping */
"{" {
    YY_LOC GASH_LANG::push_scope();
    yylval.ss = GASH_LANG::get_current_scope();
    return SCOPE_START;
}
"}" {
    YY_LOC yylval.se = GASH_LANG::get_current_scope();
    GASH_LANG::pop_scope();
    return SCOPE_END;
}

 /* comparison ops, all are a CMP token */
">"      { YY_LOC yylval.fn = nLA; return CMP; }
"<"      { YY_LOC yylval.fn = nLE; return CMP; }
"<>"     { YY_LOC yylval.fn = nNEQ; return CMP; }
"=="     { YY_LOC yylval.fn = nEQ; return CMP; }
">="     { YY_LOC yylval.fn = nLAEQ; return CMP; }
"<="     { YY_LOC yylval.fn = nLEEQ; return CMP; }
"!="     { YY_LOC yylval.fn = nEQ; return CMP; }

 /* keywords */

"if"    { YY_LOC return IF;    }
"then"  { YY_LOC return THEN;  }
"else"  { YY_LOC return ELSE;  }       /* we do not allow while */
"func"  { YY_LOC  GASH_LANG::next_scope_type = GASH_LANG::scope::scope_type::FUNCTION_SCOPE; return FUNC;}
"return" { YY_LOC return RETURN;}
"for"   { YY_LOC return FOR; }
"int"[1-9][0-9]*    { YY_LOC yylval.intlen = atoi(yytext+3); return INT;   }
"bundle" { YY_LOC return BUNDLE;}

 /* directives related */
"defip"    { YY_LOC return DEF_IP; }
"definput" { YY_LOC return DEF_INPUT; }
"defrole" { YY_LOC return DEF_ROLE;}
"defport" { YY_LOC return DEF_PORT;}
"defstart" { YY_LOC return DEF_START;}
"defexpout" { YY_LOC return DEF_EXPOUT;}
"GARBLER" { YY_LOC return GARBLER; }
"EVALUATOR" { YY_LOC return EVALUATOR;}


"//".*   /* comment             */
[ \t]   /* ignore white spaces */

\\\n    /* ignore line continuation */
\n      { /* ignore newline */    	}
<<EOF>>	{ YY_LOC return MYEOF;		}

 /* names. Note that numbers should precede single character ops */
[a-zA-Z_][a-zA-Z0-9_]*    { YY_LOC yylval.s = GASH_LANG::lookup(yytext); return NAME; }

.       { YY_LOC yyerror("Mystery character %c\n", *yytext); }

%%
