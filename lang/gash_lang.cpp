#include <sys/stat.h>
#include <stack>
#include "../gc/garbler.h"
#include "../gc/evaluator.h"
#include "gash_lang.h"
#include "gash_lang.tab.h"

extern FILE *yyin;

namespace GASH_LANG {

/* Utility Macros */
#define assign(bundle, r)                       \
    if (bundle.size() == r.size())              \
        bundle = r;                             \
    else if (bundle.size() < r.size())          \
        bundle = Bsubset(r, 0, bundle.size());  \
    else                                        \
        bundle.replace(r, 0, 0, r.size())       \

/* Bundle and Wire stuffs live here */
    int wcount = 0;
    int wcount_dref = 0; // de-referenced wire count
    int numIN = 0;
    int numOUT = 0;
    int numAND = 0;
    int numOR = 0;
    int numXOR = 0;
    int numDFF = 0;
    Circ circ;        // The circuit
    ExCtx ex_ctx;
//    std::stack<scope*> scope_stack;
    symbol_store sym_store;
    scope::scope_type next_scope_type;

    void outputIN(Bundle b) {
        /* output I/O wire */
        circ.in.bundle = Bconc(circ.in.bundle, b);
        numIN+=b.size();
        // std::cout << w.id << std::endl;
    }

/* Symbol look up functions*/
    class scope_stack
    {
    public:
        static scope_stack& Global()
            {
                static scope_stack    instance; // Guaranteed to be destroyed.
                // Instantiated on first use.
                return instance;
            }
    private:
        std::stack<scope*> scopes;
        scope_stack() {}                    // Constructor? (the {} brackets) are needed here.

    public:
        scope_stack(scope_stack const&)     = delete;
        void operator=(scope_stack const&)  = delete;
        scope* top() {
            return scopes.top();
        }
        void push(scope* new_scope) {
            if (scopes.size() > 0)
                new_scope->prev_scope = scopes.top();
            scopes.push(new_scope);
        }
        void pop() {
            scopes.pop();
        }
        int size() {
            return scopes.size();
        }
        void clear() {
            while (scopes.size() != 0) {
                scope* s = scopes.top();
                delete s;
                scopes.pop();
            }
        }
    };
    scope* get_current_scope() {
        return scope_stack::Global().top();
    }

    void push_scope() {
        scope* new_scope = new scope;
        scope_stack::Global().push(new_scope);
    }

    void push_scope(scope* scope) {
        scope_stack::Global().push(scope);
    }

    void pop_scope() {
        scope_stack::Global().pop();
    }

    // void set_function_params_in_current_scope() {
    //     // Copy all symbols from the previous scope to current scope,
    //     // given that the previous scope is a function scope.
    //     scope* current_scope = get_current_scope();
    //     pop_scope();
    //     scope* prev_scope = get_current_scope();
    //     if (prev_scope->type != FUNCTION_SCOPE) {
    //         std::cerr << "Previous scope is not function scope!";
    //         abort();
    //     }
    //     for (auto it = prev_scope->symbols.begin(); it != prev_scope->symbols.end(); ++it) {
    //         current_scope->add_symbol(*it);
    //     }
    //     push_scope(current_scope);
    // }

    static void pop_delete_scope() {
        scope* current_scope = scope_stack::Global().top();
        scope_stack::Global().pop();
        delete current_scope;
    }

    static bool at_out_most_scope() {
        return scope_stack::Global().size() == 1;
    }

    symbol* lookup(char* id) {
        return lookup(string(id));
    }

    symbol* lookup(string id) {
        /* Look up for the newest symbol with `id`
           If such symbol does not exist, create a new symbol with `id`
        */
        scope* top_scope = scope_stack::Global().top();
        scope* current_scope = top_scope;
        while (current_scope && !current_scope->has_symbol_for_id(id)) {
            current_scope = current_scope->prev_scope;
        }
        if (!current_scope) {
            symbol* sym = top_scope->new_symbol(id);
            return sym;
        } else {
            symbol* sym = current_scope->get_symbol_for_id(id);
            return sym;
        }
    }

    // symbol* lookup(string id, int version) {
    //     /* Look up symbol for specified `version` in current scope
    //      */
    //     scope* current_scope = scope_stack::Global().top();
    //     int newest_version = current_scope->get_newest_version_for_id(id);
    //     if (newest_version > version + 1) {
    //         std::cerr << "Cannot create newer symbol by more than 1 versions.";
    //         abort();
    //     }
    //     else if (newest_version == version + 1) {
    //         symbol* old_sym = current_scope->get_symbol_for_id(id);
    //         symbol* new_sym = current_scope->new_symbol(old_sym);
    //         return new_sym;
    //     }
    //     else {
    //         symbol* sym = current_scope->get_symbol_for_id_version(id, version);
    //         return sym;
    //     }
    // }

    // symbol* lookup(string id, int version, scope* scope) {
    //     /* Look up symbol from the specified `scope`
    //      */
    //     if (scope == scope_stack::Global().top()) {
    //         return lookup(id, version);
    //     }

    //     int newest_version = scope->get_newest_version_for_id(id);
    //     if (newest_version > version) {
    //         std::cerr << "Cannot create newer symbol for non-top-most scope.";
    //         abort();
    //     }
    //     else {
    //         symbol* sym = get_current_scope()->get_symbol_for_id_version(id, version);
    //         return sym;
    //     }
    // }

    bool symbol_store::has_symbol_for_id(string id) {
        auto it = this->symbols_.find(id);
        return !(it == this->symbols_.end());
    }

    bool symbol_store::examine_version_index() {
        // Examine whether each the i-th symbol has version number i-1
        // It is quite an expensive operation. Use rarely.
        for (auto it = this->symbols_.begin(); it != this->symbols_.end(); ++it) {
            string id = it->first;
            int idx = 0;
            for (auto sit = it->second.begin(); sit != it->second.end(); ++sit) {
                if ((*sit)->version != ++idx) {
                    std::cerr << "Symbol for id: " << id << " has discrepency.";
                    return false;
                }
            }
        }
        return true;
    }

    void symbol_store::require_has_symbol_for_id(string id) {
        if (!this->has_symbol_for_id(id)) {
            std::cerr << "Symbol for id: " << id << " does not exists.";
            abort();
        }
    }

    bool symbol_store::examine_version_index_for_id(string id) {
        this->require_has_symbol_for_id(id);
        auto it = this->symbols_.find(id);
        int idx = 0;
        for (auto sit = it->second.begin(); sit != it->second.end(); ++sit) {
            if ((*sit)->version != ++idx) {
                std::cerr << "Symbol for id: " << id << " has discrepency.";
                return false;
            }
        }
        return true;
    }

    int symbol_store::get_newest_version_for_id(string id) {
        if (!this->has_symbol_for_id(id)) {
            std::cerr << "Symbol for id: " << id << " does not exits.";
            abort();
        }
        return this->symbols_.find(id)->second.size();
    }

    symbol* symbol_store::get_symbol_for_id_version(string id, int version) {
        auto it = this->symbols_.find(id);
        if (it == this->symbols_.end()) {
            std::cerr << "Symbol for id: " << id << " does not exist.";
            abort();
        }
        if (it->second.size() < version - 1) {
            std::cerr << "Symbol for id: " << id << " exists, but the version " << version << "does not exists";
        }
        symbol* sym = it->second.at(version - 1);
        return sym;
    }

    symbol* symbol_store::new_symbol(string id) {
        symbol* sym;
        if (this->has_symbol_for_id(id)) {
            this->examine_version_index_for_id(id);
            int newest_version = this->get_newest_version_for_id(id);
            symbol* old_sym = this->get_symbol_for_id_version(id, newest_version);
            sym = new symbol(old_sym);
            sym->version = newest_version + 1;
            this->symbols_.find(id)->second.push_back(sym);
        } else {
            sym = new symbol(id);
            sym->version = 1;
            vector<symbol*> sym_list({sym});
            this->symbols_.insert(std::make_pair(id, sym_list));
        }
        return sym;
    }

    symbol* symbol_store::get_symbol_for_id(string id) {
        return this->get_symbol_for_id_version(id, this->get_newest_version_for_id(id));
            }

    symbol* symbol_store::new_symbol(symbol* old_sym) {
        symbol* new_sym;
        string id = old_sym->id;
        this->require_has_symbol_for_id(id);
        this->examine_version_index_for_id(id);
        int newest_version = this->get_newest_version_for_id(id);
        new_sym = new symbol(old_sym);
        new_sym->version = newest_version + 1;
        this->symbols_.find(id)->second.push_back(new_sym);
        return new_sym;
    }

    symbol_store::~symbol_store() {
        // Destroy all symbols
        for (auto it = this->symbols_.begin(); it != this->symbols_.end(); ++it) {
            string id = it->first;
            for (auto sit = it->second.begin(); sit != it->second.end(); ++sit) {
                delete *sit;
            }
        }
    }

    bool scope::has_symbol_for_id(string id) {
        if (this->symbols.size() == 0)
            return false;
        return !(this->symbols.find(id) == this->symbols.end());
    }


    bool scope::add_symbol(symbol* sym) {
        // `sym` should already be owned by sym_store;
        // So, all examination passed
        auto symbol_it = this->symbols.find(sym->id);
        if (symbol_it == this->symbols.end()) {
            vector<symbol*> symbol_list;
            symbol_list.push_back(sym);
            this->symbols.insert(std::make_pair(sym->id, symbol_list));
            return true;
        } else {
            symbol_it->second.push_back(sym);
        }
        return true;
    }

    symbol* scope::new_symbol(string id) {
        symbol* sym = sym_store.new_symbol(id);
        this->add_symbol(sym);
        return sym;
    }

    symbol* scope::new_symbol(symbol* old_sym) {
        symbol* sym = sym_store.new_symbol(old_sym);
        this->add_symbol(sym);
        return sym;
    }

    symbol* scope::get_symbol_for_id(string id) {
        scope* owner_scope = this;
        while (!owner_scope->has_symbol_for_id(id)){
            owner_scope = owner_scope->prev_scope;
            if (!owner_scope) {
                std::cerr << "Symbol with id: " << id << " does not exists in all recursive scopes.\n";
                abort();
            }
        }
        auto it = owner_scope->symbols.find(id);
        symbol* sym = it->second.at(it->second.size() - 1);
        return sym;
    }

    symbol* scope::get_return_symbol() {
        return get_symbol_for_id(string("__RETURN__"));
    }

    int scope::get_newest_version_for_id(string id) {
        scope* owner_scope = this;
        while (!owner_scope->has_symbol_for_id(id)){
            owner_scope = owner_scope->prev_scope;
            if (!owner_scope) {
                std::cerr << "Symbol with id: " << id << " does not exists in all recursive scopes.\n";
                abort();
            }
        }
        auto it = owner_scope->symbols.find(id);
        int version_from_sym = it->second.at(it->second.size() - 1)->version;
        return version_from_sym;
    }

    symbol* scope::get_symbol_for_id_version(string id, int version) {
        if (version == this->get_newest_version_for_id(id)) {
            return this->get_symbol_for_id(id);
        }

        auto it = this->symbols.find(id);
        auto sit = it->second.end() - 1;
        while ((*sit)->version != version) {
            if (sit == it->second.begin()) {
                std::cerr << "Symbol " << id << " of version " << version << " does not exist.";
                abort();
            }
            sit--;
        }
        symbol* sym = *sit;
        return sym;
    }

    symbol::symbol(string id) {
        this->id = id;
        this->version = 1;
    }

    symbol::symbol(symbol* rhs) {
        this->id = rhs->id;
        this->value = rhs->value;
        this->array_type = rhs->array_type;
        this->version = 0;
        this->bundle = Bundle(rhs->bundle.size());  // new bundle
        for (int i = 0; i < rhs->bundles.size(); ++i) {  // new bundles
            this->bundles.emplace_back(rhs->bundles.at(i).size());
        }
        this->func = rhs->func;
        this->defs = rhs->defs;
    }

/* Utility functions */
    int get_bit(int a, int b) {
        // get the b-th bit from a
        return (a >> b) & 1;
    }

    int get_bit(long a, int b) {
        // get the b-th bit from a
        return (a >> b) & 1;
    }


    int eval_bit(int a, int b, int func) {
        return get_bit(func, a + (b << 1));
    }

    void bin2wires(vector<int> bins, vector<Wire*>& wires) {
        for (int i = 0; i < bins.size(); i++) {
            Wire* w = new Wire(++(++wcount), bins[i]);
            wires.push_back(w);
        }
    }

    int getbit(int n, int j) {
        return (n >> j) & 1;
    }

    vector<int> int2bin(int n) {
        std::vector<int> ret;
        int i = 1;
        int l = 1;
        if (n == 0) {
            ret.push_back(0);
            return ret;
        }
        while (i < n) {
            i = i << 1;
            l += 1;
        }
        for (int j = 0; j < l; j++) {
            ret.push_back(getbit(n, j));
        }
        return ret;
    }

    vector<int> long2bin(long n) {
        std::vector<int> ret;
        int i = 1;
        int l = 1;
        if (n == 0) {
            ret.push_back(0);
            return ret;
        }
        while (i < n) {
            i = i << 1;
            l += 1;
        }
        for (int j = 0; j < l; j++) {
            ret.push_back(getbit(n, j));
        }
        return ret;
    }

    inline bool is_odd(int n) {
        return !(n % 2 == 0);
    }

    inline int evenify(int n) {
        return is_odd(n) ? n-1 : n;
    }
/*** Structs definitions ***/

/* Wire */
    void Wire::invert() {
        if (is_odd(this->id)) {
            this->id--;
        } else {
            this->id++;
        }
    }

    void Wire::set_id_even() {
        if (is_odd(this->id)) {
            this->id--;
        }
    }

    void Wire::set_id_odd() {
        if (!is_odd(this->id)) {
            this->id++;
        }
    }

    void Wire::invert_from(Wire* w) {
        if (is_odd(w->id)) {
            this->set_id_even();
        } else {
            this->set_id_odd();
        }
    }

/* Bundle */
    Bundle::Bundle() {}

    Bundle::Bundle(vector<Wire*> ws) { wires = ws; }

    Bundle::Bundle(Wire* w) { wires.push_back(w); }

    Bundle::Bundle(int len) {
        for (int i = 0; i < len; i++) {
            Wire* w = nextwire();
            w->v = 0;  // Default value is 0.
            wires.push_back(w);
        }
    }

    void Bundle::add(Wire* w) { wires.push_back(w); }

    Wire* &Bundle::operator[](int i) { return wires[i]; }

    int Bundle::size() { return wires.size(); }

    void Bundle::replace(Bundle r, int start, int rstart, int size) {
        if (size > this->size() - start) {
            cerr << "replacement has not enough room for " << size << " wires." << endl;
            exit(-1);
        }
        if (r.size() - rstart < size) {
            cerr << "replacement source has not enough wires for " << size << " wires." << endl;
            exit(-1);
        }
        for (int i = 0; i < size; ++i) {
            this->wires[i + start] = r[i + rstart];
        }
    }

    void Bundle::clearValue() {
        for (auto it = this->wires.begin(); it != this->wires.end(); ++it) {
            (*it)->v = -1;
        }
    }

/* sPrologue */
    sPrologue::sPrologue() {}

    sPrologue::sPrologue(int aNumVAR, int aNumIN, int aNumOUT, int aNumAND, int aNumOR, int aNumXOR, int aNumDFF)
        : numVAR(aNumVAR), numIN(aNumIN), numOUT(aNumOUT), numAND(aNumAND), numOR(aNumOR), numXOR(aNumXOR),
          numDFF(aNumDFF) {}

    void sPrologue::emit(std::ostream &outstream) {
        outstream << "circ" << ' ' << numVAR << ' ' << numIN << ' ' << numOUT << ' '
                  << numAND << ' ' << numOR << ' ' << numXOR << ' ' << numDFF << endl;
    }

/* sGate */
    sGate::sGate() {}

    sGate::sGate(Wire* aOut, Op aOp, Wire* aIn1, Wire* aIn2) : out(aOut), op(aOp), in1(aIn1), in2(aIn2) {
        out->from_gate = this;
        in1->to_gates.push_back(this);
        in2->to_gates.push_back(this);
    }

    void sGate::emit(std::ostream &outstream) {
        if (in1->v >= 0) {    // A constant wire
            if (in2->v >= 0) {
                outstream << evenify(out->id) << ' ' << op << ' ' <<
                    in1->id << '(' << in1->v << ')' << ' ' <<
                    in2->id << '(' << in2->v << ')' << ' ' << std::endl;
            } else {
                    outstream << evenify(out->id) << ' ' << op << ' ' <<
                    in1->id << '(' << in1->v << ')' << ' ' <<
                    in2->id << ' ' << std::endl;
            }
        } else {
            if (in2->v >= 0) {
                outstream << evenify(out->id) << ' ' << op << ' ' <<
                    in1->id << ' ' <<
                    in2->id << '(' << in2->v << ')' << ' ' << std::endl;
            } else {
                    outstream << evenify(out->id) << ' ' << op << ' ' <<
                    in1->id << ' ' <<
                    in2->id << ' ' << std::endl;
            }
        }
    }

/* sGates */
    void sGates::emit(std::ostream &outstream) {
        for (sGate* g: gates)
            g->emit(outstream);
    }

/* sIO */
    void sIO::emit(std::ostream &outstream) {
        for (Wire* w: bundle.wires)
            if (w->v < 0)
                outstream << evenify(w->id) << std::endl;
            else
                outstream << evenify(w->id) << ":" << w->v << std::endl;
    }

/* Circ */
    Circ::Circ() {}

    void Circ::output() {
        prologue.emit(*outstream_ptr);
        in.emit(*outstream_ptr);
        out.emit(*outstream_ptr);
        gates.emit(*outstream_ptr);
    }

    void Circ::addData(long value, Bundle &b) {
        int wid;
        int dup_wid;
        int bit;
        int len = b.size();

        for (int i = 0; i < len; ++i) {
            bit = get_bit(value, i);
            wid = b[i]->id;
            circ.data.push_back(std::make_pair(wid, bit));

            // Also assign values to duplicate input wires.
            auto it = this->input_duplicates.find(wid);
            if (it != this->input_duplicates.end()) {
                dup_wid = it->second;
                circ.data.push_back(std::make_pair(dup_wid, 1-bit)); // Give it the inverted bit.
            }
        }
    }

    void Circ::outputData() {
        int wid, src_wid, b;
        std::ostream &s = *data_stream_ptr;
        s << "input" << ' ' << circ.data.size() << endl;
        for (int i = 0; i < circ.data.size(); i++) {
            wid = circ.data[i].first;
            b = circ.data[i].second;
            s << evenify(wid) << ' ' << b << endl;
        }
        // // Output input duplicates
        // for (auto it = input_duplicates.begin(); it != input_duplicates.end(); ++it) {
        //     src_wid = it->first;
        //     wid = it->second;
        //     b = 1 - circ.data[src_wid].second; // invert it
        //     s << evenify(wid) << ' ' << b << endl;
        // }
        ((std::ofstream&) s).close();
    }

    void Circ::setOutstream(std::ostream &out) {
        outstream_ptr = &out;
    }

    void Circ::setDataOutstream(std::ostream &out) {
        data_stream_ptr = &out;
    }

    void Circ::addInputInvertDuplicate(Wire* orig_w, Wire* duplicate_w) {
        int orig_id = orig_w->id;
        int duplicate_id = duplicate_w->id;
        auto it = this->input_duplicates.find(orig_id);
        if (it != this->input_duplicates.end()) {
            std::cerr << "Input wire with id " << orig_id << " is already duplicated.";
            abort();
        }
        this->input_duplicates.insert(std::make_pair(orig_id, duplicate_id));
        this->addInvertedWire(orig_w, duplicate_w);
        outputIN(Bundle(duplicate_w));
    }

    bool Circ::isInputWire(Wire* w) {
        Wire* _w;
        for (int i = 0; i < this->in.bundle.size(); ++i) {
            _w = this->in.bundle[i];
            if (w == _w) return true;
            if (w->id == _w->id) {
                std::cerr << "Two wire pointers point to the same wire instance. ID:"  << w->id;
                if (w->v != _w->v) {
                    std::cerr << "Two wire pointers point to the same wire instance but have different value.";
                    abort();
                }
                return true;
            }
        }
        return false;
    }

    bool Circ::hasInputInvertDuplicate(Wire* w) {
        return this->input_duplicates.find(w->id) != this->input_duplicates.end();
    }

    bool Circ::hasInvertedWire(Wire* w) {
        if (this->wire_inverts.find(w) == this->wire_inverts.end())
            return false;
        return true;
    }

    Wire* Circ::getInvertedWire(Wire* w) {
        if (!this->hasInvertedWire(w)) {
            std::cerr << "Wire " << w->id << " does not have inverted wire.";
        }
        auto it = this->wire_inverts.find(w);
        return it->second;
    }

    void Circ::addInvertedWire(Wire* w, Wire* inv_w) {
        if (this->hasInvertedWire(w)) {
            std::cerr << "Wire " << w->id << " already has inverted wire.";
            abort();
        }
        this->wire_inverts.insert(std::make_pair(w, inv_w));
    }

    void outputData() {
        circ.outputData();
    }


/* symref */
    symref::symref() : nodetype(nREF) {}

/* Bundle manipulation functions */
    Bundle Bconc(Wire* w, Bundle b) {
        b.add(w);
        return b;
    }

    Bundle Bconc(Bundle b, Wire* w) {
        b.add(w);
        return b;
    }

    Bundle Bconc(Bundle b1, Bundle b2) {
        for (int i = 0; i < b2.size(); i++) {
            b1.add(b2[i]);
        }
        return b1;
    }

    Bundle Bdup(Wire* w, int t) {
        Bundle b;
        for (int i = 0; i < t; i++) {
            b.add(w);
        }
        return b;
    }

    Bundle Bsubset(Bundle b, int start, int end) {
        // Take the subset of wires from idx=start to idx=end-1
        Bundle r;
        for (int i = start; i < end; i++) {
            r.add(b[i]);
        }
        return r;
    }

    vector<Bundle> newBundles(int bitlen, int size) {
        vector<Bundle> vb;
        Bundle b;
        for (int i = 0; i < size; i++) {
            b = Bundle(bitlen);
            vb.push_back(b);
        }
        return vb;
    }

    Bundle Bmerge(vector<Bundle> vb) {
        Bundle ret;
        for (int i = 0; i < vb.size(); i++) {
            ret = Bconc(ret, vb[i]);
        }
        return ret;
    }

    Bundle number2Bundle(long num) {
        Wire* w;
        vector<Wire*> vec;
        if (num == 0) {
            w = nextwire();
            w->v = 0;
            w->from_gate = NULL;
            vec.push_back(w);
            return Bundle(vec);
        }
        while (num > 0) {
            w = nextwire();
            w->v = num % 2 == 1;
            num >>= 1;
            vec.push_back(w);
        }
        return Bundle(vec);
    }

/* Wire-level output functions */
    Wire* outputAND(Wire* a, Wire* b) {
        Wire* o = nextwire();
        output(o, AND, a, b);
        return o;
    }

    Wire* outputOR(Wire* a, Wire* b) {
        Wire* o = nextwire();
        output(o, OR, a, b);
        return o;
    }

    Wire* outputXOR(Wire* a, Wire* b) {
        Wire* o = nextwire();
        output(o, XOR, a, b);
        return o;
    }

    Wire* Winvert(Wire* w) {
        // If `w` has been used once, duplicate the from gate and return the new
        // wire with inverted invertibility.
        // If `w` has been used once and is a constant wire, return a new wire with
        // inverted value.
        Wire* new_w;
        if (w->used_once()) {
            if (!circ.isInputWire(w)) {
                if (circ.hasInvertedWire(w)) {
                    new_w = circ.getInvertedWire(w);
                } else {
                    if (w->v >= 0) {
                        new_w = nextwire();
                        new_w->v = w->v ^ 1;
                    } else {
                        sGate* from_gate = w->from_gate;
                        if (!from_gate) {
                            std::cerr << "From gate is null.";
                            abort();
                        }
                        new_w = nextwire();
                        new_w->invert_from(w);
                        circ.addInvertedWire(w, new_w);
                        circ.addInvertedWire(new_w, w);
                        output(new_w, from_gate->op, from_gate->in1, from_gate->in2);
                    }
                }
                return new_w;
            } else {
                if (circ.hasInputInvertDuplicate(w)) {
                    new_w = circ.getInvertedWire(w);
                } else {
                    new_w = nextwire();
                    circ.addInputInvertDuplicate(w, new_w);
                }
                return new_w;
            }
        } else {
            if (w->v >= 0) {
                w->v = w->v ^ 1;
            } else {
                w->invert();
            }
            return w;
        }
    }

    Wire* outputFADD(Wire*& cin, Wire* a, Wire* b) {
        Wire* xor1 = nextwire();
        output(xor1, XOR, a, b);
        Wire* s = nextwire();
        output(s, XOR, xor1, cin);
        Wire* and1 = nextwire();
        output(and1, AND, xor1, cin);
        Wire* and2 = nextwire();
        output(and2, AND, a, b);
        Wire* newcin = nextwire();
        output(newcin, OR, and1, and2);
        cin = newcin;
        return s;
    }

    Wire* outputFSUB(Wire*& bout, Wire* a, Wire* b, Wire* first_bout) {
        Wire* xor1 = nextwire();
        output(xor1, XOR, b, first_bout);
        return outputFADD(bout, xor1, a);
    }

    Wire* outputLA(Bundle as, Bundle bs) {
        /* output the result of as > bs
         * suppose A and B are four bits integers
         * (A > B) = A_3*B_3' + x_3*A_2*B_2' + x_3*x_2*A_1*B_1' +
         * x_3*x_2*x_1*A_0*B_0'
         * (A < B) = A_3'*B_3 + x_3*A_2'*B_2 + x_3*x_2*A_1'*B_1 +
         * x_3*x_2*x_1*A_0'*B_0
         * (A == B) = x_3*x_2*x_1*x_0, where x_i = (A_i ^ B_i)'
         */
        assert(as.size() == bs.size());
        int l = as.size();
        Bundle xs;
        for (int i = 0; i < as.size(); ++i) {
            xs.add(Winvert(outputXOR(as[i], bs[i])));
        }
        Wire* ret = ZERO;
        Wire* active_x;
        for (int i = 0; i < l; ++i) {
            if (i == 0)
                active_x = ONE;
            else
                active_x = outputAND(active_x, xs[l - i]);
            ret = outputOR(ret, outputAND(active_x, outputAND(as[l - i - 1], Winvert(bs[l - i - 1]))));
        }
        return ret;
    }

    Wire* outputLE(Bundle as, Bundle bs) {
//        assert(as.size() == bs.size());
//        int l = as.size();
//        Bundle xs;
//        for (int i = 0; i < l; ++i) {
//            xs.add(Winvert(outputXOR(as[i], bs[i])));
//        }
//
//        Wire* ret = ZERO;
//        for (int i = 0; i < l; ++i) {
//            ret = outputOR(ret, outputAND())
//        }
        return outputLA(bs, as);
    }

    Wire* outputEQ(Bundle as, Bundle bs) {
        assert(as.size() == bs.size());
        Wire* ret;
        for (int i = 0; i < as.size(); i++) {
            if (i == 0)
                ret = Winvert(outputXOR(as[i], bs[i]));
            else
                ret = outputAND(ret, Winvert(outputXOR(as[i], bs[i])));
        }
        return ret;
    }


    Wire* outputLAEQ(Bundle as, Bundle bs) {
        assert(as.size() == bs.size());
        int l = as.size();
        Bundle xs;
        Wire* eq;
        Wire* tmp;
        for (int i = 0; i < as.size(); i++) {            // Build xs
            if (i == 0) {
                eq = Winvert(outputXOR(as[i], bs[i]));
                xs.add(eq);
            } else {
                tmp = Winvert(outputXOR(as[i], bs[i]));
                xs.add(tmp);
                eq = outputAND(eq, tmp);
            }
        }
        Wire* ret = ZERO;
        Wire* active_x;
        for (int i = 0; i < as.size(); i++) {
            if (i == 0)
                active_x = ONE;
            else
                active_x = outputAND(active_x, xs[l - i]);
            ret = outputOR(ret, outputAND(active_x, outputAND(as[i], Winvert(bs[i]))));
        }
        ret = outputOR(eq, ret);
        return ret;
    }

    Wire* outputLEEQ(Bundle as, Bundle bs) {
        return outputLAEQ(bs, as);
    }

    Wire* outputNEQ(Bundle as, Bundle bs) {
        assert(as.size() == bs.size());
        Wire* ret;
        for (int i = 0; i < as.size(); i++) {
            if (i == 0)
                ret = Winvert(outputXOR(as[i], bs[i]));
            else
                ret = outputAND(ret, Winvert(outputXOR(as[i], bs[i])));
        }
        return Winvert(ret);
    }

    Wire* nextwire() {
        /* We have a global count of wire, increasesd by 2.
         * This function simply increase the `wcount` by 2 and returns a
         * Wire with that ID.
         */
        wcount += 2;                                //TODO: refactor this to remove the double
        Wire* w = new Wire(wcount);
        w->from_gate = NULL;
        return w;
    }

/* Bundle-level output functions */
    Bundle outputADD(Bundle as, Bundle bs) {
        // output's size is equal to `as.size()+1` and `bs.size()+1`
        Wire* cin;
        Bundle ret;
        assert(as.size() == bs.size());
        for (int i = 0; i < as.size(); i++) {
            Wire* a = as[i];
            Wire* b = bs[i];
            if (i == 0) cin = ZERO;
            Wire* s = outputFADD(cin, a, b);
            ret.add(s);
        }
        ret.add(cin);
        return ret;
    }

    Bundle outputSimpleADD(Bundle as, Bundle bs) {
        // output's size is equal to `as.size()` and `bs.size()`
        Wire* cin;
        Bundle ret;
        assert(as.size() == bs.size());
        for (int i = 0; i < as.size(); i++) {
            Wire* a = as[i];
            Wire* b = bs[i];
            if (i == 0) cin = ZERO;
            Wire* s = outputFADD(cin, a, b);
            ret.add(s);
        }
        return ret;
    }

    Bundle outputSUB(Bundle as, Bundle bs) {
        Bundle ret;
        Wire* bout;
        assert(as.size() == bs.size());
        for (int i = 0; i < as.size(); i++) {
            Wire* a = as[i];
            Wire* b = bs[i];
            Wire* s;
            if (i == 0) bout = ONE;
            s = outputFSUB(bout, a, b, ONE);
            ret.add(s);
        }
        return ret;
    }

    Bundle outputMUL(Bundle as, Bundle bs) {
        // output's size is equal to `as.size() + bs.size()`
        Bundle prev_muls;
        for (int i = 0; i < bs.size(); i++) {
            Bundle muls;
            for (int j = 0; j < as.size(); j++) {
                Wire* m = nextwire();
                output(m, AND, as[j], bs[i]);
                muls.add(m);
            }
            if (prev_muls.size() > 0)
                prev_muls = outputADD(prev_muls, Bconc(Bdup(ZERO, i), muls));
            else
                prev_muls = Bconc(muls, ZERO);
        }
        return prev_muls;
    }



    Bundle outputDVG(Bundle as, Bundle bs, Wire*& ret0) {
        assert(as.size() == bs.size());
        Bundle subs = outputSUB(as, bs);
        ret0 = Winvert(subs[subs.size() - 1]);
        return subs;
    }

    Bundle outputDIV(Bundle as, Bundle bs) {
        int l = as.size();
        int bigl = l * 2 - 1;
        Bundle ret;
        Wire* r;
        Bundle subs;
        for (int i = 0; i < l; i++) {
            if (i == 0)
                subs = outputDVG(Bconc(as, Bdup(ZERO, bigl - l)), Bconc(Bdup(ZERO, l - 1), bs), r);
            else
                subs = outputDVG(subs, Bconc(Bconc(Bdup(ZERO, l - i - 1), bs), Bdup(ZERO, i)), r);
            ret.add(r);
        }
        return ret;
    }

    Bundle outputANDs(Bundle as, Bundle bs) {
        /* Firstly, we determine the number of bits/wires that can hold
           both as and bs. Then we connect them, if not enough, use ZERO */
        int delta_as_bs = as.size() - bs.size();
        int smaller_size = as.size() > bs.size() ? bs.size() : as.size();
        int larger_size = as.size() < bs.size() ? bs.size() : as.size();
        Bundle ret;
        for (int i = 0; i < larger_size; i++) {
            if (i <= smaller_size) {
                ret.add(outputAND(as[i], bs[i]));
            } else {
                if (delta_as_bs > 0) {
                    ret.add(outputAND(as[i], ZERO));
                } else {
                    ret.add(outputAND(ZERO, bs[i]));
                }
            }
        }
        return ret;
    }

    Bundle outputANDs(Bundle as, Wire* w) {
        /* Connect `w` to all wires in `as` */
        Bundle ret;
        for (int i = 0; i < as.size(); ++i) {
            ret.add(outputAND(as[i], w));
        }
        return ret;
    }

    Bundle outputORs(Bundle as, Bundle bs) {
        int delta_as_bs = as.size() - bs.size();
        int smaller_size = as.size() > bs.size() ? bs.size() : as.size();
        int larger_size = as.size() < bs.size() ? bs.size() : as.size();
        Bundle ret;
        Wire* w = NULL;
        for (int i = 0; i < larger_size; i++) {
            if (i <= smaller_size) {
                w = outputOR(as[i], bs[i]);
                ret.add(w);
            } else {
                if (delta_as_bs > 0) {
                    ret.add(outputOR(as[i], ZERO));
                } else {
                    ret.add(outputOR(ZERO, bs[i]));
                }
            }
        }
        return ret;
    }

    Bundle outputXORs(Bundle as, Bundle bs) {
        int delta_as_bs = as.size() - bs.size();
        int smaller_size = as.size() > bs.size() ? bs.size() : as.size();
        int larger_size = as.size() < bs.size() ? bs.size() : as.size();
        Bundle ret;
        for (int i = 0; i < larger_size; i++) {
            if (i <= smaller_size) {
                ret.add(outputXOR(as[i], bs[i]));
            } else {
                if (delta_as_bs > 0) {
                    ret.add(outputXOR(as[i], ZERO));
                } else {
                    ret.add(outputXOR(ZERO, bs[i]));
                }
            }
        }
        return ret;
    }

    Bundle outputNEGs(Bundle as) {
        /* For each wire in the bundle, add 1 to its id and return it
         * If the wire is already used as an input wire but with different
         * invertibility, duplicate its parent gate and use the duplicated wire
         * with desired invertibility. */
        vector<Wire*> vec;
        for (int i = 0; i < as.size(); i++) {
            vec.push_back(Winvert(as[i]));
        }
        return Bundle(vec);
    }

    Bundle outputUMINUS(Bundle as) {
        /*
         * Output the 2's complement of input
         */
        Bundle bs = outputNEGs(as); // Invert the digits
        Bundle cs;
        for (int i = 0; i < bs.size(); ++i) {
            if (i == 0)
                cs.add(ONE);
            else
                cs.add(ZERO);
        }

        Bundle ds = outputADD(bs, cs); // Add 1
        return ds;
    }

    Bundle outputIfThenElse(Bundle cond, Bundle as, Bundle bs) {
        /* Q = (A*S) + (B*S') */
        assert(cond.size() == 1);
        Bundle q1 = outputANDs(as, cond);
        Bundle q2 = outputANDs(bs, outputNEGs(cond));
        Bundle q = outputORs(q1, q2);
        return q;
    }

    void outputIfThenElse(Bundle cond, Bundle as, Bundle bs, Bundle* cs) {
        // Assign `as`/`bs` to `cs` depends on `cond`
        assert(cond.size() == 1);
        Bundle q1 = outputANDs(as, cond[0]);
        Bundle q2 = outputANDs(bs, Winvert(cond[0]));
        *cs = outputORs(q1, q2);
    }

    void outputReturn() {
        if (!sym_store.has_symbol_for_id(string("__RETURN__"))) {
            std::cerr << "Return symbol is not defined." << std::endl;
            abort();
        }
        int newest_return_version = sym_store.get_newest_version_for_id(string("__RETURN__"));
        symbol* return_sym = sym_store.get_symbol_for_id_version(string("__RETURN__"), newest_return_version);
        Bundle b = return_sym->bundle;
        ex_ctx.outsizes.push_back(b.size());
        circ.out.bundle = b;
        numOUT = b.size();
        circ.prologue = sPrologue(wcount / 2, numIN, numOUT, numAND, numOR, numXOR, numDFF);
        circ.output();
    }

/* Bottom-level and output functions */
    void output(Wire*& o, Op op, Wire* a, Wire* b) {
        /* Output the four wire code
         * In the occurance of constant value, use boolean equivalence
         */

        // Use them
        a->used();
        b->used();
        if (a->v > -1 && b->v > -1) { // Two constant wires
            int func = (int) op;
            o->v = eval_bit(a->v, b->v, func);
            // wcount -= 4;
            return;
        } else if (a->v > -1) { // only a is constant wire
            switch (op) {
            case AND:
                if (a->v == 0) {
                    o->v = 0;
                } else if (a->v == 1) {
                    delete o;   // Delete the wire instance `o` points to
                    o = b;
                }
                break;
            case OR:
                if (a->v == 0) {
                    delete o;
                    o = b;
                } else if (a->v == 1) {
                    o->v = 1;
                }
                break;
            case XOR:
                if (a->v == 0) {
                    delete o;
                    o = b;
                } else if (a->v == 1) {
                    delete o;
                    o = Winvert(b);
                }
            default:
                break;
            }
            // wcount -= 2;
        } else if (b->v > -1) { // only b is constant wire
            switch (op) {
            case AND:
                if (b->v == 0) {
                    o->v = 0;
                } else if (b->v == 1) {
                    delete o;
                    o = a;
                }
                break;
            case OR:
                if (b->v == 0) {
                    delete o;
                    o = a;
                } else if (b->v == 1) {
                    o->v = 1;
                }
                break;
            case XOR:
                if (b->v == 0) {
                    delete o;
                    o = a;
                } else if (b->v == 1) {
                    delete o;
                    o = Winvert(a);
                }
            default:
                break;
            }
            // wcount -= 2;
        } else {
            circ.gates.gates.push_back(new sGate(o, op, a, b));
            switch (op) {
            case AND:
                numAND += 1;
                break;
            case OR:
                numOR += 1;
                break;
            case XOR:
                numXOR += 1;
                break;
            case DFF:
                numDFF += 1;
                break;
            }
        }
    }



/* AST check Null */
    template<typename T>
    void checkNull(const T &a) {
        if (!a) {
            yyerror("out of space");
            exit(0);
        }
    }

/* AST creation functions */
    ast *newast(int nodetype, ast *l, ast *r) {
        ast *a = new ast;
        checkNull(a);
        a->nodetype = nodetype;
        a->l = l;
        a->r = r;
        return a;
    }

    ast *newast(int nodetype, ast *l) {
        ast *a = new ast;
        checkNull(a);
        a->nodetype = nodetype;
        a->l = l;
        return a;
    }

    ast *newbundlenum(int d) {
        /* constant wires */
        /* Firstly, we should figure out how many wires this number needs
           and creates a bundle with that number of wires.
           However, when assigning this bundle to another one, we have to
           make sure they match.
        */
        bundlenum *a = new bundlenum; // gotta use new, since malloc does not initialize vector
        checkNull(a);
        a->nodetype = nBUNDLENUMBER;
        vector<Wire*> ws;
        bin2wires(int2bin(d), ws);
        for (int i = 0; i < ws.size(); ++i) {
            a->bundle.add(ws[i]);
        }
        a->value = d;
        return (ast *) a;
    }

    ast *newbundlenum(long d) {
        /* constant wires */
        /* Firstly, we should figure out how many wires this number needs
           and creates a bundle with that number of wires.
           However, when assigning this bundle to another one, we have to
           make sure they match.
        */
        bundlenum *a = new bundlenum; // gotta use new, since malloc does not initialize vector
        checkNull(a);
        a->nodetype = nBUNDLENUMBER;
        vector<Wire*> ws;
        bin2wires(long2bin(d), ws);
        for (int i = 0; i < ws.size(); ++i) {
            a->bundle.add(ws[i]);
        }
        a->value = d;
        return (ast *) a;
    }

    ast *newnum(long d) {
        /* constant values */
        num *a = new num;
        checkNull(a);
        a->nodetype = nNUMBER;
        a->ntype = longtype;
        a->lvalue = d;
        return (ast *) a;
    }

    ast *newnum(int d) {
        /* constant values */
        num *a = new num;
        checkNull(a);
        a->nodetype = nNUMBER;
        a->ntype = inttype;
        a->ivalue = d;
        return (ast *) a;
    }

    ast *newcmp(int cmptype, ast *l, ast *r) {
        ast *a = new ast;
        checkNull(a);
        a->nodetype = cmptype;
        a->l = l;
        a->r = r;
        return a;
    }

    ast *newref(symbol *s) {
        symref *a = new symref;
        checkNull(a);
        a->nodetype = nNAME;
        a->reftype = rBUNDLE;
        a->s = s;
        return (ast *) a;
    }

    ast *newasgn(ast *l, ast *v) {
        symasgn *a = new symasgn;
        checkNull(a);
        a->nodetype = nASGN;
        symref* ref = (symref *)l;
        ref->s = get_current_scope()->new_symbol(ref->s);     // Only change the materializable symbol
        a->l = ref;
        a->v = v;
        return (ast *) a;
    }

    ast *newinitasgn(mytypedef *def, ast *v) {
        symasgn *a = new symasgn;
        checkNull(a);
        a->nodetype = nASGN;
        a->l = (symref*) newref(def->s);
        a->v = v;
        return (ast *)a;
    }

    ast *newflow(int nodetype, ast *cond, ast *tl, ast *el, scope* tl_scope, scope* el_scope, scope* orig_scope) {
        flow *a = new flow;
        checkNull(a);
        a->nodetype = nodetype;
        a->cond = cond;
        a->tl = tl;
        a->el = el;
        a->tl_scope = tl_scope;
        a->el_scope = el_scope;
        a->orig_scope = orig_scope;
        return (ast *) a;
    }

    ast *newflow(int nodetype, ast *init, ast *cond, ast *inc, ast *list, scope* for_scope, scope* orig_scope) {
        flow *a = new flow;
        checkNull((ast *) a);
        a->nodetype = nFOR;
        a->init = init;
        a->cond = cond;
        a->inc = inc;
        a->list = list;
        a->for_scope = for_scope;
        a->orig_scope = orig_scope;
        return (ast *) a;
    }

    ast *newreturn(ast *l) {
        myreturn *r = new myreturn;
        checkNull(r);
        r->nodetype = nRETURN;
        r->l = l;
        r->ret_sym = get_current_scope()->new_symbol("__RETURN__");
        return (ast *) r;
    }

    mytypedef *newtypedef(int bitlen, symbol *s) {
        mytypedef *t = new mytypedef;
        checkNull(t);
        t->nodetype = nTD;
        t->bitlen = bitlen;
        t->size = 0;
        t->s = s;
        t->s->bundle = Bundle(bitlen);
        t->s->array_type = false;
        return t;
    }

    mytypedef *newtypedef(int bitlen, int size, symbol *s) {
        mytypedef *t = new mytypedef;
        checkNull(t);
        t->nodetype = nTD;
        t->size = size;
        t->bitlen = bitlen;
        t->s = s;
        t->s->bundles = newBundles(bitlen, size);
        t->s->array_type = true;
        return t;
    }

    mytypedeflist *newtypedeflist(mytypedef *l, mytypedeflist *r) {
        mytypedeflist *tl = new mytypedeflist;
        checkNull(tl);
        tl->nodetype = nTL;
        tl->def = (mytypedef *) l;
        tl->next = (mytypedeflist *) r;
        return tl;
    }

    void clearValue(mytypedeflist* list) {
        symbol* s;
        mytypedef* def;
        mytypedeflist* next = list;
        while (next) {
            def = next->def;
            s = def->s;
            s->bundle.clearValue();
            next = next->next;
        }
    }

    ast *newdirective(symbol *s, long value) {
        directive *d = new directive;
        d->nodetype = nDIR;
        d->dirtype = dSINGLE;
        d->s = s;
        d->value = value;
        return (ast *) d;
    }

    ast *newdirective(symbol *s, ast *l) {
        directive *d = new directive;
        d->nodetype = nDIR;
        d->dirtype = dARRAY;
        d->s = s;
        d->number_list = (numberlist *) l;
        return (ast *) d;
    }

    ast *newdirective(Kdirtype type, char *s) {
        directive *d = new directive;
        d->nodetype = nDIR;
        d->dirtype = type;
        d->spec = s;
        d->s = nullptr;
        return (ast *) d;
    }

    ast *newdirective(Kdirtype type, int s) {
        directive *d = new directive;
        d->nodetype = nDIR;
        d->dirtype = type;
        d->dspec = s;
        d->s = nullptr;
        return (ast *) d;
    }

    ast *newdirective(Kdirtype type, ast *r) {
        directive *d = new directive;
        d->nodetype = nDIR;
        d->dirtype = type;
        d->number_list = (numberlist *)r;
        d->s = nullptr;
        return (ast *) d;
    }

    ast *newnumberlist(int l, ast *r) {
        numberlist *a = new numberlist();
        a->nodetype = nNUMBERLIST;
        a->ntype = inttype;
        a->l = l;
        a->r = (numberlist *) r;
        return (ast *) a;
    }

    ast *newnumberlist(long l, ast *r) {
        numberlist *a = new numberlist();
        a->nodetype = nNUMBERLIST;
        a->ntype = longtype;
        a->ln = l;
        a->r = (numberlist *) r;
        return (ast *) a;
    }

    ast *newbitidx(symbol *s, ast *ba) {
        symref *a = new symref();
        checkNull(a);
        a->nodetype = nREF;
        a->reftype = rBUNDLE_dot_WIRE;
        a->s = s;
        a->ba = ba;
        return (ast *) a;
    }

    ast *newarrayidx(symbol *s, ast *va) {
        symref *a = new symref();
        checkNull(a);
        a->nodetype = nREF;
        a->reftype = rARRAY_dot_BUNDLE;
        a->s = s;
        a->va = va;
        return (ast *) a;
    }

    ast *newarrayidx(symbol *s, ast *va, ast *ba) {
        symref *a = new symref();
        checkNull(a);
        a->nodetype = nREF;
        a->reftype = rARRAY_dot_BUNDLE_dot_WIRE;
        a->s = s;
        a->va = va;
        a->ba = ba;
        return (ast *) a;
    }

/* define a function/circuit */
    void defunc(symbol *name, mytypedeflist *defs, ast *func) {
        if (name->defs) typedeflistfree(name->defs);        /* Redefinition */
        if (name->func) treefree(name->func);
        name->defs = defs;
        name->func = func;
        do {    // Put defs' bundle into input bundle
            mytypedef *def = defs->def;
            if (def->s->array_type)
                outputIN(Bmerge(def->s->bundles));
            else
                outputIN(def->s->bundle);
            defs = defs->next;
        } while (defs);
    }

/* output and eval */
    Bundle output(ast *a) {

        Bundle w;           /* Wire bundles */

        if (!a) {
            yyerror("internal erorr, null output");
        }

        switch (a->nodetype) {
            /* constant */
        case nBUNDLENUMBER:
            w = ((bundlenum *) a)->bundle;
            break;

            /* name reference */
        case nNAME: {
            if (!isBundleValid(((symref *) a)->s->bundle) &&
                !isBundleValid(((symref *) a)->s->bundles)) {
                cerr << "Symbol is neither a scalar nor array." << endl;
                exit(-1);
            } else if (isBundleValid(((symref *) a)->s->bundle) &&
                       isBundleValid(((symref *) a)->s->bundles)) {
                cerr << "Symbol is both a scalar and array." << endl;
                exit(-1);
            } else if (isBundleValid(((symref *) a)->s->bundle)) {
                symbol* new_s = sym_store.get_symbol_for_id(((symref *)a)->s->id);
                w = new_s->bundle;
                break;
            } else {
                symbol* new_s = sym_store.get_symbol_for_id(((symref *)a)->s->id);
                w = Bmerge(new_s->bundles);
            }
        }

            /* more complicated reference */
        case nREF: {
            int b, v;
            symref* sa = (symref *) a;
            symbol* new_s = sym_store.get_symbol_for_id(sa->s->id);
            switch (sa->reftype) {
            case rBUNDLE:
                if (sa->s->array_type) {
                    w = Bmerge(new_s->bundles);
                } else {
                    w = new_s->bundle;
                }
                break;
            case rBUNDLE_dot_WIRE:
                b = eval(sa->ba);
                w = Bundle(new_s->bundle[b]);
                break;
            case rARRAY_dot_BUNDLE:
                v = eval(sa->va);
                w = new_s->bundles[v];
                break;
            case rARRAY_dot_BUNDLE_dot_WIRE:
                b = eval(sa->ba);
                v = eval(sa->va);
                w = Bundle(new_s->bundles[v][b]);
                break;
            }
            break;
        }

            /* assignment */
        case nASGN: {
            // TODO if name is an array instead of an element, throw error
            Bundle r = output(((symasgn *) a)->v);
            symref *l = (symref *) ((symasgn *) a)->l;
            symbol *s = l->s;
            int b, v;
            switch (l->reftype) {
            case rBUNDLE:
                int ref_size;
                if (s->bundle.size() < r.size()) {
                    WARNING("Target symbol has less wires than given bundle. Cut overflow");
                    ref_size = s->bundle.size();
                }
                else if (s->bundle.size() > r.size()) {
                    WARNING("Target symbol has more wires than given bundle. Underflow is set to zero.");
                    ref_size = r.size();
                } else {
                    ref_size = s->bundle.size();
                }
                s->bundle.replace(r, 0, 0, ref_size);
                break;
            case rBUNDLE_dot_WIRE:
                b = eval(l->ba);
                s->bundle[b] = r[0];
                break;
            case rARRAY_dot_BUNDLE:
                v = eval(l->va);
                assign(s->bundles[v], r);
                break;
            case rARRAY_dot_BUNDLE_dot_WIRE:
                b = eval(l->ba);
                v = eval(l->va);
                s->bundles[v][b] = r[0];
                break;
            default:
                cerr << "Undefined ref type " << l->reftype << endl;
                exit(-1);
            }
            return ONE;
        }

            /* expression */
        case nPLUS:
            w = outputSimpleADD(output(a->l), output(a->r));
            break;

        case nMINUS:
            w = outputSUB(output(a->l), output(a->r));
            break;

        case nMUL:
            w = outputMUL(output(a->l), output(a->r));
            break;

        case nDIV:
            w = outputDIV(output(a->l), output(a->r));
            break;

        case nBOR:
            w = outputORs(output(a->l), output(a->r));
            break;

        case nBAND:
            w = outputANDs(output(a->l), output(a->r));
            break;

        case nBXOR:
            w = outputXORs(output(a->l), output(a->r));
            break;

        case nBNEG:
            w = outputNEGs(output(a->l));
            break;

        case nUMINUS:
            w = outputUMINUS(output(a->l));
            break;

            /* comparisons */
        case nLA:
            w = Bundle(outputLA(output(a->l), output(a->r)));
            break;

        case nEQ:
            w = Bundle(outputEQ(output(a->l), output(a->r)));
            break;

        case nLE:
            w = Bundle(outputLE(output(a->l), output(a->r)));
            break;

        case nLEEQ:
            w = Bundle(outputLEEQ(output(a->l), output(a->r)));
            break;

        case nNEQ:
            w = Bundle(outputNEQ(output(a->l), output(a->r)));
            break;

            /* Number */
        case nNUMBER:
            w = number2Bundle((long)floor(eval(a)));
            break;

            /* if-then-else */
        case nIF:
        {
            Bundle cond = output(((flow *) a)->cond);
            scope* tl_scope;
            scope* el_scope;
            scope* orig_scope;

            output(((flow *) a)->tl);
            tl_scope = ((flow *) a)->tl_scope;
            orig_scope = ((flow *) a)->orig_scope;

            if (((flow *) a)->el) {
                output(((flow *) a)->el);
                el_scope = ((flow *) a)->el_scope;
            }

            map<string, pair<int, int>> tl_el_version_map;
            map<string, int> el_only_version_map;
            map<string, int> tl_only_version_map;

            if (el_scope) { // Has both if and else
                for (auto it = tl_scope->symbols.begin(); it != tl_scope->symbols.end(); ++it) {
                    string id = it->first;
                    symbol* tl_sym = tl_scope->get_symbol_for_id(id);
                    if (el_scope->has_symbol_for_id(id)) {
                        symbol* el_sym = el_scope->get_symbol_for_id(id);
                        tl_el_version_map.insert(std::make_pair(id, std::make_pair(tl_sym->version, el_sym->version)));
                    }
                    else {
                        tl_only_version_map.insert(std::make_pair(id, tl_sym->version));
                    }
                }
                for (auto it = el_scope->symbols.begin(); it != el_scope->symbols.end(); ++it) {
                    string id = it->first;
                    if (!tl_scope->has_symbol_for_id(id)) {
                        symbol* el_sym = el_scope->get_symbol_for_id(id);
                        el_only_version_map.insert(std::make_pair(id, el_sym->version));
                    }
                }
            }
            else {
                // Only has if then statement
                for (auto it = tl_scope->symbols.begin(); it != tl_scope->symbols.end(); ++it) {
                    string id = it->first;
                    symbol* tl_sym = tl_scope->get_symbol_for_id(id);
                    tl_only_version_map.insert(std::make_pair(id, tl_sym->version));
                }
            }
            // Output tl_el_version_map with `outputIfThenElse`
            for (auto it = tl_el_version_map.begin(); it != tl_el_version_map.end(); ++it) {
                string id = it->first;
                pair<int, int> pair = it->second;
                int tl_version = pair.first;
                int el_version = pair.second;
                symbol* tl_sym = tl_scope->get_symbol_for_id_version(id, tl_version);
                symbol* el_sym = el_scope->get_symbol_for_id_version(id, el_version);
                symbol* new_sym = orig_scope->new_symbol(tl_sym);
                outputIfThenElse(cond, tl_sym->bundle, el_sym->bundle,
                                 &new_sym->bundle);
            }
            // Output tl_only_version_map
            for (auto it = tl_only_version_map.begin(); it != tl_only_version_map.end(); ++it) {
                string id = it->first;
                int tl_version = it->second;
                symbol* tl_sym = tl_scope->get_symbol_for_id_version(id, tl_version);
                symbol* old_sym = orig_scope->get_symbol_for_id(id);
                symbol* new_sym = orig_scope->new_symbol(tl_sym);
                outputIfThenElse(cond, tl_sym->bundle, old_sym->bundle, &new_sym->bundle);
            }
            // Output el_only_version_map
            for (auto it = el_only_version_map.begin(); it != el_only_version_map.end(); ++it) {
                string id = it->first;
                int el_version = it->second;
                symbol* el_sym = el_scope->get_symbol_for_id_version(id, el_version);
                symbol* old_sym = orig_scope->get_symbol_for_id(id);
                symbol* new_sym = orig_scope->new_symbol(el_sym);
                outputIfThenElse(cond, el_sym->bundle, old_sym->bundle, &new_sym->bundle);
            }

            // Delete tl_scope and el_scope because we no longer need them.
            delete tl_scope;
            delete el_scope;
            return ONE;
        }
        break;

        /* For-loop */
        case nFOR:
            eval(((flow *) a)->init);
            while (eval(((flow *) a)->cond)) {
                w = output(((flow *) a)->list);
                eval(((flow *) a)->inc);
            }
            break;

            /* list */
        case nLIST:
            if (a->r) {
                if (a->l)
                    output(a->l);
                w = output(a->r);
            } else {
                w = output(a->l);
            }
            break;

            /* return */
        case nRETURN: {
            ast *l = a->l;
            symbol* return_sym = ((myreturn *)a)->ret_sym;
            Bundle r = output(l);
//            ex_ctx.outsizes.push_back(r.size());
            return_sym->bundle = r;
            return ONE;
            break;
        }
        case nTD: { // Inner variable declaration
            /* do nothing */
            break;
        }
        default: {
            cerr << "Undefined node" << endl;
            exit(-1);
        }
        }
        return w;
    }

    double eval(ast *a) {
        // Evaluate intermediate expression, mainly for flow control
        // init -> assignment
        // cond -> cmp
        // inc -> assignment
        double v;
        switch (a->nodetype) {
        case nASGN: {
            // Currently only support inner scalar variable assignment through eval
            symref *l = ((symasgn *) a)->l;
            l->s->value = eval(((symasgn *) a)->v);
            v = 1;
            break;
        }
        case nLA:
            v = eval(a->l) > eval(a->r);
            break;
        case nEQ:
            v = eval(a->l) == eval(a->r);
            break;
        case nLE:
            v = eval(a->l) < eval(a->r);
            break;
        case nLAEQ:
            v = eval(a->l) >= eval(a->r);
            break;
        case nLEEQ:
            v = eval(a->l) <= eval(a->r);
            break;
        case nNEQ:
            v = eval(a->l) != eval(a->r);
            break;
        case nPLUS:
            v = eval(a->l) + eval(a->r);
            break;
        case nMINUS:
            v = eval(a->l) - eval(a->r);
            break;
        case nMUL:
            v = eval(a->l) * eval(a->r);
            break;
        case nDIV:
            v = eval(a->l) / eval(a->r);
            break;
        case nNAME: {
            double value = ((symref *) a)->s->value;
            if (value < 0) {  // Check whether the symbol does not has value
                // TODO support negative address/iterator indexing, probably?
                cerr << "Symbol " << ((symref *) a)->s->id<< " does not has value." << endl;
                exit(-1);
            }
            v = value;
            break;
        }
        case nNUMBER:
            if (((num *)a)->ntype == inttype) {
                v = (double)((num *) a)->ivalue;
            } else {
                v = (double)((num *) a)->lvalue;
            }
            break;
        default: {
            cerr << "Unsupported eval ast" << endl;
            exit(-1);
        }
        }
        return v;
    }

/* directives */
    char *newip(int a, int b, int c, int d) {
        stringstream ss;
        ss << a << '.' << b << '.' << c << '.' << d;
        string s = ss.str();
        char *cc = new char[s.size()];
        strcpy(cc, s.c_str());
        return cc;
    }


    char *newtime(Ktimetype type, int time) {
        stringstream ss;
        string s;
        char *c;
        if (type == tDELTA) {
            ss << '+' << time;
            s = ss.str();
            c = new char[s.size()];
            strcpy(c, s.c_str());
        } else {
            cerr << "Unsupported time type:" << type << endl;
            exit(-1);
        }
        return c;
    }

    char *newtime(Ktimetype type, int hr, int min) {
        stringstream ss;
        string s;
        char *c;
        if (type == tSTAMP) {
            ss << '@' << hr << ':' << min;
            s = ss.str();
            c = new char[s.size()];
            strcpy(c, s.c_str());
        } else {
            cerr << "Unsupported time type:" << type << endl;
            exit(-1);
        }
        return c;
    }

    char *newtime(Ktimetype type, int hr, int min, int sec) {
        stringstream ss;
        string s;
        char *c;
        if (type == tSTAMP) {
            ss << '@' << hr << ':' << min << ':' << sec;
            s = ss.str();
            c = new char[s.size()];
            strcpy(c, s.c_str());
        } else {
            cerr << "Unsupported time type:" << type << endl;
            exit(-1);
        }
        return c;
    }

    void evaldir(ast *a) {

        if (!a) {
            yyerror("internal erorr, null directive");
        }

        switch (a->nodetype) {
        case nDIR: {
            directive *da = (directive *) a;
            symbol *s = da->s;
            if (s)
                s = sym_store.get_symbol_for_id(s->id);
            Kdirtype type = (Kdirtype) da->dirtype;
            if (type == dSINGLE) {  // Single-value directive
                long value = da->value;
                if (!isBundleValid(s->bundle)) {
                    cerr << "Single-valued directive does not indicate symbol with\
                single bundle" << endl;
                    exit(-1);
                }
                Bundle b = s->bundle;
                circ.addData(value, b);
                break;
            } else if (type == dARRAY) { // array directive
                if (!isBundleValid(s->bundles)) {
                    cerr << "Array-valued directive does not indicate symbol with\
                vector bundle" << endl;
                    exit(-1);
                }

                vector<Bundle> vb = s->bundles;
                int bundleidx = 0;

                numberlist *current = da->number_list;
                int ivalue;
                long lvalue;

                while (current) {
                    if (current->ntype == inttype) {
                        ivalue = current->l;
                        circ.addData(ivalue, vb[bundleidx++]);
                        current = current->r;
                    } else {
                        lvalue = current->ln;
                        circ.addData(lvalue, vb[bundleidx++]);
                        current = current->r;
                    }
                }

                if (vb.size() != bundleidx) {
                    cerr << "Bundle array size mismatch with directive. \
                         bundle size:" << vb.size() << "diretive size:"\
                         << bundleidx + 1 << endl;
                    exit(-1);
                }

            } else if (type == dROLE) {
                ex_ctx.role = da->spec;
                break;
            } else if (type == dPORT) {
                ex_ctx.port = da->dspec;
                break;
            } else if (type == dIP) {
                ex_ctx.ip = da->spec;
                break;
            } else if (type == dTIME) {
                ex_ctx.startat = da->spec;
                break;
            } else if (type == dEXPOUT) {
                numberlist *current = da->number_list;
                long lvalue;
                while (current) {
                    if (current->ntype == inttype) {
                        lvalue = (long)current->l;
                    } else {
                        lvalue = current->ln;
                    }
                    ex_ctx.expout.push_back(lvalue);
                    current = current->r;
                }
                break;
            } else { // empty directive
                cerr << "Empty directive" << endl;
                exit(-1);
            }
            break;
        }
        case nDIRLIST: {
            ast *l = a->l;
            ast *r = a->r;
            evaldir(l);
            evaldir(r);
            break;
        }
        default: {
            cerr << "Invalid dir nodetype" << a->nodetype << endl;
            exit(-1);
        }
        }
    }

    int verify(ExCtx &ctx) {
        if (strcmp(ctx.role, "GARBLER") == 0) {

            GASH_GC::Garbler garbler(ctx.port);
            garbler.buildCircuit(string(ctx.circfile));
            garbler.readInput(ctx.datafile);
            garbler.simpleExecuteCircuit();
            garbler.descOutput();
            std::cout << "Other wires: " << std::endl;
            garbler.descWires();

        } else if (strcmp(ctx.role, "EVALUATOR") == 0) {

            GASH_GC::Evaluator evaluator(string(ctx.ip), ctx.port);
            evaluator.buildCircuit(string(ctx.circfile));
            evaluator.readInput(ctx.datafile);
            evaluator.simpleExecuteCircuit();
            evaluator.descOutput();

        } else {
            std::cerr << "Invalid role. " << std::endl;
            abort();
        }
    }

    int execute(ExCtx &ctx) {
        if (strcmp(ctx.role, "GARBLER") == 0) {
            // Garbler
            GASH_GC::Garbler garbler(ctx.port);

#ifdef GASH_TIMER
            GASH_GC::Timer timer = GASH_GC::Timer();
            timer.tic("Build Circuit");
            garbler.buildCircuit(string(ctx.circfile));
            garbler.readInput(string(ctx.datafile));
            timer.toc();

            timer.tic("Garbling Circuit");
            garbler.garbleCircuit();
            timer.toc();

            // garbler.print();
            garbler.buildConnection();

            timer.tic("Send garbled tables");
            garbler.sendGarbledTables();
            timer.toc();

            timer.tic("Send self labels");
            garbler.sendSelfLabels();
            timer.toc();

            timer.tic("Send peer labels");
            garbler.sendPeerLabels();
            timer.toc();
#ifndef GASH_NO_OT
            timer.summarize();
#endif

            timer.tic("Send output map");
            garbler.sendOutputMap();
            timer.toc();

            timer.summarize();
            garbler.recvOutput();
            garbler.descOutput();

#ifdef GASH_DEBUG
            garbler.recvLabelsForDebug();
            garbler.descGarbledWires();
#endif

            vector<bool> bits;
            garbler.getOutput(bits);
            ctx.out = bin2ints(bits, ctx.outsizes);
            if (verify(ctx.out, ctx.expout)) {
                cout << "\033[1;32mCorrect output, congrat!\033[0m" << endl;
            } else {
                cout << "\033]1;31mWrong output!\033[0m" << endl;
            }
#else
            garbler.buildCircuit(string(ctx.circfile));
            garbler.readInput(string(ctx.datafile));
            garbler.garbleCircuit();
            // garbler.print();
            garbler.buildConnection();
            garbler.sendGarbledTables();
            garbler.sendSelfLabels();
            garbler.sendPeerLabels();
            garbler.sendOutputMap();
            garbler.recvOutput();
            ctx.out = bin2ints(garbler.getOutput(), ctx.outsizes);
            garbler.shutdown();
            if (verify(ctx.out, ctx.expout)) {
                cout << "\033[1;32mCorrect output, congrat!\033[0m" << endl;
            } else {
                cout << "\033]1;31mWrong output!\033[0m" << endl;
            }
#endif

        } else if (strcmp(ctx.role, "EVALUATOR") == 0) {
            // Evaluator
            GASH_GC::Evaluator evaluator(string(ctx.ip), ctx.port);
#ifdef GASH_TIMER
            GASH_GC::Timer timer = GASH_GC::Timer();
            timer.tic("Build circuit");
            evaluator.buildCircuit(string(ctx.circfile));
            timer.toc();

            evaluator.readInput(string(ctx.datafile));
            evaluator.buildConnection();

            timer.tic("Receive garbled tables");
            evaluator.recvGarbledTables();
            timer.toc();

            timer.tic("Receive peer labels");
            evaluator.recvPeerLabels();
            timer.toc();

            timer.tic("Receive self labels");
            evaluator.recvSelfLabels();
            timer.toc();

#ifndef GASH_NO_OT
            timer.summarize();
#endif

            timer.tic("Receive outputmap");
            evaluator.recvOutputMap();
            timer.toc();

            timer.tic("Garbling circuit");
            evaluator.garbleCircuit();
            timer.toc();

            // evaluator.print();
            timer.tic("Evaluate circuit");
            evaluator.evaluateCircuit();
            timer.toc();

            evaluator.sendOutput();
            evaluator.descOutput();

#ifdef GASH_DEBUG
            evaluator.sendLabelsForDebug();
#endif

            vector<bool> bits;
            evaluator.getOutput(bits);
            ctx.out = bin2ints(bits, ctx.outsizes);
            // evaluator.shutdown()
            if (verify(ctx.out, ctx.expout)) {
                cout << "\033[1;32mCorrect output, congrat!\033[0m" << endl;
            } else {
                cout << "\033[1;31mWrong output!\033[0m" << endl;
            }
            timer.summarize();
#else
            evaluator.buildCircuit(string(ctx.circfile));
            evaluator.readInput(string(ctx.datafile));
            evaluator.buildConnection();
            evaluator.recvGarbledTables();
            evaluator.recvPeerLabels();
            evaluator.recvSelfLabels();
            evaluator.recvOutputMap();
            evaluator.garbleCircuit();
            // evaluator.print();
            evaluator.evaluateCircuit();
            evaluator.sendOutput();
            ctx.out = bin2ints(evaluator.getOutput(), ctx.outsizes);
            // evaluator.shutdown()
            if (verify(ctx.out, ctx.expout)) {
                cout << "\033[1;32mCorrect output, congrat!\033[0m" << endl;
            }
#endif
        }
    }

    bool verify(vector<int> a, vector<int> b) {
        /** return the number of discrepancies between a and b
         */
        if (a.size() != b.size()) {
            cerr << "Two outputs have different size." << endl;
            exit(-1);
        }
        int ai, bi;
        for (int i = 0; i < a.size(); i++) {
            ai = a[i];
            bi = b[i];
            if (ai != bi) {
                cerr << "The " << i << "-th component does not match" << endl;
                cerr << "ai:" << ai << ", bi:" << bi << endl;
            }
        }
        return true;
    }

    bool ExCtx::valid() {
        if (strcmp(this->role, "GARBLER") == 0) {
            if (!this->port) {
                cerr << "null port" << endl;
                exit(-1);
            }
            if (!filexist(this->circfile)) {
                cerr << "null circfile" << endl;
                exit(-1);
            }
            if (!filexist(this->datafile)) {
                cerr << "null datafile" << endl;
                exit(-1);
            }
            return true;
        } else if (strcmp(this->role, "EVALUATOR") == 0) {
            if (!this->port) {
                cerr << "null port" << endl;
                exit(-1);
            }
            if (!this->ip) {
                cerr << "null ip" << endl;
                exit(-1);
            }
            if (!filexist(this->circfile)) {
                cerr << "null circfile" << endl;
                exit(-1);
            }
            if (!filexist(this->datafile)) {
                cerr << "null datafile" << endl;
                exit(-1);
            }
            return true;
        } else {
            cerr << "invalid role" << endl;
            exit(-1);
        }
    }

/* free memory */
    void treefree(ast *a) {
        switch (a->nodetype) {

            /* two subtrees */
        case nPLUS:
        case nMINUS:
        case nMUL:
        case nDIV:
        case nLA:
        case nLE:
        case nLAEQ:
        case nLEEQ:
        case nEQ:
        case nNEQ:
        case nLIST:
            if (a->r)
                treefree(a->r);

            /* one subtree */
        case nUMINUS:
            if (a->l)
                treefree(a->l);

            /* no subtree */
        case nNUMBERLIST:
        case nNAME:
        case nNUMBER:
            break;

        case nASGN:
            free(((symasgn *) a)->v);
            break;

            /* up to three subtrees */
        case nIF:
            treefree(((flow *) a)->cond);
            if (((flow *) a)->tl) treefree(((flow *) a)->tl);
            if (((flow *) a)->el) treefree(((flow *) a)->el);
            break;

        case nRETURN:
            treefree(((myreturn *) a)->l);
            break;
        case nFOR:
            treefree(((flow *) a)->list);
            break;
        case nTD:
            break;
        case nTL:
            treefree((ast *) ((mytypedeflist *) a)->next);
        default:
            std::cout << "internal error: free bad node " << a->nodetype << std::endl;
        }
        free(a);    /* always free the node itself */
    }

    void typedeflistfree(mytypedeflist *sl) {
        mytypedeflist *nsl;
        while (sl) {
            nsl = sl->next;
            free(sl);
            sl = nsl;
        }
    }


/* Miscellaneous */
    void countoff(mytypedef *def) {
        if (def->s->array_type)
            numIN -= def->bitlen * def->size;
        else
            numIN -= def->bitlen;
    }

    bool isBundleValid(Bundle &b) {
        return b.size() > 0;
    }

    bool isBundleValid(vector<Bundle> &vb) {
        if (vb.size() == 0)
            return false;

        int size, prevsize;
        Bundle *b;

        for (int i = 0; i < vb.size(); ++i) {

            b = &vb[i];

            if (i == 0)
                prevsize = b->size();

            size = b->size();

            if (size == 0 || prevsize != size)
                return false;

            prevsize = size;
        }
        return true;
    }

    vector<int> bin2ints(vector<bool> bin, vector<int> sizes) {
        int sz = 0;
        int j = 0;
        int v = 0;
        vector<int> ret;
        for (int i = 0; i < sizes.size(); ++i) {
            sz = sizes[i];
            for (int k = 0; k < sz; k++) {
                v += bin[j++] << k;
            }
            ret.push_back(v);
            v = 0;
        }
        return ret;
    }

    inline bool filexist (char *name) {
        struct stat buffer;
        return (stat (name, &buffer) == 0);
    }
}

/* main function */
int main(int argc, const char *argv[]) {
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "produce help message for gash_lang")
        ("input,i", po::value<string>(), "file path of input function description file")
        ("output,o", po::value<string>(), "file path of circuit output file")
        ("data_output,d", po::value<string>(), "file path of data output file")
        ("mode,m", po::value<string>(), "running mode: normal(garbling and evaluating), verify(simply execute circuit)");


    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << "Usage: options description [options]\n";;
        cout << desc;
        return 0;
    }

    const char *input;
    const char *circ_out;
    const char *data_out;
    const char *mode;
    if (!vm.count("input")) {
        cout << "Require input file" << endl;
        return 0;
    } else
        input = vm["input"].as<string>().c_str();

    if (!vm.count("output")) {
        cout << "Missing circuit output file name, using `out.circ`" << endl;
        circ_out = "out.circ";
    } else
        circ_out = vm["output"].as<string>().c_str();
    if (!vm.count("data_output")) {
        cout << "Missing data output file name, using `data.txt`" << endl;
        data_out = "data.txt";
    } else
        data_out = vm["data_output"].as<string>().c_str();
    if (!vm.count("mode")) {
        cout << "Missing mode, usng `normal`" << endl;
        mode = "normal";
    } else
        mode = vm["mode"].as<string>().c_str();


    ofstream circ_file(circ_out, std::ios::out | std::ios::trunc);
    ofstream data_file(data_out, std::ios::out | std::ios::trunc);

    GASH_LANG::circ.setOutstream(circ_file);
    GASH_LANG::circ.setDataOutstream(data_file);
    GASH_LANG::ex_ctx.circfile = new char[strlen(circ_out) + 1]{};
    strcpy(GASH_LANG::ex_ctx.circfile, circ_out);;
    GASH_LANG::ex_ctx.datafile = new char[strlen(data_out) + 1]{};
    strcpy(GASH_LANG::ex_ctx.datafile, data_out);

    yyin = fopen(input, "r");
    GASH_GC::Timer timer = GASH_GC::Timer();
    timer.tic("Compiling");
    GASH_LANG::push_scope();  // The out-most scope
    int parseresult = yyparse();
    GASH_LANG::scope_stack::Global().clear();
    timer.toc();
    timer.summarize();

    if (strcmp(mode, "normal") == 0) {
        if (GASH_LANG::ex_ctx.valid()) {
            return execute(GASH_LANG::ex_ctx);
        }
    } else if (strcmp(mode, "verify") == 0) {
        return verify(GASH_LANG::ex_ctx);
    } else {
        cerr << "Invalid mode: " << mode << endl;
    }

    return parseresult;
}
/* error printing functions */
void yyerror(const char *s, ...) {
    // va_list ap;
    // va_start(ap, s);
    // fprintf(stderr, "%d: error: ", yylineno);
    // vfprintf(stderr, s, ap);
    // fprintf(stderr, "\n");
    fprintf(stderr, "%s line:%d, first_column:%d, last_column:%d",
            s, yylloc.first_line, yylloc.first_column, yylloc.last_column);
    exit(-1);
}
