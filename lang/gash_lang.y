/* Parser of the Gash-Lang */

%{
#include <stdio.h>
#include <stdlib.h>
#include "gash_lang.h"

extern "C" {int yyparse(void); int yylex(void); int yywrap() { return 1;} }
extern "C" {void yyerror(const char *s, ...); }

%}

/* Enable tracing. Disable it in release version. */
%debug

 /* Set debug level to verbose. */
%error-verbose

 /* Keep track of current location during parsing. */
%locations

%union {
    int d;
    long ld;
    GASH_LANG::ast *a;
    GASH_LANG::symbol *s;
    GASH_LANG::mytypedef *td;
    GASH_LANG::mytypedeflist *tl;
    GASH_LANG::scope* ss;
    GASH_LANG::scope* se;
    int fn;
    int intlen;
    char* spec;
}

/* Declare tokens */
%token <d> NUMBER INT
%token <ld> LONG_NUMBER
%token <s> NAME
%token MYEOF EOL

%token IF
%token THEN
%token ELSE
%token RETURN
%token FOR

%token FUNC
%token DEF_INPUT
%token DEF_ROLE
%token DEF_PORT
%token DEF_IP
%token DEF_START
%token DEF_EXPOUT
%token GARBLER
%token EVALUATOR
%token BUNDLE

%nonassoc <fn> CMP
%nonassoc <ss> SCOPE_START
%nonassoc <se> SCOPE_END
/* %nonassoc '{' */
/* %nonassoc '}' */
%right '='
%left '+' '-' '|' '&' '^' '.'
%left '*' '/'
%nonassoc UMINUS UNEG
%type <a> exp
%type <a> stmt
%type <a> list
%type <a> ret
%type <a> directive
%type <a> directivelist
%type <a> number_list
%type <td> mytypedef
%type <tl> mytypedeflist
%type <spec> time
%start program

%%

stmt: IF exp SCOPE_START list	SCOPE_END			                 { $$ = GASH_LANG::newflow(nIF, $2, $4, NULL, $3, NULL, GASH_LANG::get_current_scope());	}
| IF exp SCOPE_START list SCOPE_END ELSE SCOPE_START list	SCOPE_END        { $$ = GASH_LANG::newflow(nIF, $2, $4, $8, $3, $7, GASH_LANG::get_current_scope());	}
| FOR '(' exp ';' exp ';' exp ')' SCOPE_START list SCOPE_END { $$ = GASH_LANG::newflow(nFOR, $3, $5, $7, $10, $9, GASH_LANG::get_current_scope()); }
| exp ';'
| mytypedef ';'                                { $$=NULL; }
| ret ';'
;

list: stmt      					{ $$ = GASH_LANG::newast(nLIST, $1, NULL);	}
| stmt  list                        { $$ = GASH_LANG::newast(nLIST, $1, $2);   }
;

number_list : NUMBER                   { $$ = GASH_LANG::newnumberlist($1, NULL); }
| NUMBER ',' number_list               { $$ = GASH_LANG::newnumberlist($1, $3);   }
| LONG_NUMBER                          { $$ = GASH_LANG::newnumberlist($1, NULL); }
| LONG_NUMBER ',' number_list          { $$ = GASH_LANG::newnumberlist($1, $3);   }
;

time: '@' '+' NUMBER                        { $$ = GASH_LANG::newtime(tDELTA, $3);         }
| '@' NUMBER ':' NUMBER                     { $$ = GASH_LANG::newtime(tSTAMP, $2, $4);     }
| '@' NUMBER ':' NUMBER ':' NUMBER          { $$ = GASH_LANG::newtime(tSTAMP, $2, $4, $6); }
;

directive: '#' DEF_INPUT NAME NUMBER     { $$ = GASH_LANG::newdirective($3, (long)$4); }
| '#' DEF_INPUT NAME LONG_NUMBER         { $$ = GASH_LANG::newdirective($3, $4); }
| '#' DEF_INPUT NAME SCOPE_START number_list SCOPE_END { $$ = GASH_LANG::newdirective($3, $5); }
| '#' DEF_ROLE GARBLER                   { $$ = GASH_LANG::newdirective(dROLE, "GARBLER"); }
| '#' DEF_ROLE EVALUATOR                 { $$ = GASH_LANG::newdirective(dROLE, "EVALUATOR"); }
| '#' DEF_PORT NUMBER                    { $$ = GASH_LANG::newdirective(dPORT, $3);}
| '#' DEF_IP NUMBER '.' NUMBER '.' NUMBER '.' NUMBER  { $$ = GASH_LANG::newdirective(dIP, GASH_LANG::newip($3, $5, $7, $9)); }
| '#' DEF_START time                     { $$ = GASH_LANG::newdirective(dTIME, $3);}
| '#' DEF_EXPOUT SCOPE_START number_list SCOPE_END     { $$ = GASH_LANG::newdirective(dEXPOUT, $4); }
;

directivelist: directive               { $$ = $1; }
| directive  directivelist             { $$ = GASH_LANG::newast(nDIRLIST, $1, $2);}
;

exp: exp CMP exp                      { $$ = GASH_LANG::newcmp($2, $1, $3);        }
| exp '+' exp			                    { $$ = GASH_LANG::newast(nPLUS, $1, $3);			}
| exp '-' exp				                  { $$ = GASH_LANG::newast(nMINUS, $1, $3);		}
| exp '*' exp				                  { $$ = GASH_LANG::newast(nMUL, $1, $3);			}
| exp '/' exp				                  { $$ = GASH_LANG::newast(nDIV, $1, $3);			}
| exp '|' exp                         { $$ = GASH_LANG::newast(nBOR, $1, $3);      }
| exp '&' exp                         { $$ = GASH_LANG::newast(nBAND, $1, $3);     }
| exp '^' exp                         { $$ = GASH_LANG::newast(nBXOR, $1, $3);     }
| '~' exp %prec UNEG                  { $$ = GASH_LANG::newast(nBNEG, $2);         }
| '(' exp ')'					                { $$ = $2;							          }
| NUMBER						                  { $$ = GASH_LANG::newnum($1);					      }
| LONG_NUMBER                         { $$ = GASH_LANG::newnum($1);               }
| '-' exp %prec UMINUS			          { $$ = GASH_LANG::newast(nUMINUS, $2, NULL);	}
| BUNDLE '(' NUMBER ')'               { $$ = GASH_LANG::newbundlenum($3);          }
| BUNDLE '(' LONG_NUMBER ')'          { $$ = GASH_LANG::newbundlenum($3);         }
| NAME							                  { $$ = GASH_LANG::newref($1);					      }
| exp '=' exp                         { $$ = GASH_LANG::newasgn($1, $3);           }
| mytypedef '=' exp                   { $$ = GASH_LANG::newinitasgn($1, $3);}
| NAME '.' exp                        { $$ = GASH_LANG::newbitidx($1, $3);         }
| NAME '[' exp ']'                    { $$ = GASH_LANG::newarrayidx($1, $3);       }
| NAME '[' exp ']' '.' exp            { $$ = GASH_LANG::newarrayidx($1, $3, $6);   }
;

ret: RETURN exp                 		  { $$ = GASH_LANG::newreturn($2);             }

mytypedef: INT NAME 					        { $$ = GASH_LANG::newtypedef($1, $2);			  }
| INT '[' NUMBER ']' NAME             { $$ = GASH_LANG::newtypedef($1, $3, $5);    }
;

mytypedeflist: mytypedef 				      { $$ = GASH_LANG::newtypedeflist($1, NULL);  }
| mytypedef ',' mytypedeflist		      { $$ = GASH_LANG::newtypedeflist($1, $3);		}
;

program:	/* nothing */
| program directivelist FUNC NAME '(' mytypedeflist ')' SCOPE_START list SCOPE_END MYEOF {
    GASH_LANG::clearValue($6);
    GASH_LANG::defunc($4, $6, $9);
    GASH_LANG::output($9);
    GASH_LANG::outputReturn();
    GASH_LANG::evaldir($2);
    GASH_LANG::outputData();
    GASH_LANG::treefree($9);
    return 0;
 }
| program error MYEOF { yyerrok; }
;
