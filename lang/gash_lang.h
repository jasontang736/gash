/* gash_lang.h */
#ifndef GASH_LANG_H
#define GASH_LANG_H

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <stdlib.h>
#include <iostream>
#include <stdarg.h>
#include <vector>
#include <string>
#include <utility>
#include <math.h>
#include <assert.h>
#include <iomanip>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <fstream>

/* Gash GC include */
//#include "garbler.h"
//#include "evaluator.h"
//#include "util.h"

using std::vector;
using std::map;
using std::pair;
using std::cout;
using std::endl;
using std::string;

/* LOGING */
#define WARNING(s)                              \
    std::cout << "Warning: " << s << "\n"       \

/* Define ZERO wire */
#define ZERO new Wire(++(++wcount), 0)
#define ONE new Wire(++(++wcount), 1)


/* External: interface to the lexer */
extern int yylineno;    /* from lexer */
extern "C" void yyerror(const char *s, ...);
extern "C" int yyparse(void);

/* External: interface to the parser */
struct YYLTYPE;
extern "C" YYLTYPE yylloc;

/* External: counting */
extern int wcount;
extern int wcount_dref;
extern int numIN;
extern int numOUT;
extern int numAND;
extern int numOR;
extern int numXOR;
extern int numDFF;

/* Enums */
typedef enum {
    nPLUS = 1,
    nMINUS = 2,
    nUMINUS = 3,
    nNAME = 4,
    nREF = 5,
    nFUNC = 6,
    nMUL = 7,
    nDIV = 8,
    nIF = 9,
    nLIST = 10,
    nASGN = 11,
    nNUMBER = 12,
    nBUNDLENUMBER = 13,
    nNUMBERLIST = 14,
    nRETURN = 15,
    nTD = 16,    // Typedef
    nTDi = 17, // Typedef inner variable
    nFOR = 18, // for loop
    nTL = 19,    // Typedef list
    nLA = 20,    // >, larger than
    nLE = 21,    // <, less than
    nLAEQ = 22,    // >=
    nLEEQ = 23,    // <=
    nEQ = 24,    // ==
    nNEQ = 25,    // !=
    nBOR = 26, // bit-wise or
    nBAND = 27, // bit-wise and
    nBXOR = 28, // bit-wise xor
    nBNEG = 29, // bit-wise neg
    nDIR = 30,
    nDIRLIST = 31,
} Knodetype;
typedef enum {
    AND = 8,
    OR = 14,
    XOR = 6,
    DFF = 16
} Op;
// reference types other than symbol
typedef enum {
    rBUNDLE,		              // e.g. in1
    rBUNDLE_dot_WIRE,         // e.g. in1.(i+j)
    rARRAY_dot_BUNDLE,         // e.g. in1[i+j]
    rARRAY_dot_BUNDLE_dot_WIRE,     // e.g. in1[i+j].(k+l)
} Kreftype;
typedef enum {
    aSYM,
    aREF,
} Kassigntype;
typedef enum {
    dSINGLE,
    dARRAY,
    dROLE,
    dPORT,
    dIP,
    dTIME,
    dEXPOUT,
} Kdirtype;
typedef enum {
    tDELTA,
    tSTAMP
} Ktimetype;

namespace GASH_LANG {
/* All structs declaration */
    class Wire;
    class Bundle;
    class sPrologue;
    class sGate;
    class sGates;
    class sIO;
    class Circ;
    class Facade;

    class scope;
    class symbol;
    class ast;
    class flow;
    class bundlenum;
    class symref;
    class symasgn;
    class myreturn;
    class mytypedef;
    class mytypedeflist;
    class directive;
    class numberlist;
    class arrayidx;


/* Function: symbol lookup */
    symbol* lookup(char *);
    symbol* lookup(string id);
    symbol* lookup(string id, int version);
    symbol* lookup(string id, int version, scope* scope);

/* Structs: wire and bundle */
    class Wire {
    public:
        int id = 0;
        int v = -1;        // v should be set only when its a constant wire
        sGate* from_gate;
        vector<sGate*> to_gates;
        bool used_once_ = false;
        Wire() {}
        Wire(int Id) : id(Id), v(-1) {}
        Wire(int Id, int V) : id(Id), v(V) {}
        void invert();
        void set_id_even();
        void set_id_odd();
        void invert_from(Wire*);
        inline void used() { used_once_ = true; }
        inline bool used_once() { return used_once_; }
    };
    class Bundle {
    public:
        vector<Wire*> wires;

        Bundle();

        Bundle(vector<Wire*> ws);

        Bundle(Wire* w);

        Bundle(int len);

        Bundle(Bundle b, int size);

        void add(Wire* w);

        Wire* &operator[](int i);

        int size();

        void replace(Bundle r, int start, int rstart, int size);

        void clearValue();
    };

/* Structs: circ related */
    class sPrologue {
    public:
        int numVAR;
        int numIN;
        int numOUT;
        int numAND;
        int numOR;
        int numXOR;
        int numDFF;

        sPrologue();

        sPrologue(int aNumVAR, int aNumIN, int aNumOUT, int aNumAND, int aNumOR, int aNumXOR, int aNumDFF);

        void emit(std::ostream &outstream);
    };
    class sGate {
    public:
        Wire* out;
        Op op;
        Wire* in1;
        Wire* in2;

        sGate();

        sGate(Wire* aOut, Op aOp, Wire* aIn1, Wire* aIn2);

        void emit(std::ostream &outstream);
    };
    class sGates {
    public:
        vector<sGate*> gates;
        void emit(std::ostream &outstream);
    };

    class sIO {
    public:
        Bundle bundle;

        void emit(std::ostream &outstream);
    };

    class Circ {
    public:
        sPrologue prologue;
        sIO in;
        sIO out;
        sGates gates;
        vector<pair<int, int> > data;   // id, bit
        map<int, int> input_duplicates;  // map input id to id of its duplicates, for inverted wires.
        map<Wire*, Wire*> wire_inverts;
        std::ostream *outstream_ptr = &std::cout;        // Default
        std::ostream *data_stream_ptr;

        Circ();

        void output();

        void addData(long value, Bundle &b);

        bool hasInputInvertDuplicate(Wire* w);

        bool isInputWire(Wire* w);

        void addInputInvertDuplicate(Wire* orig_w, Wire* duplicate_w);

        Wire* getInputInvertDuplicate(Wire* w);

        void outputData();

        void setOutstream(std::ostream &out);

        void setDataOutstream(std::ostream &out);

        bool hasInvertedWire(Wire* w);

        Wire* getInvertedWire(Wire* w);

        void addInvertedWire(Wire* w, Wire* inv_w);
    };

    /* Scoping related */
    class symbol_store {
        /* symbol_store has the same interface as a scope, but it
           is mainatained throughout the program.
           The reason is that symbol version is global.
           It is responsible for creating and deleting symbols.
        */
    private:
        map<string, vector<symbol*> > symbols_; // symbol_store owns them, and the i-th symbol has i+1 version number.
    public:
        bool has_symbol_for_id(string id);
        void require_has_symbol_for_id(string id);
        bool examine_version_index();
        bool examine_version_index_for_id(string id);
        int get_newest_version_for_id(string id);
        symbol* get_symbol_for_id_version(string id, int version);
        symbol* new_symbol(string id);
        symbol* new_symbol(symbol* old_sym);
        symbol* get_symbol_for_id(string id);
        symbol_store(){}
        ~symbol_store();
    };
    class scope {
    public:
        // scope does not own them
        // but all vector of symbols in `symbols_` has increasing(not necessarily continuous) versions.
        map<string, vector<symbol*> > symbols;
        scope* prev_scope;
        enum scope_type {
            FUNCTION_SCOPE,
            BLOCK_SCOPE
        };
        scope_type type;
        bool has_symbol_for_id(string id);
        bool add_symbol(symbol* sym);
        symbol* new_symbol(string id);
        symbol* new_symbol(symbol* old_sym);
        symbol* get_symbol_for_id(string id);
        symbol* get_return_symbol();
        int get_newest_version_for_id(string id);
        symbol* get_symbol_for_id_version(string id, int version);
        scope() : prev_scope(nullptr) {}
    };

    void push_scope();
    void push_scope(scope* scope);
    void pop_scope();
    scope* get_current_scope();
    extern scope::scope_type next_scope_type;
    // void set_function_params_in_current_scope();
    // bool is_prev_scope_function_scope();

    Bundle number2Bundle(int num);

/* Structs: AST related structs */
    class ast {
    public:
        int nodetype;
        ast *l;
        ast *r;
    };
    class symbol {
    public:
        string id;
        double value = 0;
        bool array_type = false;
        int version = 0; // Version number, will be initialized to 1
        Bundle bundle;
        vector<Bundle> bundles;
        ast *func = nullptr;     /* stmt for the function */
        mytypedeflist *defs = nullptr; /* list of dummy args */
        symbol(){}
        symbol(string id);
        symbol(symbol* rhs);
    };
    class flow {
    public:
        int nodetype;        /* type I */
        ast* init;        // for
        ast* cond;        // for and if
        ast* inc;         // for
        ast* list;        // for
        ast* tl;          // if
        ast* el;          // if
        scope* tl_scope;
        scope* el_scope;
        scope* for_scope;
        scope* orig_scope;
    };
    enum numbertype {
        inttype,
        longtype
    };
    class bundlenum {
    public:
        int nodetype;        /* type K */
        Bundle bundle;
        int value;
        scope* orig_scope;
    };
    class num {
    public:
        int nodetype;
        numbertype ntype;
        union {
            int ivalue;
            long lvalue;
        };
    };
    class symref {
    public:
        int nodetype; // Knodetype
        int reftype;  // Kreftype
        symbol *s;
        union {
            int vidx;
            symbol *vs;
            ast *va;
        };
        union {
            int bidx;
            symbol *bs;
            ast *ba;
        };

        symref();
    };
    class symasgn {
    public:
        int nodetype;        /* type = */
        symref *l;   // LHS, bitidx, bundled bitidx, bundle
        ast *v;   // RHS, value
    };
    class myreturn {
    public:
        int nodetype;
        ast *l;
        symbol* ret_sym;
    };
    class mytypedef {
    public:
        int nodetype;
        int bitlen;
        int size;
        int value; // For internal variable
        symbol *s;
    };
    class mytypedeflist {
    public:
        int nodetype;
        mytypedef *def;
        mytypedeflist *next;
    };
    class directive {
    public:
        int nodetype;
        int dirtype;
        symbol *s;  // The symbol that this directive is going to provide input
        union {
            long value;   // To store the value of the variable
            numberlist *number_list;
        };
        char *spec;
        int dspec;
    };
    class numberlist {
    public:
        int nodetype;
        numbertype ntype;
        union {
            int l;
            long ln;
        };
        numberlist *r;
    };
    class arrayidx {
    public:
        int nodetype;
        symbol *s;
        int idx;
    };
    class ExCtx {
    public:
        char *role;
        int port;
        char *ip;
        char *startat;
        char *circfile;
        char *datafile;
        vector<int> outsizes;
        vector<int> expout;
        vector<int> out;
        bool valid();
    };
    class VeriCtx {
    public:
        char *role;
        char *circfile;
        char *datafile;
        vector<int> outsizes;
        vector<int> expout;
        vector<int> out;
    };

/* Functions: AST generation */
    ast *newast(int nodetype, ast *l, ast *r);

    ast *newast(int nodetype, ast *l);

    ast *newcmp(int cmptype, ast *l, ast *r);

    ast *newfunc(int functype, ast *l);

    ast *newref(symbol *s);

    ast *newasgn(ast *l, ast *v);

    ast *newinitasgn(mytypedef *l, ast *v);

    ast *newnum(int d);

    ast *newbundlenum(int d);

    ast *newflow(int nodetype, ast *cond, ast *tl, ast *el, scope* tl_scope, scope* el_scope, scope* orig_scope);

    ast *newflow(int nodetype, ast *init, ast *cond, ast *inc, ast *list, scope* for_scope, scope* orig_scope);

    ast *newreturn(ast *l);

    ast *newbitidx(symbol *s, ast *ba);

    ast *newarrayidx(symbol *s, ast *va);

    ast *newarrayidx(symbol *s, ast *va, ast *ba);

    mytypedef *newtypedef(int len, symbol *s);

    mytypedef *newtypedef(int len, int size, symbol *s);

    mytypedeflist *newtypedeflist(mytypedef *l, mytypedeflist *r);

    // Set all bundles of `list`'s symbols to be of value -1
    void clearValue(mytypedeflist* list);

    ast *newdirective(symbol *s, long value);

    ast *newdirective(symbol *s, ast *l);

    ast *newdirective(Kdirtype, char*);

    ast *newdirective(Kdirtype, int);

    ast *newdirective(Kdirtype, ast *r);

    ast *newnumberlist(int l, ast *r);

    ast *newnumberlist(long l, ast *r);

/* Directive related */
    char *newip(int, int, int, int);

    char *newtime(Ktimetype, int);

    char *newtime(Ktimetype, int, int);

    char *newtime(Ktimetype, int, int, int);

/* Function: define a function/circuit */
    void defunc(symbol *name, mytypedeflist *defs, ast *stmts);

/* Function: output and eval */
    Bundle output(ast *);

    double eval(ast *);

/* Directives */
    void evaldir(ast *);

/* Execute Circuit */
    int execute(ExCtx &);

    bool verify(vector<int>, vector<int>);

/* Function: free memory */
    void treefree(ast *);

    void typedeflistfree(mytypedeflist *sl);

/* Function: bundle manipulations */
    Bundle Bconc(Wire, Bundle);

    Bundle Bconc(Bundle, Wire);

    Bundle Bconc(Bundle, Bundle);

    Bundle Bdup(Wire, int t);

    vector<Bundle> newBundles(int bitlen, int size);

    Bundle Bmerge(vector<Bundle> vb);

/* Function: wire-level output */
    Wire* outputFADD(Wire*& cin, Wire* a, Wire* b);

    Wire* outputFSUB(Wire*& bout, Wire* a, Wire* b, Wire* first_bout);

    Wire* outputAND(Wire* a, Wire* b);

    Wire* outputOR(Wire* a, Wire* b);

    Wire* outputXOR(Wire* a, Wire* b);

    Wire* outputLA(Bundle as, Bundle bs);

    Wire* outputEQ(Bundle as, Bundle bs);

    Wire* outputLE(Bundle as, Bundle bs);

    Wire* outputLAEQ(Bundle as, Bundle bs);

    Wire* outputLEEQ(Bundle as, Bundle bs);

    Wire* outputNEQ(Bundle as, Bundle bs);

    Wire* Winvert(Wire* w);

    Wire* nextwire();

/* Function: bundle-level output */
    Bundle outputADD(Bundle, Bundle);

    Bundle outputSUB(Bundle, Bundle);

    Bundle outputMUL(Bundle as, Bundle bs);

    Bundle outputDVG(Bundle as, Bundle bs, Wire*& ret0);

    Bundle outputDIV(Bundle as, Bundle bs);

    Bundle outputANDs(Bundle as, Bundle bs);

    Bundle outputORs(Bundle as, Bundle bs);

    Bundle outputXORs(Bundle as, Bundle bs);

    Bundle outputNEGs(Bundle as);

    Bundle outputUMINUS(Bundle as);

    Bundle outputIfThenElse(Bundle cond, Bundle as, Bundle bs);

    void outputIfThenElse(Bundle cond, Bundle as, Bundle bs, Bundle* cs);

    void outputReturn();

/* Function: bottom-level output */
    void output(Wire*& o, Op op, Wire* a, Wire* b);

    void output(Wire* w);

    void outputData();

/* AST checkNull */
    template<typename T>
    void checkNull(const T &n);

/* Miscelaneous */
    void countoff(mytypedef *);

    bool isBundleValid(Bundle &);

    bool isBundleValid(vector<Bundle> &);

    vector<int> bin2ints(vector<bool>, vector<int>);

    bool filexist(char *name);
}


#endif
